package flowengine;

import flowengine.annotations.flow.*;
import flowengine.annotations.flow.methods.Back;
import flowengine.annotations.flow.methods.Next;
import flowengine.annotations.flow.methods.Restart;
import flowengine.annotations.flow.methods.Resume;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple FlowEngine.
 */
public class FlowEngineTest
    extends TestCase
{
    /**
     * Create the test case
     *
     */
    @FlowSteps()
    public interface TestInterface extends Flow {
        @Next
        void next();

        @Resume
        void resume();

        @Restart
        void restart();

        @Back
        void back();

        void notAnnotated();
    }

    public interface TestInjectionInterface {

    }

    public class TestImplementor implements TestInjectionInterface {

    }

    public class TestClass {

        public TestInjectionInterface testInjection;
    }

    public FlowEngineTest(String testName)
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( FlowEngineTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testInjectionMethod() {
        assertTrue(true);
    }
}
