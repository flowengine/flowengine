package flowengine;

import flowengine.annotations.flow.*;
import flowengine.core.DummyFlow;
import flowengine.core.utils.AnnotationSearcher;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Created by florencia on 19/04/15.
 */
public class SearchAnnotationValidatorProcessorTest extends TestCase
{

    public interface MainFlow extends Flow {}

    public class ClassWithoutAFlow {}

    @FEActivity(MainFlow.class)
    public class ClassWithAFlow {}

    public class ClassWithParentWithAFlow extends ClassWithAFlow {}

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public SearchAnnotationValidatorProcessorTest(String testName)
    {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( SearchAnnotationValidatorProcessorTest.class );
    }

    /**
     *
     */
    public void testSearchAnnotationAFlowForClassWithoutAnnotation() {
        testSearchAnnotationAFlow(DummyFlow.class.getName(), ClassWithoutAFlow.class);
    }

    public void testSearchAnnotationAFlowForClassWithAnnotation() {
        testSearchAnnotationAFlow(MainFlow.class.getName(), ClassWithAFlow.class);
    }

    public void testSearchAnnotationAFlowForClassWithParentWithAnnotation() {
        testSearchAnnotationAFlow(MainFlow.class.getName(), ClassWithParentWithAFlow.class);
    }


    private void testSearchAnnotationAFlow(String expectedClassName, Class cls) {
        Class<? extends Flow> flowInstance = AnnotationSearcher.findAFlowForClass(cls);
        assertNotNull(flowInstance);
        assertEquals(expectedClassName, flowInstance.getName());
    }

}
