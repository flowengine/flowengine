package flowengine;

import flowengine.core.utils.MethodSearcher;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.lang.reflect.Method;

/**
 * Created by Ignacio on 04/07/2015.
 */
public class MethodSearcherTest extends TestCase {
    public MethodSearcherTest(String testName)
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( MethodSearcherTest.class );
    }

    public class TestCorrectClass {

        public void onEvent(TestCorrectClass event) {

        }
    }
    public class TestHierarchyClass extends TestCorrectClass {

    }

    public class TestIncorrectClass {

    }


    public class TestIncorrectMethod {
        public void onEvent(TestIncorrectClass event, TestCorrectClass event2) {}
    }

    /**
     * Rigourous Test :-)
     */
    public void testMethodSearcherReturnsTrueOnMethodDeclared()
    {
        assertTrue( MethodSearcher.hasEventMethod(TestCorrectClass.class) );
    }

    public void testMethodSearcherReturnsTrueOnMethodDeclaredOnHierarchy()
    {
        assertTrue( MethodSearcher.hasEventMethod(TestHierarchyClass.class) );
    }

    public void testMethodSearcherReturnsFalseOnUndeclaredMethod() {
        assertFalse(MethodSearcher.hasEventMethod(TestIncorrectClass.class));
    }

    public void testMethodSearcherReturnsFalseOnIncorrectMethod() {
        assertFalse(MethodSearcher.hasEventMethod(TestIncorrectMethod.class));
    }
}
