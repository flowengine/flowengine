package flowengine.core.persistence;

import android.content.Context;
import flowengine.core.persistence.persistableEntities.Group;
import flowengine.core.persistence.persistableEntities.User;
import flowengine.core.persistence.strategies.PersistenceStrategy;
import flowengine.core.persistence.strategies.PersistenceStrategyBuilder;
import org.junit.Before;
import org.robolectric.Robolectric;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by florencia on 27/06/15.
 */
public class DaoTest {

    protected static String groupName = "groupTest";
    protected static String userName = "userTest";
    protected static Date userBirthDate = new Date();

    protected Context context;
    protected BasicDao<User,Long> userDao;
    protected BasicDao<Group,Long> groupDao;
    protected PersistenceStrategy strategy;
    protected Logger log = Logger.getGlobal();

    @Before
    public void setUp() {
        try{
            context = Robolectric.getShadowApplication().getApplicationContext();
            strategy = PersistenceStrategyBuilder.getInstance().newInstance(context);
            strategy.setPersistableClass(User.class);
            strategy.setPersistableClass(Group.class);

            userDao = new BasicDao(User.class,strategy);
            groupDao = new BasicDao(Group.class,strategy);
        } catch (Exception ex){
            log.log(Level.ALL,ex.getMessage());
        }
    }

    protected Group saveGroup(String groupName) throws Exception {
        Group group = new Group(groupName);
        groupDao.save(group);
        return group;
    }

    protected User saveUser(String name,Date date,Group group) throws Exception {
        User user = new User(name,date,group);
        userDao.save(user);
        return user;
    }

    protected List<Group> getGroups() throws Exception {
        String groupName2 = groupName + "2";
        Group group1 = saveGroup(groupName);
        Group group2 = saveGroup(groupName2);

        List<Group> groups = new ArrayList<>();
        groups.add(group1);
        groups.add(group2);

        return groups;
    }

    protected List<User> getUsers(Group group) throws Exception {

        User user1 = saveUser(userName, userBirthDate, group);
        User user2 = saveUser(userName + "2", userBirthDate, group);

        List<User> users = new ArrayList<>();
        users.add(user1);
        users.add(user2);

        return users;
    }

    protected List<User> getUsersWithName(List<User> users, String name){
        List<User> usersWithName = new ArrayList<>();
        for(User user : users){
            if(user.getName().equals(name))
                usersWithName.add(user);
        }
        return usersWithName;
    }


}
