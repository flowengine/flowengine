package flowengine.core.persistence.persistableEntities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by florencia on 23/05/15.
 */
@DatabaseTable
public class User {
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String BIRTH_DATE = "birth_date";
    public static final String GROUP = "group_id";

    @DatabaseField(generatedId = true, columnName = ID)
    private Long id;

    @DatabaseField(columnName = NAME)
    private String name;

    @DatabaseField(columnName = BIRTH_DATE)
    private Date birthDate;

    @DatabaseField(foreign = true, columnName = GROUP, foreignAutoRefresh=true)
    private Group group;


    public User(String name, Date birthDate, Group group){
        this.birthDate = birthDate;
        this.name = name;
        this.group = group;
    }

    public User() { }

    public Long getId() { return id;}

    public void setId(Long id) { this.id = id;}

    public String getName() { return name;}

    public void setName(String name) { this.name = name;}

    public Date getBirthDate() { return birthDate;}

    public void setBirthDate(Date birthDate) { this.birthDate = birthDate;}

    public Group getGroup() { return group;}

    public void setGroup(Group group) { this.group = group;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (id != null ? !id.equals(user.id) : user.id != null) return false;
        if (name != null ? !name.equals(user.name) : user.name != null) return false;
        if (birthDate != null ? !birthDate.equals(user.birthDate) : user.birthDate != null) return false;
        return !(group != null ? !group.equals(user.group) : user.group != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (birthDate != null ? birthDate.hashCode() : 0);
        result = 31 * result + (group != null ? group.hashCode() : 0);
        return result;
    }
}
