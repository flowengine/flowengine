package flowengine.core.persistence;

import android.content.Context;
import flowengine.core.persistence.strategies.DatabasePersistenceStrategy;
import flowengine.core.persistence.strategies.MemoryPersistenceStrategy;
import flowengine.core.persistence.strategies.PersistenceStrategy;
import flowengine.core.persistence.strategies.PersistenceStrategyBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertEquals;

/**
 * Created by florencia on 22/05/15.
 */
@RunWith(RobolectricTestRunner.class)
@Config(manifest= Config.NONE)
public class PersistenceStrategyBuilderTest {

    private PersistenceStrategyBuilder persistenceBuilder;
    private Context context;

    @Before
    public void setUp() {
        context = Robolectric.getShadowApplication().getApplicationContext();
        persistenceBuilder = PersistenceStrategyBuilder.getInstance();
    }

    @Test
    @Config(manifest="src/test/java/flowengine/core/persistence/databasePersistence/AndroidManifest.xml")
    public void testBuildDatabasePersistenceStrategy() {
        PersistenceStrategy strategy = persistenceBuilder.newInstance(context);
        assertEquals(DatabasePersistenceStrategy.class, strategy.getClass());
    }

    @Test
    @Config(manifest="src/test/java/flowengine/core/persistence/memoryPersistence/AndroidManifest.xml")
    public void testBuildMemoryPersistenceStrategy() {
        PersistenceStrategy strategy = persistenceBuilder.newInstance(context);
        assertEquals(MemoryPersistenceStrategy.class, strategy.getClass());
    }

}
