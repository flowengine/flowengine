package flowengine.core.persistence;

import android.app.Activity;
import android.content.Context;
import flowengine.core.utils.AndroidManifestParser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by florencia on 22/05/15.
 */
@RunWith(RobolectricTestRunner.class)
@Config(manifest= Config.NONE)
public class AndroidManifestParserTest {

    public class TestActivity extends Activity { }

    private Context context;

    @Before
    public void setUp() {
        context = Robolectric.getShadowApplication().getApplicationContext();
    }

    @Test
    @Config(manifest="src/test/java/flowengine/core/persistence/databasePersistence/AndroidManifest.xml")
    public void testGetPersistenceTypeSuccess() {
        String persistenceType = PersistenceConstants.PERSISTENCE_TYPE;
        String result = AndroidManifestParser.getMetadata(context, persistenceType);
        assertEquals(PersistenceTypeEnum.DATABASE_PERSISTENCE , result);

    }

    @Test
    @Config(manifest="src/test/java/flowengine/core/persistence/databasePersistence/AndroidManifest.xml")
    public void testGetDatabaseNameSuccess() {
        String databaseName = PersistenceConstants.DATABASE_NAME;
        String result = AndroidManifestParser.getMetadata(context, databaseName);
        assertEquals("testDB.db", result);

    }

    @Test
    @Config(manifest="src/test/java/flowengine/core/persistence/databasePersistence/AndroidManifest.xml")
    public void testGetMetaDataNotFound() {
        String metadataToFind = "noMetadata";
        String result = AndroidManifestParser.getMetadata(context, metadataToFind);
        assertNull(result);

    }

    @Test
    @Config(manifest="src/test/java/flowengine/core/persistence/memoryPersistence/AndroidManifest.xml")
    public void testGetPersistenceMemoryTypeSuccess() {
        String persistenceType = PersistenceConstants.PERSISTENCE_TYPE;
        String result = AndroidManifestParser.getMetadata(context, persistenceType);
        assertEquals(PersistenceTypeEnum.MEMORY_PERSISTENCE , result);

    }

    @Test
    @Config(manifest="src/test/java/flowengine/core/persistence/memoryPersistence/AndroidManifest.xml")
    public void testGetMemoryDatabaseNameSuccess() {
        String databaseName = PersistenceConstants.DATABASE_NAME;
        String result = AndroidManifestParser.getMetadata(context, databaseName);
        assertEquals("testDB.db", result);

    }

}
