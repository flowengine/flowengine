package flowengine.core.persistence.persistableEntities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by florencia on 23/05/15.
 */
@DatabaseTable(tableName= "group_")
public class Group {
    public static final String ID = "id";
    public static final String NAME = "name";

    @DatabaseField(generatedId = true, columnName = ID)
    private Long id;
    @DatabaseField(columnName = NAME)
    private String name;

    public Group(String name) {
        this.name = name;
    }

    public Group() {
    }

    public Long getId() { return id;}

    public void setId(Long id) { this.id = id;}

    public String getName() { return name;}

    public void setName(String name) { this.name = name;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Group group = (Group) o;

        if (id != null ? !id.equals(group.id) : group.id != null) return false;
        return !(name != null ? !name.equals(group.name) : group.name != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
