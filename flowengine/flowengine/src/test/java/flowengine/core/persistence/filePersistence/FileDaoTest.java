package flowengine.core.persistence.filePersistence;

import flowengine.core.persistence.DaoTest;
import flowengine.core.persistence.persistableEntities.Group;
import flowengine.core.persistence.persistableEntities.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.List;
import java.util.logging.Level;

import static org.junit.Assert.*;

/**
 * Created by florencia on 22/05/15.
 */
@RunWith(RobolectricTestRunner.class)
@Config(manifest="src/test/java/flowengine/core/persistence/filePersistence/AndroidManifest.xml")
public class FileDaoTest extends DaoTest {

    @Test
    public void testSaveSimpleEntity() {
        try{
            Group groupExpected = saveGroup(groupName);

            List<Group> groups = groupDao.getAll();
            Group group = groups.get(0);

            assertEquals(groupExpected, group);

        } catch (Exception ex){
            log.log(Level.ALL, ex.getMessage());
        }
    }

    @Test
    public void testSaveEntityWithRelationship() {
        try{
            Group group = saveGroup(groupName);
            User userExpected = saveUser(userName, userBirthDate, group);

            List<User> users = userDao.getAll();
            User user = users.get(0);

            assertEquals(userExpected, user);

        } catch (Exception ex){
            log.log(Level.ALL, ex.getMessage());
        }
    }

    @Test
    public void testGetAll() {
        try{
            List<Group> groupsExpected = getGroups();
            List<User> usersExpected = getUsers(groupsExpected.get(0));

            List<Group> groups = groupDao.getAll();
            List<User> users = userDao.getAll();

            assertEquals(groupsExpected, groups);
            assertEquals(usersExpected, users);

        } catch (Exception ex){
            log.log(Level.ALL, ex.getMessage());
        }
    }

}
