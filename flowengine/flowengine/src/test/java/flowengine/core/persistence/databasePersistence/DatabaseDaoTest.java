package flowengine.core.persistence.databasePersistence;

import flowengine.core.persistence.DaoTest;
import flowengine.core.persistence.persistableEntities.Group;
import flowengine.core.persistence.persistableEntities.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import javax.swing.tree.ExpandVetoException;
import java.util.*;
import java.util.logging.Level;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by florencia on 22/05/15.
 */
@RunWith(RobolectricTestRunner.class)
@Config(manifest="src/test/java/flowengine/core/persistence/databasePersistence/AndroidManifest.xml")
public class DatabaseDaoTest extends DaoTest {

    @Test
    public void testSaveSimpleEntity() {
        try{
            Group groupSaved = saveGroup(groupName);
            assertNotNull(groupSaved.getId());

            Group groupRecovered = groupDao.get(groupSaved.getId());
            assertEquals(groupSaved, groupRecovered);

        } catch (Exception ex){
            log.log(Level.ALL, ex.getMessage());
        }
    }

    @Test
    public void testSaveEntityWithRelationship() {
        try {
            Group group = saveGroup(groupName);
            User userSaved = saveUser(userName, userBirthDate, group);
            assertNotNull(userSaved.getId());

            User userRecovered = userDao.get(userSaved.getId());
            assertEquals(userSaved, userRecovered);

        } catch (Exception ex) {
            log.log(Level.ALL, ex.getMessage());
        }
    }

    @Test
    public void testGet() {
        try{
            Group group = saveGroup(groupName);
            User userSaved = saveUser(userName, userBirthDate, group);
            User userRecovered = userDao.get(userSaved.getId());

            assertEquals(userSaved, userRecovered);

        } catch (Exception ex){
            log.log(Level.ALL, ex.getMessage());
        }
    }

    @Test
    public void testGetAll() {
        try{
            String groupName2 = groupName + "2";
            Group group1 = saveGroup(groupName);
            Group group2 = saveGroup(groupName2);

            List<Group> expectedGroups = new ArrayList<>();
            expectedGroups.add(group1);
            expectedGroups.add(group2);

            List<Group> groups = groupDao.getAll();
            assertEquals(expectedGroups, groups);

        } catch (Exception ex){
            log.log(Level.ALL, ex.getMessage());
        }
    }

    @Test
    public void testUpdate() {
        try {
            Group group = saveGroup(groupName);
            User userSaved = saveUser(userName, userBirthDate, group);

            User userRecovered = userDao.get(userSaved.getId());
            assertEquals(userName, userRecovered.getName());

            String updatedName = "testUpdateName";
            userRecovered.setName(updatedName);
            userDao.update(userRecovered);

            userRecovered = userDao.get(userSaved.getId());
            assertEquals(updatedName, userRecovered.getName());

        } catch (Exception ex){
            log.log(Level.ALL, ex.getMessage());
        }
    }

    @Test
    public void testDelete() {
        try{
            Group group = saveGroup(groupName);
            User userSaved = saveUser(userName, userBirthDate, group);

            User userRecovered = userDao.get(userSaved.getId());
            assertNotNull(userRecovered);

            userDao.delete(userRecovered);
            userRecovered = userDao.get(userRecovered.getId());

            assertNull(userRecovered);

        } catch (Exception ex){
            log.log(Level.ALL, ex.getMessage());
        }
    }

    @Test
    public void testDeleteCollection() {
        try{
            List<Group> groups = getGroups();

            for(Group group : groups)
                assertNotNull(groupDao.get(group.getId()));

            groupDao.deleteAll(groups);

            for(Group group : groups)
                assertNull(groupDao.get(group.getId()));

        } catch (Exception ex){
            log.log(Level.ALL, ex.getMessage());
        }
    }

    @Test
    public void testSimpleFindAllBy() {
        try{
            Group group = saveGroup(groupName);
            List<User> users = getUsers(group);

            /* Must return the same list, as the birth date is the same for all */
            List<User> usersResult = userDao.findAllBy(User.BIRTH_DATE, userBirthDate);
            assertEquals(users, usersResult);

            /* Must return only one element */
            usersResult = userDao.findAllBy(User.NAME,userName);
            assertEquals(1, usersResult.size());
            assertEquals(getUsersWithName(users, userName), usersResult);

            /* Must return the same list, as the group is the same for all  */
            usersResult = userDao.findAllBy(User.GROUP, group);
            assertEquals(users, usersResult);

        } catch (Exception ex){
            log.log(Level.ALL, ex.getMessage());
        }
    }

    @Test
    public void testMultipleFindAllBy() {
        try {
            Group group = saveGroup(groupName);
            List<User> users = getUsers(group);

            Map fieldValues = new HashMap();
            fieldValues.put(User.BIRTH_DATE, userBirthDate);
            fieldValues.put(User.GROUP, group);

            /* Must return the same list, as the birth date and group is the same for all */
            List<User> usersResult = userDao.findAllBy(fieldValues);
            assertEquals(users, usersResult);

            fieldValues.put(User.NAME, userName);

            /* Must return only one element */
            usersResult = userDao.findAllBy(fieldValues);
            assertEquals(1, usersResult.size());
            assertEquals(getUsersWithName(users, userName), usersResult);

        } catch (Exception ex){
            log.log(Level.ALL, ex.getMessage());
        }
    }

    @Test
    public void testSimpleFindBy() {
        try {
            Group group = saveGroup(groupName);
            getUsers(group);

            /* Must return the first element */
            User userResult = userDao.findBy(User.GROUP, group);
            assertEquals(group, userResult.getGroup());

        } catch (Exception ex){
            log.log(Level.ALL, ex.getMessage());
        }
    }

    @Test
    public void testMultipleFindBy() {
        try {
            Group group = saveGroup(groupName);
            getUsers(group);

            Map fieldValues = new HashMap();
            fieldValues.put(User.BIRTH_DATE, userBirthDate);
            fieldValues.put(User.GROUP, group);

            /* Must return the first element */
            User userResult = userDao.findBy(fieldValues);
            assertEquals(group, userResult.getGroup());
            assertEquals(userBirthDate, userResult.getBirthDate());

        } catch (Exception ex){
            log.log(Level.ALL, ex.getMessage());
        }

    }

    @Test
    public void testQuery() {
        try {
            Group group = saveGroup(groupName);
            List<User> users = getUsers(group);

            String query = "select * from user where " + User.NAME + " = ? ";
            List usersResult = userDao.query(query, userName);
            assertEquals(1, usersResult.size());
            assertEquals(getUsersWithName(users, userName), usersResult);

        } catch (Exception ex){
            log.log(Level.ALL, ex.getMessage());
        }
    }

    @Test
    public void testJoinWithQuery() {
        try {
            Group group = saveGroup(groupName);
            List<User> users = getUsers(group);

            String groupName2 = groupName + "2";
            User user = saveUser(userName,userBirthDate,saveGroup(groupName2));

            String query = " select u.* from user as u inner join group_ as g  " +
                    " on u." + User.GROUP + " = g." + Group.ID +
                    " where g." + Group.NAME + " = ? ";

            List usersResult = userDao.query(query, groupName);
            assertEquals(2, usersResult.size());
            assertEquals(users, usersResult);

            usersResult = userDao.query(query, groupName2);
            users.clear();
            users.add(user);
            assertEquals(1, usersResult.size());
            assertEquals(users, usersResult);

        } catch (Exception ex){
            log.log(Level.ALL, ex.getMessage());
        }
    }

    @Test
    public void testQueryValue() {
        try {
            Group group = saveGroup(groupName);
            getUsers(group);

            String query = "select count(*) from user where " + User.GROUP + " = ? ";
            long total = userDao.queryValue(query, group.getId().toString());
            assertEquals(2,total);

            query = "select count(*) from user where " + User.NAME + " = ? ";
            total = userDao.queryValue(query, userName);
            assertEquals(1, total);

        } catch (Exception ex){
            log.log(Level.ALL, ex.getMessage());
        }
    }

}
