package flowengine.core.configuration;

import flowengine.AppConfigurator;
import flowengine.annotations.webservice.FEWebService;
import flowengine.core.webservice.WebServiceAdapter;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import retrofit.http.GET;

/**
 * Unit test for simple FlowEngine.
 */
public class AppConfigurationTest
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */

    public AppConfigurationTest(String testName)
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppConfigurationTest.class );
    }

    private AppConfigurator configurator;
    private WebServiceAdapter adapter1;
    private WebServiceAdapter adapter2;
    private WebServiceAdapter adapter3;

    protected void setUp() {
        adapter1 = createAdapter("1");
        adapter2 = createAdapter("2");
        adapter2 = createAdapter("3");
    }

    @FEWebService
    public interface TestWebApi {
        @GET("/get/")
        long get();
    }

    private WebServiceAdapter createAdapter(String endpoint) {
        return WebServiceAdapter.startBuilding()
                .setEndpoint(endpoint)
                .build();
    }

    /**
     * Rigourous Test :-)
     */
    public void testAdapterConfigurator() {
        configurator = new AppConfigurator() {
            @Override
            public void createWebAdapters() {
                registerWebAdapter("1", adapter1);
                registerWebAdapter("2", adapter1);
                registerWebAdapter("3", adapter1);
            }

            @Override
            public void createWebServices() {
                registerWebService("1", TestWebApi.class);
                registerWebService("2", TestWebApi.class);
                registerWebService("3", TestWebApi.class);
            }

            @Override
            public void registerSingletons() {
                registerSingleton(TestSingletionInterface.class, TestSingletonImplementor.class);
            }
        };
        configurator.createWebAdapters();
        configurator.createWebServices();
        configurator.registerSingletons();
        assertEquals(3, configurator.getRegisteredWebAdapters().size());
        assertEquals(3, configurator.getRegisteredWebServices().size());
        assertEquals(1, configurator.getRegisteredSingletons().size());
    }
}
