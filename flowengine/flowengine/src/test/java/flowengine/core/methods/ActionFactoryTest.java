package flowengine.core.methods;

import flowengine.annotations.flow.*;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.lang.annotation.Annotation;

/**
 * Created by Ignacio on 19/04/2015.
 */

public class ActionFactoryTest extends TestCase
{
    public interface TestInterface extends Flow {

        @flowengine.annotations.flow.methods.Next
        public void testNext();

        public void testNotAnnotated();
    }

    public class TestClass implements TestInterface {

        @Override
        public void testNext() {

        }

        @Override
        public void testNotAnnotated() {

        }

        @Override
        public Class<? extends Annotation> annotationType() {
            return null;
        }
    }

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public ActionFactoryTest(String testName)
    {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( ActionFactoryTest.class );
    }

    /**
     * Event factory returns Next for Next annotated method
     */
    public void testEventFactoryReturnsNextForNextAnnotatedMethod() {
        testEventEquity("testNext", flowengine.core.methods.Next.class.getName());
    }

    public void testEventFactoryReturnsResumeForResumeAnnotatedMethod() {
        testEventEquity("resume", flowengine.core.methods.Resume.class.getName());
    }

    public void testEventFactoryReturnsBackForBackAnnotatedMethod() {
        testEventEquity("back", flowengine.core.methods.Back.class.getName());
    }

    public void testEventFactoryReturnsRestartForRestartAnnotatedMethod() {
        testEventEquity("restart", flowengine.core.methods.Restart.class.getName());
    }

    public void testEventFactoryReturnsNullForNonAnnotatedMethod() {
        try {
            Action event = ActionFactory.getInstance().getMethod(TestInterface.class.getMethod("testNotAnnotated"), null, null, null, null);
            assertNull(event);
        } catch (NoSuchMethodException ex) {
        }
    }

    private void testEventEquity(String methodName, String expectedClassName) {
        try {
            Action event = ActionFactory.getInstance().getMethod(TestInterface.class.getMethod(methodName), null, null, null, null);
            assertNotNull(event);
            assertEquals(expectedClassName, event.getClass().getName());
        } catch (NoSuchMethodException ex) {
        }
    }
}
