package flowengine.core.methods;

import flowengine.core.StorageHandler;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ignacio on 17/05/2015.
 */
public class SetModelTest extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public SetModelTest(String testName)
    {
        super(testName);
    }

    public class TestSetMethodClass {

        @flowengine.annotations.flow.methods.SetModel("test")
        public void setMethod(String model){}
    }

    public class TestStorageHandler implements StorageHandler {

        private Map<String, Object> testMap = new HashMap<String, Object>();

        public Map<String, Object> getTestMap() {
            return testMap;
        }

        public void setTestMap(Map<String, Object> map) {
            testMap = map;
        }

        @Override
        public <T> T getModel(String key) {
            return (T)testMap.get(key);
        }

        @Override
        public <T> void setModel(String key, T value) {
            testMap.put(key, value);
        }

        @Override
        public Map<String, ? extends Object> getModels() {
            return testMap;
        }
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( ActionFactoryTest.class );
    }

    public void testSetMethod() {
        TestStorageHandler handler = new TestStorageHandler();
        try {
            SetModel testInstance = new SetModel(TestSetMethodClass.class.getMethod("setMethod", String.class), null, null, null, null, "test", handler);
            testInstance.invoke();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        assertTrue(handler.getTestMap().containsKey("test"));
        assertNotNull(handler.getTestMap().get("test"));
        assertEquals("test", handler.getTestMap().get("test"));
    }
}
