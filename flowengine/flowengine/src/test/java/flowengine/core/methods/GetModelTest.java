package flowengine.core.methods;

import flowengine.core.StorageHandler;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ignacio on 17/05/2015.
 */
public class GetModelTest extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public GetModelTest(String testName)
    {
        super(testName);
    }

    public interface TestGetMethodClass {


        @flowengine.annotations.flow.methods.GetModel("test")
        public void failGetMethod();

        @flowengine.annotations.flow.methods.GetModel("test")
        public String getMethod();
    }

    public class TestStorageHandler implements StorageHandler {

        private Map<String, Object> testMap = new HashMap<String, Object>();

        public Map<String, Object> getTestMap() {
            return testMap;
        }

        public void setTestMap(Map<String, Object> map) {
            testMap = map;
        }

        @Override
        public <T> T getModel(String key) {
            return (T)testMap.get(key);
        }

        @Override
        public <T> void setModel(String key, T value) {
            testMap.put(key, value);
        }

        @Override
        public Map<String, ? extends Object> getModels() {
            return testMap;
        }

    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( ActionFactoryTest.class );
    }

    public void testGetMethod() {
        TestStorageHandler handler = new TestStorageHandler();
        handler.getTestMap().put("test", "test");
        try {
            GetModel<String> testInstance = new GetModel<String>(null, TestGetMethodClass.class.getMethod("getMethod"), null, null, null, handler);
            String result = testInstance.invoke();
            assertNotNull(result);
            assertEquals("test", result);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

    }

    public void testGetMethodAnnotatesVoidMethod() {
        TestStorageHandler handler = new TestStorageHandler();
        try {
            GetModel<String> testInstance = new GetModel<String>(null, TestGetMethodClass.class.getMethod("failGetMethod"), null, null, null, handler);
            String result = testInstance.invoke();
            assertFalse("Invocation did not fail when the annotated method did not return value" , true);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (RuntimeException e) {
            assertEquals("Annotated method must return a value", e.getMessage());
        }
    }
}
