package flowengine.core.injection;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple FlowEngine.
 */
public class IoCContainerTest
    extends TestCase {

    public interface TestInjectionInterface {

    }

    public interface TestInjectionInterface2 {

    }

    public class TestImplementor implements TestInjectionInterface {

    }

    public class TestImplementor2 implements TestInjectionInterface2 {

    }

    public class CircularTestImplementor implements TestInjectionInterface {
        public TestInjectionInterface2 implementor;
    }

    public class CircularTestImplementor2 implements TestInjectionInterface2 {
        public TestInjectionInterface implementor;
    }

    public class TestSingle implements TestInjectionInterface, TestInjectionInterface2 {

    }

    public class TestClass {

        public TestInjectionInterface testInjection;

        public TestInjectionInterface2 testInjection2;
    }

    public IoCContainerTest(String testName) {
        super(testName);
    }

    protected void setUp() throws Exception {
        testContainer = new BaseIoCContainer();
    }

    private IoCContainer testContainer;

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(IoCContainerTest.class);
    }

    /**
     * Rigourous Test :-)
     */
    public void testInjectionMethod() {
        testContainer.registerSingleton(TestInjectionInterface.class, new TestImplementor());
        TestClass target = new TestClass();
        testContainer.injectSingletons(target);
        assertNotNull(target.testInjection);
    }

    public void testTwoImplementorTwoInterfacesInjectionMethod() {
        TestImplementor implementor = new TestImplementor();
        TestImplementor2 implementor2 = new TestImplementor2();
        testContainer.registerSingleton(TestInjectionInterface.class, implementor);
        testContainer.registerSingleton(TestInjectionInterface2.class, implementor2);
        TestClass target = new TestClass();
        testContainer.injectSingletons(target);
        assertNotNull(target.testInjection);
        assertNotNull(target.testInjection2);
    }


    public void testOneImplementorTwoInterfacesInjectionMethod() {
        TestSingle implementor = new TestSingle();
        testContainer.registerSingleton(TestInjectionInterface.class, implementor);
        testContainer.registerSingleton(TestInjectionInterface2.class, implementor);
        TestClass target = new TestClass();
        testContainer.injectSingletons(target);
        assertNotNull(target.testInjection);
        assertNotNull(target.testInjection2);
    }


    public void testCircularResolution() {
        CircularTestImplementor implementor = new CircularTestImplementor();
        CircularTestImplementor2 implementor2 = new CircularTestImplementor2();
        testContainer.registerSingleton(TestInjectionInterface.class, implementor);
        testContainer.registerSingleton(TestInjectionInterface2.class, implementor2);
        testContainer.resolveInjectedSingletonDependencies();
        TestClass target = new TestClass();
        testContainer.injectSingletons(target);
        assertNotNull(target.testInjection);
        assertNotNull(implementor.implementor);
        assertNotNull(target.testInjection2);
        assertNotNull(implementor2.implementor);
    }
}
