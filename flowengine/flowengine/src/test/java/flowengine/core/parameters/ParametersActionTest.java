package flowengine.core.parameters;

import flowengine.Flow;
import flowengine.annotations.flow.parameters.Argument;
import flowengine.core.methods.Action;
import flowengine.core.methods.ActionFactory;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by florencia on 26/04/15.
 */
public class ParametersActionTest extends TestCase
{
    public class TestClass {
        String name;

        public TestClass(String name) {
            this.name = name;
        }
    }

    protected static final String PARAMETER_NAME_STRING = "testString";
    protected static final String PARAMETER_NAME_CLASS = "testClass";

    public interface TestInterface extends Flow {

        @flowengine.annotations.flow.methods.Next
        public void testNext(@Argument(PARAMETER_NAME_STRING) String testValue);

        @flowengine.annotations.flow.methods.Next
        public void testNext(@Argument(PARAMETER_NAME_CLASS) TestClass testValue);

        @flowengine.annotations.flow.methods.Next
        public void testNext(@Argument(PARAMETER_NAME_STRING) String testValueString, @Argument(PARAMETER_NAME_CLASS) TestClass testValueClass );

    }

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public ParametersActionTest(String testName)
    {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( ParametersActionTest.class );
    }

    public void testStringParameterMethod() {
        try {
            Method interfaceMethod = TestInterface.class.getMethod("testNext", String.class);
            String value = "testValue";
            Object[] args = {value};
            List<Parameter> expectedParameters = new ArrayList<>();
            expectedParameters.add(new NamedParameter(PARAMETER_NAME_STRING,value,String.class));
            testParameterMethod(interfaceMethod, args, expectedParameters);
        } catch (NoSuchMethodException ex) {
        }
    }

    public void testClassParameterMethod() {
        try {
            Method interfaceMethod = TestInterface.class.getMethod("testNext", TestClass.class);
            TestClass value = new TestClass("testValue");
            Object[] args = {value};
            List<Parameter> expectedParameters = new ArrayList<>();
            expectedParameters.add(new NamedParameter(PARAMETER_NAME_CLASS,value,TestClass.class));
            testParameterMethod(interfaceMethod, args, expectedParameters);
        } catch (NoSuchMethodException ex) {
        }
    }

    public void testMultipleParameterMethod() {
        try {
            Method interfaceMethod = TestInterface.class.getMethod("testNext", String.class, TestClass.class);
            String stringValue = "testValue";
            TestClass classValue = new TestClass("testValue");
            Object[] args = {stringValue, classValue};
            List<Parameter> expectedParameters = new ArrayList<>();
            expectedParameters.add(new NamedParameter(PARAMETER_NAME_STRING,stringValue,String.class));
            expectedParameters.add(new NamedParameter(PARAMETER_NAME_CLASS,classValue,TestClass.class));
            testParameterMethod(interfaceMethod, args, expectedParameters);
        } catch (NoSuchMethodException ex) {
        }
    }

    private void testParameterMethod(Method interfaceMethod, Object[] args, List<Parameter> expectedParameters){
        Action event = ActionFactory.getInstance().getMethod(interfaceMethod, null, null, null, args);
        List<Parameter> parameters = event.getParametersWithAnnotation(Argument.class);
        assert (parameters.equals(expectedParameters));
    }

}
