package flowengine.core.parameters;

import android.os.Bundle;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.junit.Before;
import org.junit.Test;
import org.robolectric.annotation.Config;

import static org.junit.Assert.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by florencia on 25/04/15.
 */
@RunWith(RobolectricTestRunner.class)
@Config(manifest=Config.NONE)
public class BundleBuilderTest
{

    protected static final String STRING_PARAMETER_VALUE = "testStringValue";
    protected static final Integer INTEGER_PARAMETER_VALUE = 10;
    protected static final Double DOUBLE_PARAMETER_VALUE = 10.5;
    protected static final Long LONG_PARAMETER_VALUE = 105L;

    protected static final String PARAMETER_NAME = "testName";

    public class NotSeriableClass {
        String name;

        public NotSeriableClass(String name) {
            this.name = name;
        }
    }

    public class SeriableClass implements Serializable {
        String name;

        public SeriableClass(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    Parameter stringParameter = new NamedParameter(PARAMETER_NAME,STRING_PARAMETER_VALUE,String.class);
    Parameter intParameter = new NamedParameter(PARAMETER_NAME, INTEGER_PARAMETER_VALUE ,Integer.class);
    Parameter doubleParameter = new NamedParameter(PARAMETER_NAME,DOUBLE_PARAMETER_VALUE,Double.class);
    Parameter longParameter = new NamedParameter(PARAMETER_NAME,LONG_PARAMETER_VALUE,Long.class);
    Parameter serializableParameter = new NamedParameter(PARAMETER_NAME,new SeriableClass("testName"),SeriableClass.class);
    Parameter notSerializableParameter = new NamedParameter(PARAMETER_NAME,new NotSeriableClass("testName"),NotSeriableClass.class);


    private BundleBuilder bundleBuilder;

    @Before
    public void setUp() {
        bundleBuilder = BundleBuilder.getInstance();
    }

    @Test
    public void testBundleBuilderWithStringParameter() {
        Bundle bundle = getBundle(stringParameter);
        assertEquals(bundle.get(PARAMETER_NAME), STRING_PARAMETER_VALUE);
        assertEquals(bundle.get(PARAMETER_NAME).getClass(), STRING_PARAMETER_VALUE.getClass());
    }

    @Test
    public void testBundleBuilderWithIntegerParameter() {
        Bundle bundle = getBundle(intParameter);
        assertEquals(bundle.get(PARAMETER_NAME), INTEGER_PARAMETER_VALUE);
        assertEquals(bundle.get(PARAMETER_NAME).getClass(), INTEGER_PARAMETER_VALUE.getClass());
    }

    @Test
    public void testBundleBuilderWithDoubleParameter() {
        Bundle bundle = getBundle(doubleParameter);
        assertEquals(bundle.get(PARAMETER_NAME), DOUBLE_PARAMETER_VALUE);
        assertEquals(bundle.get(PARAMETER_NAME).getClass(), DOUBLE_PARAMETER_VALUE.getClass());
    }

    @Test
    public void testBundleBuilderWithLongParameter() {
        Bundle bundle = getBundle(longParameter);
        assertEquals(bundle.get(PARAMETER_NAME), LONG_PARAMETER_VALUE);
        assertEquals(bundle.get(PARAMETER_NAME).getClass(), LONG_PARAMETER_VALUE.getClass());
    }

    @Test
    public void testBundleBuilderWithSerializableParameter() {
        Bundle bundle = getBundle(serializableParameter);
        assertEquals(bundle.get(PARAMETER_NAME).getClass(), SeriableClass.class);
    }

    @Test
    public void testBundleBuilderWithNotSerializableParameter() {
        Bundle bundle = getBundle(notSerializableParameter);
        assertNull(bundle.get(PARAMETER_NAME));
    }

    @Test
    public void testBundleBuilderWithStringList(){
        List<String> stringList = new ArrayList<>();
        stringList.add(STRING_PARAMETER_VALUE);
        Parameter parameter = new NamedParameter(PARAMETER_NAME,stringList, stringList.getClass());
        Bundle bundle = getBundle(parameter);
        assertEquals(bundle.get(PARAMETER_NAME),stringList);
    }

    private Bundle getBundle(Parameter parameter) {
        List<Parameter> parameters = new ArrayList<>();
        parameters.add(parameter);
        return bundleBuilder.newInstance(parameters);
    }

}
