package flowengine;

import flowengine.annotations.webservice.FEWebService;
import flowengine.core.utils.AnnotationSearcher;
import flowengine.core.webservice.WebServiceAdapter;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by florencia on 10/07/15.
 */
public abstract class AppConfigurator {

    private Map<String, List<Class<?>>> webServices = new HashMap<>();
    private Map<String, WebServiceAdapter> webAdapters = new HashMap<>();
    private Map<Class<?>, Object> registeredSingletons = new HashMap<>();

    public AppConfigurator() {

    }

    public abstract void createWebAdapters();

    public abstract void createWebServices();

    public abstract void registerSingletons();

    protected final void registerWebService(String adapter, Class<?> webServiceClass) {
        if (AnnotationSearcher.findAnnotationForClass(webServiceClass, FEWebService.class) == null) {
            throw new RuntimeException(webServiceClass.toString() + " is not annotated as a WebService.");
        }
        if (!webServices.containsKey(adapter))
            webServices.put(adapter, new LinkedList<Class<?>>());
        if (!webServices.get(adapter).contains(webServiceClass)) {
            webServices.get(adapter).add(webServiceClass);
        }
    }

    protected final void registerWebAdapter(String adapter, WebServiceAdapter webAdapter) {
        webAdapters.put(adapter, webAdapter);
    }

    protected final <I> void registerSingleton(Class<I> representingInterface, Class<? extends I> implementor)  {
        try {
            registeredSingletons.put(representingInterface, implementor.newInstance());
        } catch (InstantiationException e) {
            throw new RuntimeException("Implementor of type " + implementor.toString() + " cannot be instantiated. Remember that it must have a public zero argument constructor");
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Implementor of type " + implementor.toString() + " cannot be instantiated. Remember that it must have a public zero argument constructor");
        }
    }

    public final Map<String, List<Class<?>>> getRegisteredWebServices() {
        return webServices;
    }

    public final Map<String, WebServiceAdapter> getRegisteredWebAdapters() {
        return webAdapters;
    }

    public final Map<Class<?>, Object> getRegisteredSingletons() {
        return registeredSingletons;
    }
}
