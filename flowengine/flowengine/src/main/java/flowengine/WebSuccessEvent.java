package flowengine;

import flowengine.core.webservice.Response;

public class WebSuccessEvent<T> extends WebEvent<T> {
    Response response;

    final void setResponse(Response response) {
        this.response = response;
    }

    Response getResponse() {
        return response;
    }
}
