package flowengine.core.persistence.strategies;

import com.j256.ormlite.dao.Dao;
import flowengine.core.persistence.DatabaseInMemoryHelper;


/**
 * Strategy to handle the operations on an in-memory database, used by the data access objects. Using ORMLite.
 */
public class MemoryPersistenceStrategy extends DatabasePersistence implements PersistenceStrategy {

    private DatabaseInMemoryHelper databaseHelper;

    /**
     * Constructor of Memory Persistence. Creates the {@link DatabaseInMemoryHelper}
     *
     * @param databaseName
     *            Name of the database.
     */
    public MemoryPersistenceStrategy(String databaseName) {

        if (databaseHelper == null)
            databaseHelper = new DatabaseInMemoryHelper(databaseName);

    }

    @Override
    public void setPersistableClass(Class<?> cls) {
        databaseHelper.createDao(cls);
    }

    @Override
    public void close() {
        databaseHelper.close();
    }

    /**
     * Create {@link com.j256.ormlite.dao.Dao} for the class pass in. Provided by {@link DatabaseInMemoryHelper}.
     *
     * @param cls
     *          Class of the dao to create.
     * @return The {@link com.j256.ormlite.dao.Dao} created.
     */
    @Override
    public Dao getDaoWithClass(Class<?> cls) {
        return databaseHelper.getDaoWithClass(cls);
    }

}
