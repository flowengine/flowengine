package flowengine.core.persistence;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * SQLite database open helper to help manage when the application needs to create or upgrade its database.
 *
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    private Map<Class<?>, Dao<?,?>> daos;

    /**
     * Create the SQLite database open helper
     *
     * @param context
     *            Associated content from the application. This is needed to locate the database.
     * @param databaseName
     *            Name of the database.
     */
    public DatabaseHelper(final Context context, String databaseName) {
        super(context, databaseName, null, DATABASE_VERSION);
        daos = new HashMap<>();
    }

    /**
     * What to do when your database needs to be created. Usually this entails creating the tables and loading any
     * initial data.
     *
     * <p>
     * <b>NOTE:</b> You should use the connectionSource argument that is passed into this method call or the one
     * returned by getConnectionSource(). If you use your own, a recursive call or other unexpected results may result.
     * </p>
     *
     * @param db
     *            Database being created.
     * @param connectionSource
     *            To use get connections to the database to be created.
     */
    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {

            for(Class<?> daoClass: daos.keySet())
                TableUtils.createTableIfNotExists(connectionSource, daoClass);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * What to do when your database needs to be updated. This could mean careful migration of old data to new data.
     * Maybe adding or deleting database columns, etc..
     *
     * <p>
     * <b>NOTE:</b> You should use the connectionSource argument that is passed into this method call or the one
     * returned by getConnectionSource(). If you use your own, a recursive call or other unexpected results may result.
     * </p>
     *
     * @param db
     *            Database being upgraded.
     * @param connectionSource
     *            To use get connections to the database to be updated.
     * @param oldVersion
     *            The version of the current database so we can know what to do to the database.
     * @param newVersion
     *            The version that we are upgrading the database to.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {

        try {
            for(Class<?> daoClass: daos.keySet())
                TableUtils.dropTable(connectionSource, daoClass, true);

            onCreate(db, connectionSource);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Close any open connections.
     */
    @Override
    public void close() {
        super.close();
        daos.clear();
    }

    /**
     * Get the current dao for the class passed in.
     *
     * @param clazz
     *          Class of the dao.
     * @return Dao
     *      Current Dao for the class passed in.
     */
    public Dao getDaoWithClass(Class<?> clazz) {
        return daos.get(clazz);
    }

    /**
     * Create {@link com.j256.ormlite.dao.Dao} for the class passed in.
     *
     * @param clazz
     *          Class of the dao to be created.
     * @throws SQLException
     *          on any SQL problems creating the dao.
     */
    public void createDao(Class<?> clazz) throws SQLException {
        try{
            Dao dao = getDao(clazz);
            daos.put(clazz,dao);

        } catch (SQLException ex){
            throw new RuntimeException(ex);
        }
    }
}
