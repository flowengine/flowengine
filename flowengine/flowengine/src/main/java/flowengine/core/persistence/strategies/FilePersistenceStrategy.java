package flowengine.core.persistence.strategies;

import android.content.Context;
import flowengine.core.persistence.JSONParser;
import flowengine.core.utils.FileParser;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Strategy to handle the persistence of objects on a file, using JSON.
 */
public class FilePersistenceStrategy implements PersistenceStrategy {

    private JSONParser parser;
    private FileParser fileParser;

    private final static String METHOD_NOT_SUPPORTED_MESSAGE = "This method is not supported by file persistence.";

    /**
     * Constructor of File Persistence. Uses {@link com.google.gson}
     *
     * @param context
     *            Associated content from the application. This is needed to locate the database.
     * @param filename
     *            Name of the file.
     */
    public FilePersistenceStrategy (Context context,String filename) {
        this.parser = new JSONParser();
        this.fileParser = new FileParser(context, filename);
    }

    @Override
    public List<?> getAll(Class<?> cls) {
        try {
            String fileData = fileParser.readFromFile();
            if(fileData.isEmpty())
                return null;

            return parser.getAllWithClass(cls, fileData);
        }
        catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void save(Object data) {
        try {
            String fileData = fileParser.readFromFile();
            if(fileData.isEmpty())
                fileData = parser.getNewPersistableObject();

            String result = parser.insertObjectIntoArray(data.getClass(), fileData, data);
            fileParser.writeToFile(result);
        }
        catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void setPersistableClass(Class<?> cls) {}

    @Override
    public void close() {}

    /**
     * Unsupported Method.
     *
     * @throws UnsupportedOperationException
     *          Method not supported by File Persistence
     */
    @Override
    public Object get(Class<?> cls, Object id) throws Exception {
        throw new UnsupportedOperationException(METHOD_NOT_SUPPORTED_MESSAGE);
    }

    /**
     * Unsupported Method.
     *
     * @throws UnsupportedOperationException
     *          Method not supported by File Persistence
     */
    @Override
    public void update(Object data) {
        throw new UnsupportedOperationException(METHOD_NOT_SUPPORTED_MESSAGE);
    }

    /**
     * Unsupported Method.
     *
     * @throws UnsupportedOperationException
     *          Method not supported by File Persistence
     */
    @Override
    public void delete(Object data) {
        throw new UnsupportedOperationException(METHOD_NOT_SUPPORTED_MESSAGE);
    }

    /**
     * Unsupported Method.
     *
     * @throws UnsupportedOperationException
     *          Method not supported by File Persistence
     */
    @Override
    public void deleteAll(Class<?> cls, Collection<Object> collection) {
        throw new UnsupportedOperationException(METHOD_NOT_SUPPORTED_MESSAGE);
    }

    /**
     * Unsupported Method.
     *
     * @throws UnsupportedOperationException
     *          Method not supported by File Persistence
     */
    @Override
    public Long count(Class<?> cls) {
        throw new UnsupportedOperationException(METHOD_NOT_SUPPORTED_MESSAGE);
    }

    /**
     * Unsupported Method.
     *
     * @throws UnsupportedOperationException
     *          Method not supported by File Persistence
     */
    @Override
    public List<Object> findAllBy(Class<?> cls, String fieldName, Object value) {
        throw new UnsupportedOperationException(METHOD_NOT_SUPPORTED_MESSAGE);
    }

    /**
     * Unsupported Method.
     *
     * @throws UnsupportedOperationException
     *          Method not supported by File Persistence
     */
    @Override
    public Object findBy(Class<?> cls, String fieldName, Object value) {
        throw new UnsupportedOperationException(METHOD_NOT_SUPPORTED_MESSAGE);
    }

    /**
     * Unsupported Method.
     *
     * @throws UnsupportedOperationException
     *          Method not supported by File Persistence
     */
    @Override
    public List<Object> findAllBy(Class<?> cls, Map<String, Object> fieldValues) {
        throw new UnsupportedOperationException(METHOD_NOT_SUPPORTED_MESSAGE);
    }

    /**
     * Unsupported Method.
     *
     * @throws UnsupportedOperationException
     *          Method not supported by File Persistence
     */
    @Override
    public Object findBy(Class<?> cls, Map<String, Object> fieldValues) {
        throw new UnsupportedOperationException(METHOD_NOT_SUPPORTED_MESSAGE);
    }

    /**
     * Unsupported Method.
     *
     * @throws UnsupportedOperationException
     *          Method not supported by File Persistence
     */
    @Override
    public List<Object> query(Class<?> cls, String query, String... arguments) {
        throw new UnsupportedOperationException(METHOD_NOT_SUPPORTED_MESSAGE);
    }

    /**
     * Unsupported Method.
     *
     * @throws UnsupportedOperationException
     *          Method not supported by File Persistence
     */
    @Override
    public Long queryValue(Class<?> cls, String query, String... arguments) {
        throw new UnsupportedOperationException(METHOD_NOT_SUPPORTED_MESSAGE);
    }

}
