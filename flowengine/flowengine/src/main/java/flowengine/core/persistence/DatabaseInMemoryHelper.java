package flowengine.core.persistence;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * SQLite database open helper to help manage when the application needs to create or upgrade its in-memory-database.
 *
 */
public class DatabaseInMemoryHelper {

    private ConnectionSource connectionSource;

    private Map<Class<?>, Dao<?,?>> daos;

    /**
     * Create the {@link JdbcConnectionSource}.
     *
     * @param databaseName
     *            Name of the database.
     */
    public DatabaseInMemoryHelper(String databaseName) {

        daos = new HashMap<>();

        String dbName = "jdbc:h2:mem:" + databaseName;
        if (connectionSource == null){
            try {
                connectionSource = new JdbcConnectionSource(dbName);
            } catch (SQLException e) {
                throw new RuntimeException("Problems initializing database objects", e);
            }
        }

    }

    /**
     * Get the current dao for the class passed in.
     *
     * @param clazz
     *          Class of the dao.
     * @return Dao
     *      Current Dao for the class passed in.
     */
    public Dao getDaoWithClass(Class<?> clazz) {
        return daos.get(clazz);
    }

    /**
     * Create {@link com.j256.ormlite.dao.Dao} for the class passed in.
     *
     * @param clazz
     *          Class of the dao to be created.
     * @throws RuntimeException
     *          on any problems creating the dao.
     */
    public void createDao(Class<?> clazz) throws RuntimeException {
        try{
            Dao dao = DaoManager.createDao(connectionSource, clazz);
            if(daos.get(clazz) == null){
                TableUtils.createTableIfNotExists(connectionSource, clazz);
                daos.put(clazz,dao);
            }

        } catch (SQLException ex){
            throw new RuntimeException(ex);
        }
    }

    /**
     * Close any open connections.
     */
    public void close() {
        try{
            connectionSource.close();
            daos.clear();

        } catch (SQLException ex){
            throw new RuntimeException(ex);
        }
    }


}
