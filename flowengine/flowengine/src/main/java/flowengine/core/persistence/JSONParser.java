package flowengine.core.persistence;

import com.google.gson.*;

import java.util.List;

/**
 * Parser of JSON data. Uses {@link com.google.gson.Gson}.
 */
public class JSONParser {

    private static String FORMAT_DATE = "yyyy-MM-dd'T'HH:mm:ss.SSS zzz";
    private Gson gson;
    private JsonParser jsonParser;

    /**
     * Constructor for JSON Parser.
     */
    public JSONParser(){
        gson = new GsonBuilder().setDateFormat(FORMAT_DATE).create();
        jsonParser = new JsonParser();
    }

    /**
     * Return a new json object.
     *
     * @return String
     *      A string of a new {@link JsonObject}.
     */
    public String getNewPersistableObject(){
        return (new JsonObject()).toString();
    }

    /**
     * Convert an Object into a json string.
     *
     * @param data
     *      The data to convert into json.
     * @return String
     *      The data converted to json string.
     */
    public String getParsedObject(Object data){
        return gson.toJson(data);
    }

    /**
     * Retrieve all the objects of class cls from the json data passed in.
     *
     * @param cls
     *      Class of the objects to retrieve from the data.
     * @param data
     *      The json data containing objects of different classes.
     * @return
     *       A list of objects of the class passed in, contained in data.
     * @throws Exception
     *      on any problems retrieving the objects.
     */
    public List<?> getAllWithClass(Class<?> cls, String data) throws Exception {

        try{

            JsonObject jsonObject = jsonParser.parse(data).getAsJsonObject();
            JsonArray jsonClassArray = jsonObject.getAsJsonArray(cls.getName());

            return fromJsonList(jsonClassArray.toString(),cls);

        } catch (Exception ex){
            throw ex;
        }
    }

    private List<?> fromJsonList(String json, Class<?> cls) {
        return gson.fromJson(json, new JSONList(cls));
    }

    /**
     * Insert a new object into a json array that corresponds to its class. If there is not an array
     * with this class, then it is created.
     *
     * @param cls
     *          The class of the new object to be inserted.
     * @param data
     *          The data that contains all the arrays, each one has objects of different classes.
     * @param newObject
     *          The new object to be inserted in the corresponding array depending on its class.
     * @return  String
     *      The data containing the new object.
     * @throws Exception
     *      on any problems inserting the new object.
     */
    public String insertObjectIntoArray(Class<?> cls, String data, Object newObject) throws Exception {

        try{

            JsonObject jsonObject = jsonParser.parse(data).getAsJsonObject();
            JsonArray jsonClassArray = jsonObject.getAsJsonArray(cls.getName());
            JsonElement jsonNewObject = gson.toJsonTree(newObject);

            if(jsonClassArray != null){
                jsonClassArray.add(jsonNewObject);

            } else {

                JsonArray newJsonArray = new JsonArray();
                newJsonArray.add(jsonNewObject);
                jsonObject.add(cls.getName(), newJsonArray);
            }

            return jsonObject.toString();

        } catch (Exception ex){
            throw ex;
        }
    }

}
