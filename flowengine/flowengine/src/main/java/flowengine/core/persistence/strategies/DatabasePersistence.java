package flowengine.core.persistence.strategies;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Strategy to handle the operations used by Daos objects for SQL persistence type, which includes Memory and
 * Database.
 */
public abstract class DatabasePersistence {

    /**
     * Create {@link com.j256.ormlite.dao.Dao} for the class pass in.
     *
     * @param cls
     *          Class of the dao to create.
     * @return The {@link com.j256.ormlite.dao.Dao} created.
     */
    public abstract Dao getDaoWithClass(Class<?> cls) ;

    /**
     * Retrieves all the objects persisted corresponding to the class passed in.
     *
     * @param cls
     *          Class of the items to be retrieved.
     * @return A list of all of the objects persisted of the same class.
     * @throws Exception
     *             on any problems retrieving the data.
     */
    public List<?> getAll(Class<?> cls) throws Exception {
        try{
            Dao dao = getDaoWithClass(cls);
            return dao.queryForAll();

        } catch (SQLException ex){
            throw new RuntimeException(ex);
        }
    }

    /**
     * Retrieves an object associated with a specific ID.
     *
     * @param cls
     *            Class of the items to be retrieved.
     * @param id
     *            Identifier that matches a specific row in the database to find and return.
     * @return The object that has the ID field which equals id or null if no matches.
     * @throws Exception
     *             on any SQL problems or if more than 1 item with the id are found in the database.
     */
    public Object get(Class<?> cls, Object id) throws Exception {
        try{
            Dao dao = getDaoWithClass(cls);
            return dao.queryForId(id);
        } catch (SQLException ex){
            throw new RuntimeException(ex);
        }
    }

    /**
     * Create an item to persist from an object of class T.
     *
     * @param data
     *            The data item that we are creating to persist.
     * @throws Exception
     *             on any problems creating the data.
     */
    public void save(Object data) throws Exception {
        try{
            Dao dao = getDaoWithClass(data.getClass());
            dao.createOrUpdate(data);

        } catch (SQLException ex){
            throw new RuntimeException(ex);
        }
    }

    /**
     * Store the fields from an object to the database row corresponding to the id of the data parameter. This way
     * you can persist the changes made to an object, except for the id field.
     *
     * <p>
     * NOTE: This will not save changes made to foreign objects or to foreign collections.
     * </p>
     *
     * @param data
     *            The data item that is been updated in the database.
     * @throws Exception
     *             on any SQL problems or if there is only an ID field in the object.
     */
    public void update(Object data) throws Exception {
        try{
            Dao dao = getDaoWithClass(data.getClass());
            dao.update(data);

        } catch (SQLException ex){
            throw new RuntimeException(ex);
        }
    }

    /**
     * Delete the database row corresponding to the id of the data parameter.
     *
     * @param data
     *            The data item that is been deleted from the database.
     * @throws Exception
     *             on any SQL problems.
     */
    public void delete(Object data) throws Exception {
        try{
            Dao dao = getDaoWithClass(data.getClass());
            dao.delete(data);

        } catch (SQLException ex){
            throw new RuntimeException(ex);
        }
    }

    /**
     * Delete a collection of objects from the database, the rows to be removed correspond to the ids of the objects
     * in the collection parameter.
     *
     * @param cls
     *            The class of the items to be deleted.
     * @param collection
     *            A collection of data items to be deleted.
     * @throws Exception
     *             on any SQL problems.
     */
    public void deleteAll(Class<?> cls, Collection<Object> collection) throws Exception {
        try{
            Dao dao = getDaoWithClass(cls);
            dao.delete(collection);

        } catch (SQLException ex){
            throw new RuntimeException(ex);
        }
    }

    /**
     * Returns the number of rows in the table associated with the data class.
     *
     * @param cls
     *            Class of the items to count.
     * @return The number of rows in the table associated with the data class.
     * @throws Exception
     *             on any SQL problems.
     */
    public Long count(Class<?> cls) throws Exception {
        try{
            Dao dao = getDaoWithClass(cls);
            return dao.countOf();

        } catch (SQLException ex){
            throw new RuntimeException(ex);
        }
    }

    /**
     * Query for the items in the object table that match a simple where with a single field = value type of WHERE
     * clause.
     *
     * @param cls
     *            Class of the items to find.
     * @param fieldName
     *            The name of the field to be compared in the WHERE clause.
     * @param value
     *            The value of the field that has to match.
     * @return A list of the objects in the table that match the fieldName = value;
     * @throws Exception
     *             on any SQL problems.
     */
    public List<Object> findAllBy(Class<?> cls,String fieldName, Object value) throws Exception {
        try{
            Dao dao = getDaoWithClass(cls);
            return dao.queryForEq(fieldName, value);

        } catch (SQLException ex){
            throw new RuntimeException(ex);
        }
    }

    /**
     * Query for the item in the object table that match a simple where with a single field = value type of WHERE
     * clause. In case of having multiples rows matching, only the first object is returned.
     *
     * @param cls
     *            Class of the items to find.
     * @param fieldName
     *            The name of the field to be compared in the WHERE clause.
     * @param value
     *            The value of the field that has to match.
     * @return The first object in the table that matches the fieldName = value.
     * @throws Exception
     *             on any SQL problems.
     */
    public Object findBy(Class<?> cls, String fieldName, Object value) throws Exception {
        try{
            Dao dao = getDaoWithClass(cls);

            Map<String,Object> fieldValues = new HashMap();
            fieldValues.put(fieldName,value);

            PreparedQuery preparedQuery = preparedQueryWhere(dao, fieldValues);
            return dao.queryForFirst(preparedQuery);

        } catch (SQLException ex){
            throw new RuntimeException(ex);
        }
    }

    /**
     * Query for the rows in the database that matches all of the field to value entries from the map passed in.
     *
     * @param cls
     *            Class of the items to find.
     * @param fieldValues
     *            A map with the field name and the corresponding value to be matched.
     * @return A list of the objects in the table that match all the fields values passed in.
     * @throws Exception
     *             on any SQL problems.
     */
    public List<Object> findAllBy(Class<?> cls, Map<String,Object> fieldValues) throws Exception {
        try{
            Dao dao = getDaoWithClass(cls);
            return dao.queryForFieldValues(fieldValues);

        } catch (SQLException ex){
            throw new RuntimeException(ex);
        }
    }

    /**
     * Query for the row in the database that matches all of the field to value entries from the map passed in. In case
     * of having multiples rows matching, only the object of the first is returned.
     *
     * @param cls
     *            Class of the items to find.
     * @param fieldValues
     *            A map with the field name and the corresponding value to be matched.
     * @return The first object in the table that matches all the fields values passed in.
     * @throws Exception
     *             on any SQL problems.
     */
    public Object findBy(Class<?> cls, Map<String, Object> fieldValues) throws Exception {
        try{
            Dao dao = getDaoWithClass(cls);
            PreparedQuery preparedQuery = preparedQueryWhere(dao, fieldValues);
            return dao.queryForFirst(preparedQuery);

        } catch (SQLException ex){
            throw new RuntimeException(ex);
        }
    }

    /**
     * Query for the rows in the database that matches the SQL statement passed in.
     *
     * <p>
     *     Example of using this method
     *</p>
     * <pre>
     *     String query = &quot;select * from user where age &gt; ?&quot;;
     *     List&lt;User&gt; users = userDao.query(query,21);
     * </pre>
     *
     * @param cls
     *            Class of the items to find.
     * @param query
     *            The SQL query to be executed.
     * @param arguments
     *            The values of the fields that will be replaced the wildcards. The wildcards will be replace in the order
     *            of the parameters passed in.
     * @return The list of objects that matches the query executed.
     * @throws Exception
     *             on any SQL problems.
     */
    public List<Object> query(Class<?> cls,String query,  String... arguments) throws Exception {

        try{
            Dao dao = getDaoWithClass(cls);

            GenericRawResults rawResults = dao.queryRaw(query, dao.getRawRowMapper(), arguments);
            List<Object> results = rawResults.getResults();
            rawResults.close();
            return results;

        } catch (SQLException ex){
            throw new RuntimeException(ex);
        }
    }

    /**
     * Perform a query that returns a single value (usually an aggregate function like MAX or COUNT). If the query
     * does not return a single long value then it will throw a SQLException.
     *
     * <p>
     *     Example of using this method
     *</p>
     * <pre>
     *     String query = &quot;select count(*) from user where age &gt; ?&quot;;
     *     Long numberOfUsers = userDao.query(query,21);
     * </pre>
     *
     * @param cls
     *            Class of the items to find.
     * @param query
     *            The SQL query to be executed. It must return a single value.
     * @param arguments
     *            The values of the fields that will be replaced the wildcards. The wildcards will be replaced in the
     *            order of the parameters passed in.
     * @return The result of the SQL query executed.
     * @throws Exception
     *             on any SQL problems.
     */
    public Long queryValue(Class<?> cls,String query,  String... arguments) throws Exception {

        try{
            Dao dao = getDaoWithClass(cls);
            return dao.queryRawValue(query, arguments);

        } catch (SQLException ex){
            throw new RuntimeException(ex);
        }
    }

    private PreparedQuery preparedQueryWhere(Dao dao, Map<String,Object> fieldValues) throws SQLException {
        try{
            QueryBuilder queryBuilder = dao.queryBuilder();
            Where where = queryBuilder.where();

            for (Map.Entry<String, Object> fieldValue : fieldValues.entrySet()) {
                where.eq(fieldValue.getKey(), fieldValue.getValue());
            }
            where.and(fieldValues.size());

            return queryBuilder.prepare();
        } catch (SQLException ex){
            throw new RuntimeException(ex);
        }
    }

}
