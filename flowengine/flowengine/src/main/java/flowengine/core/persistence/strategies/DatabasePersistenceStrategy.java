package flowengine.core.persistence.strategies;

import android.content.Context;
import com.j256.ormlite.dao.Dao;
import flowengine.core.persistence.DatabaseHelper;

import java.sql.SQLException;

/**
 * Strategy to handle the operations on the database, used by the data access objects. Using ORMLite.
 */
public class DatabasePersistenceStrategy extends DatabasePersistence implements PersistenceStrategy {

    private DatabaseHelper databaseHelper;

    // TODO Ver si se puede implementar con OpenHelperManager.getHelper(context, DatabaseHelper.class)
    /**
     * Constructor of Database Persistence. Creates the {@link DatabaseHelper}
     *
     * @param context
     *            Associated content from the application. This is needed to locate the database.
     * @param databaseName
     *            Name of the database.
     */
    public DatabasePersistenceStrategy (Context context, String databaseName) {

        if (databaseHelper == null)
            databaseHelper = new DatabaseHelper(context, databaseName);

    }

    @Override
    public void setPersistableClass(Class<?> cls) {
        try{
            databaseHelper.createDao(cls);
        } catch (SQLException ex){
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void close() {
        databaseHelper.close();
    }

    /**
     * Create {@link com.j256.ormlite.dao.Dao} for the class pass in. Provided by {@link DatabaseHelper}.
     *
     * @param cls
     *          Class of the dao to create.
     * @return The {@link com.j256.ormlite.dao.Dao} created.
     */
    @Override
    public Dao getDaoWithClass(Class<?> cls) {
        return databaseHelper.getDaoWithClass(cls);
    }
}
