package flowengine.core.persistence;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Database Access Objects that handle the reading and writing of a class from the database. This interface can be used
 * only with SQL persistence, which includes the Database and Memory Persistence types.
 *
 * <p>
 * <b> NOTE: </b> If you are using this interface for File Persistence type, you will get an
 * UnsupportedOperationException when accessing these methods.
 * </p>
 *
 * @param <T>
 *            The class in which the dao will be operating on.
 * @param <ID>
 *            The class of the ID column associated with the class. If the T class does not have an ID, it can be used
 *            Void or Object instead.
 *
 */
public interface SQLDao<T,ID> extends Dao<T,ID> {

    /**
     * Retrieves an object associated with a specific ID.
     *
     * @param id
     *            Identifier that matches a specific row in the database to find and return.
     * @return The object that has the ID field which equals id or null if no matches.
     * @throws Exception
     *             on any SQL problems or if more than 1 item with the id are found in the database.
     */
    T get(ID id) throws Exception;

    /**
     * Store the fields from an object to the database row corresponding to the id of the data parameter. This way
     * you can persist the changes made to an object, except for the id field.
     *
     * <p>
     * NOTE: This will not save changes made to foreign objects or to foreign collections.
     * </p>
     *
     * @param data
     *            The data item that is been updated in the database.
     * @throws Exception
     *             on any SQL problems or if there is only an ID field in the object.
     */
    void update(T data) throws Exception;

    /**
     * Delete the database row corresponding to the id of the data parameter.
     *
     * @param data
     *            The data item that is been deleted from the database.
     * @throws Exception
     *             on any SQL problems.
     */
    void delete(T data) throws Exception;

    /**
     * Delete a collection of objects from the database, the rows to be removed correspond to the ids of the objects
     * in the collection parameter.
     *
     * @param collection
     *            A collection of data items to be deleted.
     * @throws Exception
     *             on any SQL problems.
     */
    void deleteAll(Collection<T> collection) throws Exception;

    /**
     * Returns the number of rows in the table associated with the data class.
     *
     * @return The number of rows in the table associated with the data class.
     * @throws Exception
     *             on any SQL problems.
     */
    Long count() throws Exception;

    /**
     * Query for the items in the object table that match a simple where with a single field = value type of WHERE
     * clause.
     *
     * @param fieldName
     *            The name of the field to be compared in the WHERE clause.
     * @param value
     *            The value of the field that has to match.
     * @return A list of the objects in the table that match the fieldName = value;
     * @throws Exception
     *             on any SQL problems.
     */
    List<T> findAllBy(String fieldName, Object value) throws Exception;

    /**
     * Query for the item in the object table that match a simple where with a single field = value type of WHERE
     * clause. In case of having multiples rows matching, only the first object is returned.
     *
     * @param fieldName
     *            The name of the field to be compared in the WHERE clause.
     * @param value
     *            The value of the field that has to match.
     * @return The first object in the table that matches the fieldName = value.
     * @throws Exception
     *             on any SQL problems.
     */
    T findBy(String fieldName, Object value) throws Exception;

    /**
     * Query for the rows in the database that matches all of the field to value entries from the map passed in.
     *
     * @param fieldValues
     *            A map with the field name and the corresponding value to be matched.
     * @return A list of the objects in the table that match all the fields values passed in.
     * @throws Exception
     *             on any SQL problems.
     */
    List<T> findAllBy(Map<String,Object> fieldValues) throws Exception;

    /**
     * Query for the row in the database that matches all of the field to value entries from the map passed in. In case
     * of having multiples rows matching, only the object of the first is returned.
     *
     * @param fieldValues
     *            A map with the field name and the corresponding value to be matched.
     * @return The first object in the table that matches all the fields values passed in.
     * @throws Exception
     *             on any SQL problems.
     */
    T findBy(Map<String, Object> fieldValues) throws Exception;

    /**
     * Query for the rows in the database that matches the SQL statement passed in.
     *
     * <p>
     *     Example of using this method
     *</p>
     * <pre>
     *      {@code
     *          String query = "select * from user where age > ?";
     *          List<User> users = userDao.query(query,21);
     *      }
     * </pre>
     *
     * @param query
     *            The SQL query to be executed.
     * @param arguments
     *            The values of the fields that will be replaced the wildcards. The wildcards will be replace in the order
     *            of the parameters passed in.
     * @return The list of objects that matches the query executed.
     * @throws Exception
     *             on any SQL problems.
     */
    List<T> query(String query,  String... arguments) throws Exception;

    /**
     * Perform a query that returns a single value (usually an aggregate function like MAX or COUNT). If the query
     * does not return a single long value then it will throw a SQLException.
     *
     * <p>
     *     Example of using this method
     *</p>
     * <pre>
     *      {@code
     *          String query = "select count(*) from user where age > ?";
     *          Long numberOfUsers = userDao.query(query,21);
     *      }
     * </pre>
     *
     * @param query
     *            The SQL query to be executed. It must return a single value.
     * @param arguments
     *            The values of the fields that will be replaced the wildcards. The wildcards will be replaced in the order
     *            of the parameters passed in.
     * @return The result of the SQL query executed.
     * @throws Exception
     *             on any SQL problems.
     */
    Long queryValue(String query,  String... arguments) throws Exception;

}
