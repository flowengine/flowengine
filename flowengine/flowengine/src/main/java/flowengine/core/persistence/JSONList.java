package flowengine.core.persistence;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

/**
 * Parametrized type of a JSON array
 */
public class JSONList<T> implements ParameterizedType {

    private Class<T> wrapped;

    /**
     * @param wrapped
     *      The class to be wrapped, it should be json array.
     */
    public JSONList(Class<T> wrapped) {
        this.wrapped = wrapped;
    }

    @Override
    public Type[] getActualTypeArguments() {
        return new Type[] {wrapped};
    }

    @Override
    public Type getRawType() {
        return List.class;
    }

    @Override
    public Type getOwnerType() {
        return null;
    }

}