package flowengine.core.persistence.strategies;

import android.content.Context;
import flowengine.core.persistence.PersistenceConstants;
import flowengine.core.persistence.PersistenceTypeEnum;
import flowengine.core.utils.AndroidManifestParser;

import java.util.HashMap;
import java.util.Map;

/**
 * Class used to build the corresponding persistence strategy, depending on the configuration of Android Manifest.
 *
 * @see PersistenceStrategy
 * @see DatabasePersistenceStrategy
 * @see MemoryPersistenceStrategy
 * @see FilePersistenceStrategy
 *
 */
public class PersistenceStrategyBuilder {

    private static PersistenceStrategyBuilder instance = null;

    /**
     * Returns the current instance of PersistenceStrategyBuilder
     *
     * @return The current instance of the class.
     */
    public static PersistenceStrategyBuilder getInstance(){
        if (instance == null)
            instance = new PersistenceStrategyBuilder();
        return instance;
    }

    /**
     * Create a new instance of Persistence Strategy({@link DatabasePersistenceStrategy},
     * {@link MemoryPersistenceStrategy} or {@link FilePersistenceStrategy}), depending on the configuration of
     * Android Manifest.
     *
     * @param context
     *          The {@link Context} of the Activity.
     * @return The corresponding instance of {@link PersistenceStrategy }.
     */
    public PersistenceStrategy newInstance(Context context){

        String persistenceType = AndroidManifestParser.getMetadata(context, PersistenceConstants.PERSISTENCE_TYPE);
        return persistenceStrategyMap.get(persistenceType).buildStrategy(context);
    }

    private abstract class StrategyBuilder {
        PersistenceStrategy instance = null;
        abstract PersistenceStrategy buildStrategy(Context context);
    }

    private Map<String, StrategyBuilder> persistenceStrategyMap;

    private PersistenceStrategyBuilder() {

        persistenceStrategyMap = new HashMap<>();
        persistenceStrategyMap.put(PersistenceTypeEnum.DATABASE_PERSISTENCE, new StrategyBuilder() {
            public PersistenceStrategy buildStrategy(Context context) {
                String databaseName = AndroidManifestParser.getMetadata(context,PersistenceConstants.DATABASE_NAME);
                if(instance == null)
                    instance = new DatabasePersistenceStrategy(context, databaseName);
                return instance;
            }
        });
        persistenceStrategyMap.put(PersistenceTypeEnum.MEMORY_PERSISTENCE, new StrategyBuilder() {
            public PersistenceStrategy buildStrategy(Context context) {
                String databaseName = AndroidManifestParser.getMetadata(context,PersistenceConstants.DATABASE_NAME);
                if(instance == null)
                    instance = new MemoryPersistenceStrategy(databaseName);
                return instance;
            }
        });
        persistenceStrategyMap.put(PersistenceTypeEnum.FILE_PERSISTENCE, new StrategyBuilder() {
            public PersistenceStrategy buildStrategy(Context context) {
                String filename = AndroidManifestParser.getMetadata(context,PersistenceConstants.FILE_NAME);
                if(instance == null)
                    instance = new FilePersistenceStrategy(context,filename);
                return instance;
            }
        });
    }


}
