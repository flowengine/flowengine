package flowengine.core.persistence;

/**
 * Created by florencia on 22/05/15.
 */
public class PersistenceConstants {

    public static String PERSISTENCE_TYPE = "persistenceType";
    public static String DATABASE_NAME = "databaseName";
    public static String FILE_NAME = "filename";

}
