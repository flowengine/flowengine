package flowengine.core.persistence;

import com.j256.ormlite.field.DatabaseField;

import java.util.List;

/**
 * Database Access Objects that handle the reading and writing of a class from the database. This interface can be used
 * with Database, Memory and File Persistence types.
 *
 * @param <T>
 *            The class in which the dao will be operating on.
 * @param <ID>
 *            The class of the ID column associated with the class. If the T class does not have an ID, it can be used
 *            Void or Object instead.
 *
 */
public interface Dao<T,ID> {

    /**
     * Retrieve all the objects persisted corresponding to class T.
     *
     * @return A list of all of the objects persisted of the same class.
     * @throws Exception
     *             on any problems retrieving the data.
     */
    List<T> getAll() throws Exception;

    /**
     * Create an item to persist from an object of class T. In case of Database and Memory Persistence:
     * If the object being created uses {@link DatabaseField#generatedId()} then the data parameter will be modified
     * and set with the corresponding id from the database. In case of File Persistence, the identifier parameter will
     * not be modified.
     *
     * @param data
     *            The data item that we want to persist.
     * @throws Exception
     *             on any problems creating the data.
     */
    void save(T data) throws Exception;

}
