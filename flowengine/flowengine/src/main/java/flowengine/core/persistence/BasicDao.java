package flowengine.core.persistence;

import flowengine.core.persistence.strategies.FilePersistenceStrategy;
import flowengine.core.persistence.strategies.DatabasePersistenceStrategy;
import flowengine.core.persistence.strategies.MemoryPersistenceStrategy;
import flowengine.core.persistence.strategies.PersistenceStrategy;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Base class for the Database Access Objects that handle the reading and writing a class from the database.
 *
 * <p>
 * <b> NOTE: </b> If you are using File Persistence type, you will get an UnsupportedOperationException when accessing
 * the methods of SQLDao interface.
 * </p>
 *
 * @param <T>
 *            The class in which the dao will be operating on.
 * @param <ID>
 *            The class of the ID column associated with the class. If the T class does not have an ID, it can be used
 *            Void or Object instead.
 *
 */
public class BasicDao<T,ID> implements SQLDao<T,ID> {

    PersistenceStrategy persistenceStrategy;
    Class<T> persistableClass;

    /**
     * Constructor of the base Dao for class T.
     *
     * @param persistableClass
     *            The class in which the dao will be operating on.
     * @param strategy
     *            The persistence strategy to be used for all the operations({@link FilePersistenceStrategy},
     *            {@link MemoryPersistenceStrategy},{@link DatabasePersistenceStrategy}).
     */
    public BasicDao(Class<T> persistableClass, PersistenceStrategy strategy){
        persistenceStrategy = strategy;
        this.persistableClass = persistableClass;
    }

    @Override
    public List<T> getAll() throws Exception {
        return (List<T>) persistenceStrategy.getAll(persistableClass);
    }

    @Override
    public T get(ID id) throws Exception {
        return (T) persistenceStrategy.get(persistableClass, id);
    }

    @Override
    public void save(T data)throws Exception {
        persistenceStrategy.save(data);
    }

    @Override
    public void update(T data) throws Exception {
        persistenceStrategy.update(data);
    }

    @Override
    public void delete(T data) throws Exception {
        persistenceStrategy.delete(data);
    }

    @Override
    public void deleteAll(Collection<T> collection) throws Exception {
        persistenceStrategy.deleteAll(persistableClass, (Collection<Object>) collection);
    }

    @Override
    public Long count() throws Exception {
        return persistenceStrategy.count(persistableClass);
    }

    @Override
    public List<T> findAllBy(String fieldName, Object value) throws Exception {
        return (List<T>) persistenceStrategy.findAllBy(persistableClass, fieldName, value);
    }

    @Override
    public T findBy(String fieldName, Object value) throws Exception {
        return (T) persistenceStrategy.findBy(persistableClass, fieldName, value);
    }

    @Override
    public List<T> findAllBy(Map<String,Object> fieldValues) throws Exception {
        return (List<T>) persistenceStrategy.findAllBy(persistableClass, fieldValues);
    }

    @Override
    public T findBy(Map<String, Object> fieldValues) throws Exception {
        return (T) persistenceStrategy.findBy(persistableClass, fieldValues);
    }

    @Override
    public List<T> query(String query,  String... arguments) throws Exception {
        return (List<T>) persistenceStrategy.query(persistableClass, query, arguments);
    }

    @Override
    public Long queryValue(String query,  String... arguments) throws Exception {
        return persistenceStrategy.queryValue(persistableClass,query,arguments);
    }

}
