package flowengine.core.persistence.strategies;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Strategy to handle the operations used by Daos objects for the different types of persistence (File, Memory,
 * Database).
 */
public interface PersistenceStrategy {

    /**
     * Retrieves all the objects persisted corresponding to the class passed in.
     *
     * @param cls
     *          Class of the items to be retrieved.
     * @return A list of all of the objects persisted of the same class.
     * @throws Exception
     *             on any problems retrieving the data.
     */
    List<?> getAll(Class<?> cls) throws Exception;

    /**
     * Retrieves an object associated with a specific ID.
     *
     * @param cls
     *            Class of the items to be retrieved.
     * @param id
     *            Identifier that matches a specific object persisted to find and return.
     * @return The object that has the ID field which equals the parameter id or null if no matches.
     * @throws Exception
     *             on any problems retrieving the data or if more than 1 item with the id are found in the database.
     */
    Object  get(Class<?> cls,Object id) throws Exception;

    /**
     * Create an item to persist from an object of class T.
     *
     * @param data
     *            The data item that we are creating to persist.
     * @throws Exception
     *             on any problems creating the data.
     */
    void save(Object data) throws Exception;

    /**
     * Store the fields from an object, previously persisted, corresponding to the id from the data parameter. This way
     * you can persist the changes made to an object, except for the id field.
     *
     * <p>
     * NOTE: This will not save changes made to foreign objects or to foreign collections.
     * </p>
     *
     * @param data
     *            The data item that is been updated.
     * @throws Exception
     *             on any problems saving the data or if there is only an ID field in the object.
     */
    void update(Object data) throws Exception;

    /**
     * Delete the object persisted corresponding to the id from the data parameter.
     *
     * @param data
     *            The data item that is been deleted.
     * @throws Exception
     *             on any problems deleting the object.
     */
    void delete(Object data) throws Exception;

    /**
     * Delete a collection of objects persisted, the objects to be removed correspond to the ids of the objects
     * in the collection parameter.
     *
     * @param cls
     *            Class of the items to be removed.
     * @param collection
     *            A collection of data items to be deleted.
     * @throws Exception
     *             on any problems deleting the objects.
     */
    void deleteAll(Class<?> cls, Collection<Object> collection) throws Exception;

    /**
     * Returns the number of objects in the table associated with the data class.
     *
     * @param cls
     *            Class of the items to count.
     * @return The number of rows in the table associated with the data class.
     * @throws Exception
     *             on any problems accessing the data.
     */
    Long count(Class<?> cls) throws Exception;

    /**
     * Query for the items persisted that match a simple where with a single field = value type of WHERE
     * clause.
     *
     * @param cls
     *            Class of the items to find.
     * @param fieldName
     *            The name of the field to be compared in the WHERE clause.
     * @param value
     *            The value of the field that has to match.
     * @return A list of the objects that match the fieldName = value;
     * @throws Exception
     *             on any problems accessing the data.
     */
    List<Object> findAllBy(Class<?> cls,String fieldName, Object value) throws Exception;

    /**
     * Query for the item persisted that match a simple where with a single field = value type of WHERE
     * clause. In case of having multiples rows matching, only the first object is returned.
     *
     * @param cls
     *            Class of the items to find.
     * @param fieldName
     *            The name of the field to be compared in the WHERE clause.
     * @param value
     *            The value of the field that has to match.
     * @return The first object persisted that matches the fieldName = value.
     * @throws Exception
     *             on any problems accessing the data.
     */
    Object findBy(Class<?> cls, String fieldName, Object value) throws Exception;

    /**
     * Query for the objects persisted that matches all of the field to value entries from the map passed in.
     *
     * @param cls
     *            Class of the items to find.
     * @param fieldValues
     *            A map with the field name and the corresponding value to be matched.
     * @return A list of the objects that match all the fields values passed in.
     * @throws Exception
     *             on any problems accessing the data.
     */
    List<Object> findAllBy(Class<?> cls, Map<String,Object> fieldValues) throws Exception;

    /**
     * Query for the objects persisted that matches all of the field to value entries from the map passed in. In case
     * of having multiples rows matching, only the object of the first is returned.
     *
     * @param cls
     *            Class of the items to find.
     * @param fieldValues
     *            A map with
     * @return The first object persisted that matches all the fields values passed in.
     * @throws Exception
     *             on any problems accessing the data.
     */
    Object findBy(Class<?> cls, Map<String, Object> fieldValues) throws Exception;

    /**
     * Query for the rows in the database that matches the SQL query passed in.
     *
     * <p>
     * NOTE: This method can not be implemented for strategies that does not support SQL.
     * </p>
     *
     * <p>
     *     Example of using this method
     *</p>
     * <pre>
     *     String query = &quot;select * from user where age &gt; ?&quot;;
     *     List&lt;User&gt; users = userDao.query(query,21);
     * </pre>
     *
     * @param cls
     *            Class of the items to find.
     * @param query
     *            The SQL query to be executed.
     * @param arguments
     *            The values of the fields that will replaced the wildcards. The wildcards will be replaced in the order
     *            of the parameters passed in.
     * @return The list of objects that matches the query executed.
     * @throws Exception
     *             on any SQL problems.
     */
    List<Object> query(Class<?> cls,String query,  String... arguments) throws Exception;

    /**
     * Perform a query that returns a single value (usually an aggregate function like MAX or COUNT). If the query
     * does not return a single long value then it will throw a SQLException.
     *
     * <p>
     * NOTE: This method can not be implemented for strategies that does not support SQL.
     * </p>
     *
     * <p>
     *     Example of using this method
     *</p>
     * <pre>
     *     String query = &quot;select count(*) from user where age &gt; ?&quot;;
     *     Long numberOfUsers = userDao.query(query,21);
     * </pre>
     *
     * @param cls
     *            Class of the items to perform the query.
     * @param query
     *            The SQL query to be executed. It must return a single value.
     * @param arguments
     *            The values of the fields that will replaced the wildcards. The wildcards will be replaced in the order
     *            of the parameters passed in.
     * @return The result of the SQL query executed.
     * @throws Exception
     *             on any SQL problems.
     */
    Long queryValue(Class<?> cls,String query,  String... arguments) throws Exception;

    /**
     * Set the class to be persisted.
     *
     * @param cls
     *            The class in which the dao will be operating on.
     */
    void setPersistableClass(Class<?> cls);

    /**
     * Close the access to daos.
     *
     * @throws Exception
     *             on any problems.
     */
    void close() throws Exception;

}
