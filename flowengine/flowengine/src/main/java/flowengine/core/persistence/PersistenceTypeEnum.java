package flowengine.core.persistence;

/**
 * Created by florencia on 21/05/15.
 */
public final class PersistenceTypeEnum {

    public final static String DATABASE_PERSISTENCE = "database";
    public final static String FILE_PERSISTENCE = "file";
    public final static String MEMORY_PERSISTENCE = "memory";

}
