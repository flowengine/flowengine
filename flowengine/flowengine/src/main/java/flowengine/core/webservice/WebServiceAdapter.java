package flowengine.core.webservice;

import android.content.Context;
import flowengine.annotations.flow.methods.Restart;
import retrofit.RestAdapter;

import java.lang.reflect.Proxy;

/**
 * Created by igaray on 04/06/2015.
 */
public class WebServiceAdapter {
    private String endpoint;
    private Context context;

    public static WebServiceBuilder startBuilding() {
        return new WebServiceBuilder();
    }

    public <T> T create(Class<T> webApiClass) {
       RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(getEndpoint()).build();
        return BuildWebServiceProxy(webApiClass, restAdapter);
    }

    private <T> T BuildWebServiceProxy(Class<T> webApiClass, RestAdapter restAdapter) {
        return (T) Proxy.newProxyInstance(
                webApiClass.getClassLoader(),
                new Class[] {webApiClass},
                new WebServiceProxyHandler(restAdapter.create(webApiClass), context)
        );
    }

    void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    void setContext(Context context) {
        this.context = context;
    }

    public String getEndpoint() {
        return endpoint;
    }
}
