package flowengine.core.webservice;

import android.content.Context;
import flowengine.core.webservice.strategies.InvocationStrategy;
import flowengine.core.webservice.strategies.InvocationStrategyFactory;
import flowengine.*;
import flowengine.core.webservice.strategies.InvocationStrategyHelper;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.UUID;

public class WebServiceProxyHandler<T> implements InvocationHandler {
    private T retrofitProxy;
    private Context context;
    private HashMap<UUID, WebServiceProxyHandler.RetryInvoke> cachedMethodsForRetry;
    private Object mutex;

    public WebServiceProxyHandler() {}

    public WebServiceProxyHandler(T proxy, Context context) {
        this.retrofitProxy = proxy;
        this.context = context;
        this.cachedMethodsForRetry = new HashMap<>();
        this.mutex = new Object();
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        saveForRetryIfNecessary(method, args);
        setReturnType(method, args);
        return wrapInvocation(method, retrofitProxy, args);
    }

    private void setReturnType(Method method, Object[] args) {
        WebResponse webResponse = getWebResponseIfExists(args);
        if (webResponse != null) {
            WebCallbackResponseType responseType = InvocationStrategyHelper.getCallbackResponseType(method);
            if(responseType != null)
                webResponse.setReturnType(responseType.getResponseType());
        }
    }

    public synchronized void onEvent(RetryEvent retryEvent) {
        UUID uuid = retryEvent.getUuid();
        if (cachedMethodsForRetry.containsKey(uuid)) {
            RetryInvoke retryInvoke = cachedMethodsForRetry.remove(uuid);
            try {
                wrapInvocation(retryInvoke.method, retrofitProxy, retryInvoke.args);
            } catch (Throwable throwable) {
                // Should never get here because the method && args has been invoked once (before retrying).
            }
            unregisterFromBus();
        }
    }

    public synchronized void onEvent(CancelRetryEvent cancelRetryEvent) {
        UUID uuid = cancelRetryEvent.getUuid();
        if (cachedMethodsForRetry.containsKey(uuid))
            cachedMethodsForRetry.remove(uuid);
        unregisterFromBus();
    }

    private void unregisterFromBus() {
        synchronized (mutex) {
            if (cachedMethodsForRetry.isEmpty()) {
                EventBus eventBus = FlowEngine.getCurrentBus();
                if (eventBus.isRegistered(this)) {
                    eventBus.unregister(this);
                }
            }
        }
    }

    private void saveForRetryIfNecessary(Method method, Object[] args) {
        WebResponse webResponse = getWebResponseIfExists(args);
        if (webResponse != null && webResponse.isRetryable()) {
            synchronized (mutex) {
                this.cachedMethodsForRetry.put(webResponse.getUuid(), new RetryInvoke(method, args));
                EventBus eventBus = FlowEngine.getCurrentBus();
                if (!eventBus.isRegistered(this)) {
                    eventBus.register(this);
                }
            }
        }
    }

    private WebResponse getWebResponseIfExists(Object[] args) {
        for (Object obs : args) {
            if (obs instanceof WebResponse) {
                return (WebResponse) obs;
            }
        }
        return null;
    }

    private Object wrapInvocation(Method method, Object target, Object[] args) throws Throwable {
        return getInvocationStrategy(method).invokeMethod(method, target, args);
    }

    private InvocationStrategy getInvocationStrategy(Method method) {
        return InvocationStrategyFactory.getInstance().getStrategy(method, context);
    }

    public T getRetrofitProxy() {
        return retrofitProxy;
    }

    public void setRetrofitProxy(T proxy) {
        this.retrofitProxy = proxy;
    }

    private class RetryInvoke {
        Method method;
        Object[] args;

        RetryInvoke (Method method, Object[] args) {
            this.method = method;
            this.args = args;
        }
    }
}
