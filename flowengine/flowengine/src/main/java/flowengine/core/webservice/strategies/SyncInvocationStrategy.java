package flowengine.core.webservice.strategies;

import flowengine.WebResponse;

import java.lang.reflect.Method;

/**
 * Created by igaray on 08/06/2015.
 */
public class SyncInvocationStrategy implements InvocationStrategy {

    @Override
    public Object invokeMethod(Method method, Object target, Object[] args) throws Throwable {

        Method restMethod = InvocationStrategyHelper.findMethodFromProxy(method, target);

        if (restMethod.getReturnType() != Void.TYPE) {
            return restMethod.invoke(target, args);
        }

        restMethod.invoke(target, args);
        return null;
    }
}
