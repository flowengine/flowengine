package flowengine.core.webservice.strategies;

/**
 * Created by florencia on 29/07/15.
 */
public final class InvocationStrategyType {

    public final static String SYNC_INVOCATION_STRATEGY = "SyncInvocationStrategy";
    public final static String SYNC_INVOCATION_REPOSITORY_STRATEGY = "SyncInvocationRepositoryStrategy";
    public final static String ASYNC_INVOCATION_REPOSITORY_STRATEGY = "AsyncInvocationRepositoryStrategy";

}
