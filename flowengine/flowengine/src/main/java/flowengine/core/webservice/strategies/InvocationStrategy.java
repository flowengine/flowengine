package flowengine.core.webservice.strategies;

import java.lang.reflect.Method;

/**
 * Created by igaray on 08/06/2015.
 */
public interface InvocationStrategy {
    Object invokeMethod(Method method, Object target, Object[] args) throws Throwable;
}
