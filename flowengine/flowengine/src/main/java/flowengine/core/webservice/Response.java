package flowengine.core.webservice;

import retrofit.client.Header;
import retrofit.mime.TypedInput;

import java.util.List;

/**
 * Created by Ignacio on 05/07/2015.
 */
public class Response {
    private retrofit.client.Response retrofitResponse;

    public Response(retrofit.client.Response response) {
        retrofitResponse = response;
    }

    public String getUrl() {
        return retrofitResponse.getUrl();
    }

    public int getStatus() {
        return retrofitResponse.getStatus();
    }

    public String getReason() {
        return retrofitResponse.getReason();
    }

    public List<Header> getHeaders() {
        return retrofitResponse.getHeaders();
    }

    public TypedInput getBody() {
        return retrofitResponse.getBody();
    }
}
