package flowengine.core.webservice;

import java.util.UUID;

public final class CancelRetryEvent {
    private UUID uuid;

    public CancelRetryEvent(UUID uuid) {
        this.uuid = uuid;
    }

    public UUID getUuid() {
        return uuid;
    }

}
