package flowengine.core.webservice.strategies.events;

import flowengine.WebResponse;
import flowengine.core.persistence.SQLDao;

import java.lang.reflect.Method;

/**
 * Created by Ignacio on 03/09/2015.
 */
public class AsyncInvocationRepositoryGetMethodEvent {
    public SQLDao dao;
    public Method method;
    public Object target;
    public Object[] args;
    public AsyncInvocationRepositoryGetMethodEvent(SQLDao dao, Method method, Object target, Object[] args) {
        this.dao = dao; this.method = method; this.target = target; this.args = args;
    }
}
