package flowengine.core.webservice;

import flowengine.core.processors.MirrorTypeHelper;

import java.util.List;

/**
 * Created by florencia on 11/08/15.
 */
public class RepositoryEvent {

    private Object element;
    private Object originalElement;
    private Class elementClass;

    public RepositoryEvent(Object element, Object originalElement, Class elementClass) {
        this.element = element;
        this.originalElement = originalElement;
        this.elementClass = elementClass;
    }

    public Object getElement() {
        return element;
    }

    public Object getOriginalElement() {
        return originalElement;
    }

    public Class getElementClass() {
        return elementClass;
    }

    public Class getElementGenericClass() {
        if(!MirrorTypeHelper.extendsClass(List.class, elementClass))
            return elementClass;

        /* If it is a list, the first element's class is used  */
        List objectList = (List) element;
        Object firstObject = objectList.get(0);
        if(firstObject == null)
            return null;

        return firstObject.getClass();
    }

}
