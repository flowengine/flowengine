package flowengine.core.webservice;

import retrofit.RetrofitError;

import java.lang.reflect.Type;

import retrofit.client.*;
import retrofit.client.Response;
import retrofit.converter.ConversionException;
import retrofit.converter.Converter;
import retrofit.mime.TypedInput;

/**
 * Created by Ignacio on 05/07/2015.
 */
public class WebError {

    private RetrofitError retrofitError;

    public WebError(RetrofitError error) {
        retrofitError = error;
    }

    public String getUrl() {
        return retrofitError.getUrl();
    }

    public Response getResponse() {
        return retrofitError.getResponse();
    }

    @Deprecated
    public boolean isNetworkError() {
        return retrofitError.isNetworkError();
    }

    public WebError.Kind getKind() {
        RetrofitError.Kind error = retrofitError.getKind();
        if (RetrofitError.Kind.CONVERSION.equals(error))
            return WebError.Kind.CONVERSION;
        if (RetrofitError.Kind.HTTP.equals(error))
            return Kind.HTTP;
        if (RetrofitError.Kind.NETWORK.equals(error))
            return Kind.NETWORK;
        return Kind.UNEXPECTED;
    }

    public Object getBody() {
        return retrofitError.getBody();
    }

    public Type getSuccessType() {
        return this.getSuccessType();
    }

    public Object getBodyAs(Type type) {
        return retrofitError.getBodyAs(type);
    }

    public static enum Kind {
        NETWORK,
        CONVERSION,
        HTTP,
        UNEXPECTED;

        private Kind() {
        }
    }
}
