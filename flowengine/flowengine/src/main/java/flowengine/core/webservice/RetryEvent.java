package flowengine.core.webservice;

import java.util.UUID;

public final class RetryEvent {
    private UUID uuid;

    public RetryEvent(UUID uuid) {
        this.uuid = uuid;
    }

    public UUID getUuid() {
        return uuid;
    }
}
