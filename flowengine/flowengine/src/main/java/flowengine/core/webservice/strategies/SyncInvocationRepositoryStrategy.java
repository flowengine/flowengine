package flowengine.core.webservice.strategies;

import android.content.Context;
import flowengine.WebResponse;
import flowengine.core.persistence.SQLDao;
import flowengine.core.processors.PersistableBuilder;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by florencia on 30/07/15.
 */
public class SyncInvocationRepositoryStrategy implements InvocationStrategy {
    private Context context;

    public SyncInvocationRepositoryStrategy(Context context){
        this.context = context;
    }

    @Override
    public Object invokeMethod(Method method, Object target, Object[] args) throws Throwable {

        Class returnedClass = InvocationStrategyHelper.getReturnType(method);
        SQLDao dao = getDaoForClass(returnedClass);

        RepositoryTypesMethods methodType = InvocationStrategyHelper.getRepositoryMethodType(method);

        if(methodType == RepositoryTypesMethods.GET_METHOD)
            return syncInvocationRepositoryGetMethod(dao, method, target, args);

        if(methodType == RepositoryTypesMethods.GET_ALL_METHOD)
            return syncInvocationRepositoryGetAllMethod(dao, method, target, args);

        return null;
    }

    private Object syncInvocationRepositoryGetMethod(SQLDao dao, Method method, Object target, Object[] args) throws Exception {

        Object id = InvocationStrategyHelper.getIdFromParameters(method, args);
        if(id == null)
            throw new RuntimeException("There is not an ID annotation in the method's parameters: " + args.toString());

        Object result, oldResult;
        oldResult = dao.get(id);

        /* If there is an object persisted with the id */
        if(!InvocationStrategyHelper.isUpdateForced(method) && oldResult != null)
            return oldResult;

        /* If there isn't an object in the local storage or update is forced then the web service is called and the data is saved */
        result = callWebService(method, target, args);
        if (result != null) {
            dao.save(result);
        } else if (oldResult != null) {
            dao.delete(oldResult);
        }

        return result;
    }

    private List<?> syncInvocationRepositoryGetAllMethod(SQLDao dao, Method method, Object target, Object[] args) throws Exception {

        List<?> results, oldResults;

        HashMap<String, Object> fieldValues = InvocationStrategyHelper.getWhereArgumentsFromParameters(method, args);
        oldResults = (fieldValues.isEmpty()) ? dao.getAll() : dao.findAllBy(fieldValues);

        /* If there are objects persisted */
        if(!InvocationStrategyHelper.isUpdateForced(method) && oldResults.size() > 0)
            return oldResults;

        /* If there aren't objects in the local storage or update is forced then the web service is called and the data is saved */
        results = (List<?>) callWebService(method, target, args);
        results = results != null ? results : new ArrayList<>();
        saveResults(oldResults, results, dao);

        return results;

    }

    private HashMap<String, Object> getFieldValues(Method method, Object target, Object[] args) {
        return null;
    }

    private Object callWebService(Method method, Object target, Object[] args) throws Exception {

        Method restMethod = InvocationStrategyHelper.findMethodFromProxy(method, target);
        return restMethod.invoke(target, args);

    }


    private void saveResults(List oldResults, List newResults, SQLDao dao) throws Exception {
        for (Object old: oldResults)
            if (!newResults.contains(old))
                dao.delete(old);
        for (Object result: newResults)
            dao.save(result);
    }

    private SQLDao getDaoForClass(Class cls){
        PersistableBuilder builder = new PersistableBuilder(context);
        return (SQLDao) builder.newInstance(cls.getCanonicalName());
    }


}
