package flowengine.core.webservice;

/**
 * Created by florencia on 05/08/15.
 */
public class WebCallbackResponseType {

    private Class responseType;
    private Class responseGenericType;

    public WebCallbackResponseType(Class responseType, Class responseGenericType) {
        this.responseType = responseType;
        this.responseGenericType = responseGenericType;
    }

    public Class getResponseGenericType() {
        return ((responseGenericType != null) ? responseGenericType : responseType);
    }

    public void setResponseGenericType(Class responseGenericType) {
        this.responseGenericType = responseGenericType;
    }

    public Class getResponseType() {
        return responseType;
    }

    public void setResponseType(Class responseType) {
        this.responseType = responseType;
    }
}
