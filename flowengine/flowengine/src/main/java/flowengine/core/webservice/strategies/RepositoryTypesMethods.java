package flowengine.core.webservice.strategies;

/**
 * Created by florencia on 31/07/15.
 */
public enum RepositoryTypesMethods {

    GET_METHOD, GET_ALL_METHOD;

}
