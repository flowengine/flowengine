package flowengine.core.webservice.strategies;

import android.content.Context;

import flowengine.FlowEngine;
import flowengine.WebResponse;
import flowengine.core.persistence.SQLDao;
import flowengine.core.processors.MirrorTypeHelper;
import flowengine.core.processors.PersistableBuilder;
import flowengine.core.webservice.RepositoryEvent;
import flowengine.core.webservice.WebCallbackResponseType;
import flowengine.core.webservice.strategies.events.AsyncInvocationRepositoryGetAllMethodEvent;
import flowengine.core.webservice.strategies.events.AsyncInvocationRepositoryGetMethodEvent;

import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by florencia on 30/07/15.
 */
public class AsyncInvocationRepositoryStrategy implements InvocationStrategy {

    private Context context;

    public AsyncInvocationRepositoryStrategy(Context context){
        this.context = context;
    }

    @Override
    public Object invokeMethod(Method method, Object target, Object[] args) throws Throwable {
        WebCallbackResponseType responseType = InvocationStrategyHelper.getCallbackResponseType(method);
        if(responseType == null)
            throw new Exception("An error occurred parsing the callback returned type ");

        Class responseTypeClass = responseType.getResponseType();
        Class responseGenericTypeClass = responseType.getResponseGenericType();
        SQLDao dao = getDaoForClass(responseGenericTypeClass);

        RepositoryTypesMethods methodType = InvocationStrategyHelper.getRepositoryMethodType(method, responseTypeClass);
        if(methodType == RepositoryTypesMethods.GET_METHOD)
            FlowEngine.getCurrentBus().post(new AsyncInvocationRepositoryGetMethodEvent(dao, method, target, args));
        if(methodType == RepositoryTypesMethods.GET_ALL_METHOD)
            FlowEngine.getCurrentBus().post(new AsyncInvocationRepositoryGetAllMethodEvent(dao, method, target, args));

        return null;
    }

    private SQLDao getDaoForClass(Class cls){
        PersistableBuilder builder = new PersistableBuilder(context);
        return (SQLDao) builder.newInstance(cls.getCanonicalName());
    }

    public void onEventAsync(AsyncInvocationRepositoryGetMethodEvent event) {
        try {
            Object id = InvocationStrategyHelper.getIdFromParameters(event.method, event.args);
            if(id == null)
                throw new RuntimeException("There is not an ID annotation in the method's parameters: " + event.args.toString());

            Object result = event.dao.get(id);

            getWebResponse(event.args).setOriginalResult(result);
            if(result != null){
                postEventWithResult(event.args,result);
                if (!InvocationStrategyHelper.isUpdateForced(event.method)) {
                    return;
                }
            }

            callWebService(event.method, event.target, event.args);

        } catch (Exception ex) {

        }
    }

    public void onEventAsync(AsyncInvocationRepositoryGetAllMethodEvent event) {
        try {
            HashMap<String, Object> fieldValues = InvocationStrategyHelper.getWhereArgumentsFromParameters(event.method, event.args);
            List<?> result = (fieldValues.isEmpty()) ? event.dao.getAll() : event.dao.findAllBy(fieldValues);

            getWebResponse(event.args).setOriginalResult(result);
            if(result.size() > 0){
                postEventWithResult(event.args,result);
                if (!InvocationStrategyHelper.isUpdateForced(event.method)) {
                    return;
                }
            }

            callWebService(event.method, event.target, event.args);

        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private void callWebService(Method method, Object target, Object[] args) throws Exception {
        Method restMethod = InvocationStrategyHelper.findMethodFromProxy(method, target);
        restMethod.invoke(target, args);
    }

    private void postEventWithResult(Object[] args,Object o){
        postEvent(args, o);
    }

    private synchronized void postEvent(Object[] args,Object o){
        WebResponse webResponse = getWebResponse(args);
        if(webResponse != null)
            webResponse.postEventWithResult(o, null);
    }

    private WebResponse getWebResponse(Object[] args){
        for(Object arg: args){
            if(MirrorTypeHelper.extendsClass(WebResponse.class,arg.getClass()))
                return (WebResponse) arg;
        }
        return null;
    }

    public void onEventAsync(RepositoryEvent event){
        try {
            Boolean extendsList = MirrorTypeHelper.extendsClass(List.class, event.getElementClass());
            Object original = event.getOriginalElement();
            SQLDao dao = getDaoForClass(event.getElementGenericClass());
            if(!extendsList) {
                if (event.getElement() != null) {
                    dao.save(event.getElement());
                } else if (original != null){
                    dao.delete(original);
                }
                return;
            }
            List results = event.getElement() != null ? (List) event.getElement() : new ArrayList();
            for (Object o: (List) original) {
                if (!results.contains(o)) {
                    dao.delete(o);
                }
            }
            for (Object o: results) {
                dao.save(o);
            }
        } catch (Exception ex) {

        }

    }

}
