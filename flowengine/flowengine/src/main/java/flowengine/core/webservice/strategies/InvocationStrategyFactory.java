package flowengine.core.webservice.strategies;

import android.content.Context;
import flowengine.FlowEngine;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by florencia on 29/07/15.
 */
public class InvocationStrategyFactory {

    private interface InvocationStrategyBuilder {
        InvocationStrategy build(Context context);
    }

    public static InvocationStrategyFactory getInstance() {
        if (instance == null)
            instance = new InvocationStrategyFactory();
        return instance;
    }

    public InvocationStrategy getStrategy(Method method, Context context) {
        String invocationStrategyType = InvocationStrategyHelper.getInvocationStrategyType(method);
        if (invocationStrategyMap.containsKey(invocationStrategyType)) {
            InvocationStrategy strategy = invocationStrategyMap.get(invocationStrategyType);
            if (strategy == null) {
                strategy = invocationStrategyBuilderMap.get(invocationStrategyType).build(context);
                FlowEngine.getCurrentBus().register(strategy);
                invocationStrategyMap.put(invocationStrategyType, strategy);
            }
            return strategy;
        }
        return null;
    }

    private static InvocationStrategyFactory instance = null;

    private Map<String, InvocationStrategy> invocationStrategyMap;
    Map<String, InvocationStrategyBuilder> invocationStrategyBuilderMap = new HashMap<>();

    private InvocationStrategyFactory() {
        invocationStrategyMap = new HashMap<>();
        invocationStrategyMap.put(InvocationStrategyType.SYNC_INVOCATION_STRATEGY, null);
        invocationStrategyMap.put(InvocationStrategyType.SYNC_INVOCATION_REPOSITORY_STRATEGY, null);
        invocationStrategyMap.put(InvocationStrategyType.ASYNC_INVOCATION_REPOSITORY_STRATEGY, null);
        invocationStrategyBuilderMap.put(InvocationStrategyType.SYNC_INVOCATION_STRATEGY, new InvocationStrategyBuilder(){
            public InvocationStrategy build(Context context) {
                return new SyncInvocationStrategy();
            }
        });
        invocationStrategyBuilderMap.put(InvocationStrategyType.SYNC_INVOCATION_REPOSITORY_STRATEGY, new InvocationStrategyBuilder(){
            public InvocationStrategy build(Context context) {
                return new SyncInvocationRepositoryStrategy(context);
            }
        });
        invocationStrategyBuilderMap.put(InvocationStrategyType.ASYNC_INVOCATION_REPOSITORY_STRATEGY, new InvocationStrategyBuilder(){
            public InvocationStrategy build(Context context) {
                return new AsyncInvocationRepositoryStrategy(context);
            }
        });
    }

}
