package flowengine.core.webservice;

import android.content.Context;

/**
 * Created by igaray on 04/06/2015.
 */
public class WebServiceBuilder {

    private String endpoint;
    private Context context;

    public String getEndpoint() {
        return endpoint;
    }

    public Context getContext() {
        return context;
    }

    public WebServiceBuilder setEndpoint(String endpoint) {
        this.endpoint = endpoint;
        return this;
    }

    public WebServiceBuilder setContext(Context context) {
        this.context = context;
        return this;
    }

    public WebServiceAdapter build() {
        WebServiceAdapter adapter = new WebServiceAdapter();
        adapter.setEndpoint(getEndpoint());
        adapter.setContext(getContext());
        return adapter;
    }

}
