package flowengine.core.webservice.strategies;

import flowengine.annotations.webservice.Cache;
import flowengine.annotations.webservice.Equals;
import flowengine.annotations.webservice.ID;
import flowengine.core.processors.MirrorTypeHelper;
import flowengine.core.utils.Types;
import flowengine.core.webservice.WebCallbackResponseType;
import retrofit.Callback;
import java.lang.reflect.Type;
import retrofit.http.GET;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.WildcardType;
import java.util.HashMap;
import java.util.List;

/**
 * Created by florencia on 01/08/15.
 */
public class InvocationStrategyHelper {


    public static Method findMethodFromProxy(Method method, Object retrofitProxy) {
        Class proxyClass = retrofitProxy.getClass();
        try {
            return proxyClass.getMethod(method.getName(), method.getParameterTypes());
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public static String getInvocationStrategyType(Method method){

        if(!isARepositoryMethod(method))
            return InvocationStrategyType.SYNC_INVOCATION_STRATEGY;

        if(isAnAsynchronousMethod(method))
            return InvocationStrategyType.ASYNC_INVOCATION_REPOSITORY_STRATEGY;

        return InvocationStrategyType.SYNC_INVOCATION_REPOSITORY_STRATEGY;
    }

    private static Boolean isARepositoryMethod(Method method){
        return method.isAnnotationPresent(Cache.class);
    }

    public static Boolean isUpdateForced(Method method){
        Cache annotation = method.getAnnotation(Cache.class);
        return annotation != null && annotation.forceUpdate();
    }

    private static Boolean isAnAsynchronousMethod(Method method){
        return method.getReturnType().equals(Void.TYPE);
    }

    public static Class getReturnType(Method method){
        if(!returnedTypeExtendsList(method))
            return method.getReturnType();
        return MirrorTypeHelper.getGenericType(method.getGenericReturnType());
    }

    public static RepositoryTypesMethods getRepositoryMethodType(Method method){
        if(isAGetMethod(method))
            return RepositoryTypesMethods.GET_METHOD;
        if(isAGetAllMethod(method))
            return RepositoryTypesMethods.GET_ALL_METHOD;
        return null;
    }

    private static Boolean returnedTypeExtendsList(Method method){
        return MirrorTypeHelper.extendsClass(List.class, method.getReturnType());
    }

    private static Boolean isAGetMethod(Method method){
        return (method.isAnnotationPresent(GET.class) && (!returnedTypeExtendsList(method)));
    }

    private static Boolean isAGetAllMethod(Method method){
        return (method.isAnnotationPresent(GET.class) && returnedTypeExtendsList(method));
    }

    public static Object getIdFromParameters(Method method, Object[] args){

        Annotation[][] parameterAnnotations = method.getParameterAnnotations();
        int i=0;
        for(Annotation[] annotations : parameterAnnotations){
            Object parameterValue = args[i++];

            for(Annotation annotation : annotations){
                if(annotation.annotationType().equals(ID.class)){
                    return parameterValue;
                }
            }
        }
        return null;
    }

    public static HashMap<String, Object> getWhereArgumentsFromParameters(Method method, Object[] args){

        HashMap<String, Object> where = new HashMap<>();
        Annotation[][] parameterAnnotations = method.getParameterAnnotations();
        int i=0;
        for(Annotation[] annotations : parameterAnnotations){
            Object parameterValue = args[i++];
            for(Annotation annotation : annotations){
                if(annotation.annotationType().equals(Equals.class)){
                    where.put(((Equals) annotation).value(), parameterValue);
                    break;
                }
            }
        }
        return where;
    }

    public static RepositoryTypesMethods getRepositoryMethodType(Method method, Class cls){
        if(isAGetMethod(method, cls))
            return RepositoryTypesMethods.GET_METHOD;
        if(isAGetAllMethod(method, cls))
            return RepositoryTypesMethods.GET_ALL_METHOD;
        return null;
    }

    private static Boolean isAGetMethod(Method method, Class cls){
        return (method.isAnnotationPresent(GET.class) && (!MirrorTypeHelper.extendsClass(List.class, cls)));
    }

    private static Boolean isAGetAllMethod(Method method, Class cls){
        return (method.isAnnotationPresent(GET.class) && MirrorTypeHelper.extendsClass(List.class, cls));
    }

    private static Class<?> getCallbackGenericResponseType(Type type) {

        if (type instanceof ParameterizedType) {

            Type callbackType = getParameterUpperBound((ParameterizedType) type);

            if (callbackType instanceof Class)
                return (Class<?>) callbackType;

            return getCallbackGenericResponseType(type);
        }
        return null;
    }

    public static WebCallbackResponseType getCallbackResponseType(Method method) {

        // Asynchronous methods should have a Callback type as the last argument.
        Type lastArgType = null;
        Type[] parameterTypes = method.getGenericParameterTypes();

        if (parameterTypes.length > 0)
            lastArgType = parameterTypes[parameterTypes.length - 1];

        lastArgType = Types.getSupertype(lastArgType, Types.getRawType(lastArgType), Callback.class);

        if (lastArgType instanceof ParameterizedType) {
            Type callbackType = getParameterUpperBound((ParameterizedType) lastArgType);

            if (callbackType instanceof Class)
                return new WebCallbackResponseType((Class<?>) callbackType, null);

            if (callbackType instanceof ParameterizedType) {

                Type callbackResponseType = ((ParameterizedType) callbackType).getRawType();
                Class callbackGenericType = getCallbackGenericResponseType(callbackType);

                if (callbackResponseType instanceof Class)
                    return new WebCallbackResponseType((Class) callbackResponseType,callbackGenericType);
            }
        }
        return null;
    }

    private static Type getParameterUpperBound(ParameterizedType type) {
        Type[] types = type.getActualTypeArguments();
        for (int i = 0; i < types.length; i++) {
            Type paramType = types[i];
            if (paramType instanceof WildcardType) {
                types[i] = ((WildcardType) paramType).getUpperBounds()[0];
            }
        }
        return types[0];
    }

}
