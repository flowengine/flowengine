package flowengine.core;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import com.google.gson.Gson;

import flowengine.Flow;
import flowengine.FlowEngine;
import flowengine.TransitionManager;

import flowengine.annotations.flow.FlowSteps;
import flowengine.annotations.flow.methods.GetModel;
import flowengine.annotations.flow.methods.SetModel;

import flowengine.core.methods.Action;
import flowengine.core.methods.ActionFactory;

import java.io.*;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import java.util.Set;

public class FlowHandler implements InvocationHandler, StorageHandler {

    static Map<Flow, TransitionManager> flowTransitionManagerMap = new LinkedHashMap<>();
    private Class<? extends Flow> flowInterface;
    private FlowContext flowContext;
    private String uuid;
    private Context context;


    public FlowHandler(Class<? extends Flow> flowInterface, String uuid, Context context) {
        this.flowInterface = flowInterface;
        this.uuid = uuid;
        this.context = context;
        this.flowContext = new FlowContext();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj) || ((InnerFlow) obj).getUuid().equals(this.uuid);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        // If the method is a method from Object then defer to normal invocation.
        if (method.getDeclaringClass() == Object.class) {
            return method.invoke(this, args);
        }
        if (method.isAnnotationPresent(GetModel.class)) {
            flowengine.core.methods.GetModel getModel = new flowengine.core.methods.GetModel((Flow) proxy, method, args, flowInterface, getOrCreateTransitionManager((Flow) proxy), this);
            return getModel.invoke();
        } else if (method.isAnnotationPresent(SetModel.class)) {
            flowengine.core.methods.SetModel setModel = new flowengine.core.methods.SetModel(method,(Flow) proxy, flowInterface, getOrCreateTransitionManager((Flow) proxy), args, args[0], this);
            setModel.invoke();
        } else if (method.getName().equals("saveInstanceState")) {
            saveInstanceState((Bundle) args[0]);
        } else if (method.getName().equals("initSavedInstanceState")) {
            getOrCreateTransitionManager((Flow) proxy);
            initSavedInstanceState((Bundle) args[0]);
        } else if (method.getName().equals("getUuid")) {
            return getUuid();
        } else if (method.getName().equals("setFlowEngineInitialContext")) {
            this.context = (Context) args[0];
        }  else if (method.getName().equals("getFlowEngineInitialContext")) {
            return this.context;
        } else if (method.getName().equals("pause")) {
            pause(getOrCreateTransitionManager((Flow) proxy));
        } else {
            Action flowengineAction = ActionFactory.getInstance().getMethod(method, (Flow) proxy, flowInterface, getOrCreateTransitionManager((Flow) proxy), args);
            if (flowengineAction != null) {
                flowengineAction.invoke();
            }
        }
        return null;
    }

    private String getUuid() {
        return this.uuid;
    }

    private void pause(TransitionManager transitionManager) {
        if (flowInterface.getAnnotation(FlowSteps.class).persistable()) {
            Bundle bundle = new Bundle();
            saveInstanceState(bundle);
            Activity activity = FlowEngine.currentActivity;
            File flowFile = new File(activity.getFilesDir(), flowInterface.getClass().getCanonicalName());
            File modelFile = new File(activity.getFilesDir(), flowInterface.getClass().getCanonicalName() + "_model");
            try {
                FileOutputStream flowFileOutputStream = new FileOutputStream(flowFile);
                ObjectOutputStream flowOutputStream = new ObjectOutputStream(flowFileOutputStream);
                flowOutputStream.writeObject(transitionManager.serialize());
                flowOutputStream.close();
                flowFileOutputStream.close();
                flowContext.serialize(modelFile);
            } catch (Exception e) {
                throw new RuntimeException("Unable to save current flow status");
            }
        }
    }

    private TransitionManager getOrCreateTransitionManager(Flow flow) throws Exception {
        TransitionManager transitionManager = flowTransitionManagerMap.get(flow);
        if (transitionManager != null) {
            return transitionManager;
        }
        Class<? extends Flow> clazz = flowInterface;
        if (clazz.isAnnotationPresent(FlowSteps.class)) {
            FlowSteps annotation = clazz.getAnnotation(FlowSteps.class);
            transitionManager = new StepsTransitionManager(annotation.sequences(), annotation.mainSequenceId());
            if (annotation.persistable()) {
                transitionManager = getPersistedTransitionManagerStatusIfExists(transitionManager);
            }
        } else {
            throw new RuntimeException("Current flow does not define steps");
        }
        flowTransitionManagerMap.put(flow, transitionManager);
        return transitionManager;
    }

    private TransitionManager getPersistedTransitionManagerStatusIfExists(TransitionManager transitionManager) {
        Activity activity = FlowEngine.currentActivity;
        File flowFile = new File(activity.getFilesDir(), flowInterface.getClass().getCanonicalName());
        File modelFile = new File(activity.getFilesDir(), flowInterface.getClass().getCanonicalName() + "_model");
        if (flowFile.exists()) {
            try {
                FileInputStream fin = new FileInputStream(flowFile);
                ObjectInputStream objectInputStream = new ObjectInputStream(fin);
                transitionManager.deserialize((Serializable) objectInputStream.readObject());
                objectInputStream.close();
                fin.close();
                flowContext.deserialize(modelFile);
            } catch (Exception e) {
                throw new RuntimeException("Unable to restor saved flow.");
            }
        }
        return transitionManager;
    }

    private void saveInstanceState(Bundle bundle) {
        if (bundle == null) return;
        new InstanceStateHandler(bundle, flowContext).save();
    }

    private void initSavedInstanceState(Bundle bundle) {
        if (bundle == null) return;
        new InstanceStateHandler(bundle, flowContext).restore();
    }

    public static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }

    public static String getStringFromFile(File fl) throws Exception {
        FileInputStream fin = new FileInputStream(fl);
        String ret = convertStreamToString(fin);
        fin.close();
        return ret;
    }

    @Override
    public <T> T getModel(String key) {
        return flowContext.getModel(key);
    }

    @Override
    public <T> void setModel(String key, T value) {
        flowContext.setModel(key, value);
    }

    @Override
    public Map<String, ? extends Object> getModels() {
        return flowContext.getModels();
    }

    private class FlowContext implements StorageHandler{

        private HashMap<String, Object> context;

        public FlowContext() {
            context = new HashMap<>();
        }

        @Override
        public <T> T getModel(String key) {
            if (!context.containsKey(key)) {
                throw new RuntimeException("Flow has no model for " + key + " in the current context");
            }
            return (T)context.get(key);
        }

        @Override
        public <T> void setModel(String key, T value) {
            context.put(key, value);
        }

        public void serialize(File modelFile) throws IOException {
            Map<String, Object> filteredMap = new HashMap<>();
            Set<String> keys = getModels().keySet();
            for(String key: keys) {
                Object object = getModel(key);
                if (object instanceof Serializable) {
                    filteredMap.put(key, object);
                }
            }
            FileOutputStream modelFileOutputStream = new FileOutputStream(modelFile);
            ObjectOutputStream modelOutputStream = new ObjectOutputStream(modelFileOutputStream);
            modelOutputStream.writeObject(filteredMap);
            modelOutputStream.close();
        }

        public void deserialize(File modelFile) throws IOException {
            FileInputStream fin = new FileInputStream(modelFile);
            ObjectInputStream objectInputStream = new ObjectInputStream(fin);
            try {
                context = (HashMap) objectInputStream.readObject();
            } catch (ClassNotFoundException ex) {
                // Ignore
            }
            objectInputStream.close();
            fin.close();
        }

        @Override
        public Map<String, ? extends Object> getModels() {
            return context;
        }
    }

    private class InstanceStateHandler {
        private Bundle bundle;
        private StorageHandler storageHandler;

        public InstanceStateHandler(Bundle bundle, StorageHandler storageHandler) {
            this.bundle = bundle;
            this.storageHandler = storageHandler;
        }

        public void save() {
            Gson gson = new Gson();
            Map<String, ?> models = storageHandler.getModels();
            Set<String> keys = models.keySet();
            bundle.putString(FlowEngineProcessor.FLOWENGINE_PREFIX + "UUID", uuid);
            for(String key: keys){
                String newKey;
                Object object = models.get(key);
                if (object instanceof Serializable) {
                    newKey = FlowEngineProcessor.FLOWENGINE_PREFIX + key + FlowEngineProcessor.FLOWENGINE_SEPARATOR + Serializable.class.getCanonicalName();
                    bundle.putSerializable(newKey, (Serializable) object);
                } else if (object instanceof Parcelable) {
                    newKey = FlowEngineProcessor.FLOWENGINE_PREFIX + key + FlowEngineProcessor.FLOWENGINE_SEPARATOR + Parcelable.class.getCanonicalName();
                    bundle.putParcelable(newKey, (Parcelable) object);
                } else {
                    newKey = FlowEngineProcessor.FLOWENGINE_PREFIX + key + FlowEngineProcessor.FLOWENGINE_SEPARATOR + object.getClass().getCanonicalName();
                    bundle.putString(newKey, gson.toJson(object));
                }
            }
        }

        public void restore() {
            Gson gson = new Gson();
            Set<String> keys = bundle.keySet();
            keys.remove(FlowEngineProcessor.FLOWENGINE_PREFIX + "UUID");
            for(String key: keys) {
                if (!key.startsWith(FlowEngineProcessor.FLOWENGINE_PREFIX)) continue;
                Object restoredObject = null;
                String objectKey = key.substring(FlowEngineProcessor.FLOWENGINE_PREFIX.length(), key.indexOf(FlowEngineProcessor.FLOWENGINE_SEPARATOR));
                String objectType = key.substring(key.indexOf(FlowEngineProcessor.FLOWENGINE_SEPARATOR) + FlowEngineProcessor.FLOWENGINE_SEPARATOR.length());
                if (Serializable.class.getCanonicalName().equals(objectType)) {
                    restoredObject = bundle.getSerializable(key);
                } else if (Parcelable.class.getCanonicalName().equals(objectType)) {
                    restoredObject = bundle.getParcelable(key);
                } else {
                    try {
                        restoredObject = gson.fromJson(bundle.getString(key), Class.forName(objectType));
                    } catch (ClassNotFoundException e) {
                        // Ignore
                    }
                }
                if (restoredObject != null) {
                    storageHandler.setModel(objectKey, restoredObject);
                }
            }
        }
    }
}
