package flowengine.core.processors;

import java.util.List;

/**
 * Created by florencia on 09/08/15.
 */
public final class PersistableModelBuilder {

    private static String className = "PersistableModelBuilder";
    private static String PACKAGE_NAME = "generated";
    private List<String> persistableModelClassNames;


    public PersistableModelBuilder(List<String> persistableModelClassNames) {
        this.persistableModelClassNames = persistableModelClassNames;
    }

    public static String getFqcn() {
        return PACKAGE_NAME + "." + className;
    }

    public String brewJava() {
        StringBuilder builder = new StringBuilder();
        builder.append("// Generated code from FlowEngine. Do not modify!\n");
        builder.append("package ").append(PACKAGE_NAME).append(";\n\n");

        builder.append("import flowengine.FlowEngine.Builder;\n");
        builder.append("import flowengine.core.persistence.strategies.PersistenceStrategyBuilder;\n");
        builder.append("import flowengine.core.persistence.strategies.PersistenceStrategy;\n");
        builder.append("import android.content.Context;\n\n");

        builder.append("public class ").append(className).append(" implements Builder { \n\n");

        emitPersistableModelBuilder(builder);
        builder.append("}\n");
        return builder.toString();
    }

    public void emitPersistableModelBuilder(StringBuilder builder) {

        builder.append(" public void build(Object o) {\n");
        builder.append("    PersistenceStrategy persistenceStrategy = PersistenceStrategyBuilder.getInstance().newInstance((Context) o); \n");

        builder.append("    try{ \n ")
                .append("      Class cls; \n ");
        for (String persistableModelClassName: persistableModelClassNames) {
            builder.append("      cls = Class.forName(\"").append(persistableModelClassName).append("\"); \n")
                    .append("      persistenceStrategy.setPersistableClass(cls); \n");
        }
        builder.append("   } catch (Exception ex) { \n ")
               .append("       throw new RuntimeException(ex); \n")
               .append("   } \n");

        builder.append(" }\n");
    }

}
