package flowengine.core.processors.injectorsParsers;

import flowengine.core.FlowEngineProcessor;
import flowengine.core.processors.*;

import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import java.lang.annotation.Annotation;
import java.util.Map;
import java.util.Set;

/**
 * Created by florencia on 02/07/15.
 */
public abstract class InjectorParser {

    public abstract Class<? extends Annotation> getAnnotationClass();

    public abstract void parse(Element element, Map<TypeElement, FlowInjector> targetClassMap,
                               Set<String> erasedTargetNames, Processor processor) ;


    protected InjectableBinding parseInjectable(Element element, Set<String> erasedTargetNames, Processor processor) {

        boolean hasError = false;
        TypeElement enclosingElement = (TypeElement) element.getEnclosingElement();

        // Verify that the target type is a Class or an Interface.
        TypeMirror elementType = element.asType();
        if (elementType instanceof TypeVariable) {
            TypeVariable typeVariable = (TypeVariable) elementType;
            elementType = typeVariable.getUpperBound();
        }
        if (!processor.isClass(elementType) && !processor.isInterface(elementType)) {
            String className = getAnnotationClass().getSimpleName();
            processor.error(element, "@%s fields must be a Class or be an interface. (%s.%s)",
                    className, enclosingElement.getQualifiedName(), element.getSimpleName());
            hasError = true;
        }

        // Verify common generated code restrictions.
        hasError |= processor.isInaccessibleViaGeneratedCode(getAnnotationClass(), "fields", element);

        if (hasError) {
            return null;
        }

        erasedTargetNames.add(enclosingElement.toString());
        return generateInjectableBinding(element, elementType);
    }

    protected InjectableBinding generateInjectableBinding(Element element, TypeMirror elementType){
        String name = element.getSimpleName().toString();
        String type = elementType.toString();
        return new InjectableBinding(name, type);
    }

    protected FlowInjector getOrCreateTargetClass(Map<TypeElement, FlowInjector> targetClassMap,
                                                  TypeElement enclosingElement, boolean allowMultiple,
                                                  Processor processor) {

        FlowInjector flowInjector = targetClassMap.get(enclosingElement);
        if (flowInjector == null) {
            String targetType = enclosingElement.getQualifiedName().toString();
            String classPackage = processor.getPackageName(enclosingElement);
            String className = processor.getClassName(enclosingElement, classPackage) + FlowEngineProcessor.SUFFIX;
            flowInjector = new FlowInjector(classPackage, className, targetType);
            targetClassMap.put(enclosingElement, flowInjector);
        } else if (!allowMultiple) {
            return null;
        }
        return flowInjector;
    }

}
