package flowengine.core.processors.injectorsParsers;

import flowengine.annotations.injectable.Persistable;
import flowengine.core.persistence.BasicDao;
import flowengine.core.persistence.Dao;
import flowengine.core.processors.*;

import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by florencia on 02/07/15.
 */
public class PersistableInjectorParser extends InjectorParser {

    public Class<? extends Annotation> getAnnotationClass(){
        return Persistable.class;
    }

    public void parse(Element element, Map<TypeElement, FlowInjector> targetClassMap,
                      Set<String> erasedTargetNames, Processor processor) {

        TypeElement enclosingElement = (TypeElement) element.getEnclosingElement();
        InjectableBindingGeneric binding = (InjectableBindingGeneric) parseInjectable(element, erasedTargetNames, processor);

        Class daoClass = processor.isClass(element.asType()) ? BasicDao.class : Dao.class;
        String daoClassName = MirrorTypeHelper.buildGenericTypeFullName(daoClass);

        if (!processor.isSubtypeOfType(element.asType(), daoClassName)) {
            processor.error(element, "@Persistable must implement Dao Interface. (%s.%s)",
                    enclosingElement.getQualifiedName(), element.getSimpleName());
        }

        FlowInjector viewInjector = getOrCreateTargetClass(targetClassMap, enclosingElement, true, processor);
        viewInjector.addInjectable(InjectableType.PERSISTABLE, binding);
    }

    protected InjectableBindingGeneric generateInjectableBinding(Element element, TypeMirror elementType){

        String name = element.getSimpleName().toString();
        String type = elementType.toString();
        TypeMirror[] typeMirrors = MirrorTypeHelper.getGenericType(element.asType());

        List<String> genericsFields = new ArrayList<>();
        for (TypeMirror typeMirror : typeMirrors)
            genericsFields.add(typeMirror.toString());

        return new InjectableBindingGeneric(name, type, genericsFields);
    }

}
