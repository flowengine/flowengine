package flowengine.core.processors.validators;

import flowengine.core.processors.MirrorTypeHelper;
import flowengine.core.processors.ValidatorProcessor;

import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import java.lang.annotation.Annotation;

public class Jump extends Validator {

    @Override
    protected Class<? extends Annotation> getAnnotation() {
        return flowengine.annotations.flow.methods.Jump.class;
    }

    @Override protected boolean validateElement(Element element, ValidatorProcessor processor) {
        TypeMirror elementType = element.asType();
        if (elementType instanceof TypeVariable) {
            TypeVariable typeVariable = (TypeVariable) elementType;
            elementType = typeVariable.getUpperBound();
        }
        if (!MirrorTypeHelper.isInterface(element.getEnclosingElement().asType())
                && !MirrorTypeHelper.isMethod(element.asType())
                && !validateMethodReturnType(element)) {
            setError(new Error(element, "@" + getAnnotation().getSimpleName() + " must be a method inside an interface that extends Flow with no return type"));
            return false;
        }
        if (element.getAnnotation(flowengine.annotations.flow.methods.Next.class) != null) {
            setError(new Error(element, "@" + getAnnotation().getSimpleName() + " can not be used with @Next"));
        }
        if (element.getAnnotation(flowengine.annotations.flow.methods.GetModel.class) != null) {
            setError(new Error(element, "@" + getAnnotation().getSimpleName() + " can not be used with @GetModel"));
        }
        if (element.getAnnotation(flowengine.annotations.flow.methods.SetModel.class) != null) {
            setError(new Error(element, "@" + getAnnotation().getSimpleName() + " can not be used with @SetModel"));
        }
        processor.addProcessedElement(getAnnotation(), elementType);
        return true;
    }

    protected boolean validateMethodReturnType(Element element) {
        if (!((ExecutableElement) element).getReturnType().getKind().equals(TypeKind.VOID)) {
            setError(new Error(element, "@" + getAnnotation().getSimpleName() + " methods must have no return value."));
            return false;
        }
        return true;
    }
}
