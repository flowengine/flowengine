package flowengine.core.processors;

import javax.annotation.processing.Filer;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.tools.Diagnostic;
import java.lang.annotation.Annotation;
import java.util.Set;

public abstract class Processor {
    protected ProcessingEnvironment processingEnv;
    protected Elements elementUtils;
    protected Filer filer;

    public Processor(ProcessingEnvironment env) {
        processingEnv = env;
        elementUtils = env.getElementUtils();
        filer = env.getFiler();
    }

    public abstract Set<String> getSupportedAnnotationTypes();

    public abstract boolean process(Set<? extends TypeElement> elements, RoundEnvironment env);

    public void error(Element element, String message, Object... args) {
        if (args.length > 0) {
            message = String.format(message, args);
        }
        processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, message, element);
    }

    public static String getClassName(TypeElement type, String packageName) {
        int packageLen = packageName.length() + 1;
        return type.getQualifiedName().toString().substring(packageLen).replace('.', '$');
    }

    public String getPackageName(TypeElement type) {
        return elementUtils.getPackageOf(type).getQualifiedName().toString();
    }

    public boolean isInterface(TypeMirror typeMirror) {
        return MirrorTypeHelper.isInterface(typeMirror);
    }

    public boolean isClass(TypeMirror typeMirror) {
        return MirrorTypeHelper.isClass(typeMirror);
    }

    public boolean isSubtypeOfType(TypeMirror typeMirror, String otherType) {
        return MirrorTypeHelper.isSubtypeOfType(typeMirror, otherType);
    }

    public TypeMirror[] getGenericType(final TypeMirror type){
        return MirrorTypeHelper.getGenericType(type);
    }

    public boolean isInaccessibleViaGeneratedCode(Class<? extends Annotation> annotationClass,
                                                   String targetThing, Element element) {
        boolean hasError = false;
        TypeElement enclosingElement = (TypeElement) element.getEnclosingElement();

        // Verify method modifiers.
        Set<Modifier> modifiers = element.getModifiers();
        if (modifiers.contains(Modifier.PRIVATE) || modifiers.contains(Modifier.STATIC)) {
            error(element, "@%s %s must not be private or static. (%s.%s)",
                    annotationClass.getSimpleName(), targetThing, enclosingElement.getQualifiedName(),
                    element.getSimpleName());
            hasError = true;
        }

        // Verify containing type.
        if (enclosingElement.getKind() != ElementKind.CLASS) {
            error(enclosingElement, "@%s %s may only be contained in classes. (%s.%s)",
                    annotationClass.getSimpleName(), targetThing, enclosingElement.getQualifiedName(),
                    element.getSimpleName());
            hasError = true;
        }

        // Verify containing class visibility is not private.
        if (enclosingElement.getModifiers().contains(Modifier.PRIVATE)) {
            error(enclosingElement, "@%s %s may not be contained in private classes. (%s.%s)",
                    annotationClass.getSimpleName(), targetThing, enclosingElement.getQualifiedName(),
                    element.getSimpleName());
            hasError = true;
        }

        return hasError;
    }

    public ProcessingEnvironment getProcessingEnv() {
        return processingEnv;
    }
}
