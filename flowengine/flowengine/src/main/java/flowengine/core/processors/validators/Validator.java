package flowengine.core.processors.validators;

import flowengine.core.processors.ValidatorProcessor;

import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Element;
import java.lang.annotation.Annotation;

public abstract class Validator {
    private Error error;

    public boolean validate(RoundEnvironment env, ValidatorProcessor processor) {
        for (Element element : env.getElementsAnnotatedWith(getAnnotation())) {
            if (!validateElement(element, processor))
                return false;
        }
        return true;
    }

    protected abstract Class<? extends Annotation> getAnnotation();
    protected abstract boolean validateElement(Element element, ValidatorProcessor processor);

    public String getSupportedAnnotationType() {
        return getAnnotation().getCanonicalName();
    }

    public Error getError() {
        return error;
    }

    protected void setError(Error err) {
        this.error = err;
    }

    public class Error {
        Element element;
        String message;

        public Error(Element element, String message){
            this.element = element;
            this.message = message;
        }

        public Element getElement() {
            return element;
        }

        public String getMessage() {
            return message;
        }
    }
}
