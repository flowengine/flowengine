package flowengine.core.processors.validators;

import flowengine.core.processors.ValidatorProcessor;

import javax.lang.model.element.Element;

public abstract class Forbbiden extends Validator {

    @Override protected boolean validateElement(Element element, ValidatorProcessor processor) {
        setError(new Error(element, "@" + getAnnotation().getSimpleName() + " can not be used."));
        return false;
    }
}
