package flowengine.core.processors;

import flowengine.FlowEngine;
import flowengine.annotations.injectable.Builder;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

@SuppressWarnings("unused")
public class FlowBuilder {

    public Object newInstance(){
        return FlowEngine.getFlow();
    }

}
