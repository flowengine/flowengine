package flowengine.core.processors.validators;

import flowengine.annotations.flow.FEActivity;
import flowengine.core.processors.MirrorTypeHelper;
import flowengine.core.processors.ValidatorProcessor;

import javax.lang.model.element.Element;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import java.lang.annotation.Annotation;

public class AFlow extends Validator {

    @Override
    protected Class<? extends Annotation> getAnnotation() {
        return FEActivity.class;
    }

    @Override protected boolean validateElement(Element element, ValidatorProcessor processor) {
        TypeMirror elementType = element.asType();
        if (elementType instanceof TypeVariable) {
            TypeVariable typeVariable = (TypeVariable) elementType;
            elementType = typeVariable.getUpperBound();
        }
        if (!MirrorTypeHelper.isClass(elementType) ||
                !MirrorTypeHelper.isSubtypeOfType(elementType, "android.support.v4.app.FragmentActivity")) {
            setError(new Error(element, "@FEActivity must be use with FragmentActivity"));
            return false;
        }
        processor.addProcessedElement(getAnnotation(), elementType);
        return true;
    }
}
