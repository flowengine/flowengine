package flowengine.core.processors.validators;

import flowengine.core.processors.MirrorTypeHelper;
import flowengine.core.processors.ValidatorProcessor;

import javax.lang.model.element.Element;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import java.lang.annotation.Annotation;

public class FlowSteps extends Validator {

    @Override
    protected Class<? extends Annotation> getAnnotation() {
        return flowengine.annotations.flow.FlowSteps.class;
    }

    @Override protected boolean validateElement(Element element, ValidatorProcessor processor) {
        TypeMirror elementType = element.asType();
        if (elementType instanceof TypeVariable) {
            TypeVariable typeVariable = (TypeVariable) elementType;
            elementType = typeVariable.getUpperBound();
        }
        if (!MirrorTypeHelper.isInterface(element.asType())) {
            setError(new Error(element, "@" + getAnnotation().getSimpleName() + " must be use with an interface that extends Flow"));
            return false;
        }
        processor.addProcessedElement(getAnnotation(), elementType);
        return true;
    }
}
