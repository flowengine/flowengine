package flowengine.core.processors.validators;

import java.lang.annotation.Annotation;

public class OnFlowFinish extends Callbacks {
    @Override
    protected Class<? extends Annotation> getAnnotation() {
        return flowengine.annotations.callbacks.OnFlowFinish.class;
    }
}
