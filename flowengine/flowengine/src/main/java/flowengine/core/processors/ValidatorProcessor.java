package flowengine.core.processors;

import flowengine.core.processors.validators.*;

import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic;
import java.lang.annotation.Annotation;
import java.util.*;

public class ValidatorProcessor extends Processor {
    private Map<Class<?extends Annotation>, List<TypeMirror>> processedAnnotations;

    private Validator[] validators = new Validator[] {
            new AFlow(),
            new Animation(),
            new Back(),
            new BeforeJump(),
            new BeforeTransition(),
            new BreadCrumbShortTitle(),
            new Flow(),
            new FlowSteps(),
            new Jump(),
            new Next(),
            new OnFlowFinish(),
            new Restart(),
            new Resume(),
            new StepContainer(),
            new Transition(),
            new Validate(),
    };

    public ValidatorProcessor(ProcessingEnvironment env) {
        super(env);
        this.processedAnnotations = new HashMap<>();
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        Set<String> set = new LinkedHashSet<>();
        for (Validator validator: validators) {
            set.add(validator.getSupportedAnnotationType());
        }
        return set;
    }

    @Override
    public boolean process(Set<? extends TypeElement> elements, RoundEnvironment env) {
        for (Validator validator: validators) {
            if (!validator.validate(env, this)) {
                Validator.Error error = validator.getError();
                error(error.getElement(), error.getMessage());
                return false;
            }
        }
        return true;
    }

    public void addProcessedElement(Class<? extends Annotation> annotationType, TypeMirror processedElement) {
        if (processedAnnotations.containsKey(annotationType)) {
            processedAnnotations.get(annotationType).add(processedElement);
        } else {
            List<TypeMirror> list = new LinkedList();
            list.add(processedElement);
            processedAnnotations.put(annotationType, list);
        }
    }

    public boolean hasAnnotation(Class<? extends Annotation> annotationType, TypeMirror processedElement) {
        return processedAnnotations.containsKey(annotationType) && processedAnnotations.get(annotationType).contains(processedElement);
    }
}
