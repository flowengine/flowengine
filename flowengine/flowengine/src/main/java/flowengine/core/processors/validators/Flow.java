package flowengine.core.processors.validators;

import flowengine.annotations.flow.FEActivity;
import flowengine.core.processors.MirrorTypeHelper;
import flowengine.core.processors.ValidatorProcessor;
import flowengine.core.utils.AnnotationSearcher;

import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import java.lang.annotation.Annotation;

public class Flow extends Validator {

    @Override
    protected Class<? extends Annotation> getAnnotation() {
        return flowengine.annotations.flow.Flow.class;
    }

    @Override protected boolean validateElement(Element element, ValidatorProcessor processor) {
        TypeMirror elementType = element.getEnclosingElement().asType();
        if (elementType instanceof TypeVariable) {
            TypeVariable typeVariable = (TypeVariable) elementType;
            elementType = typeVariable.getUpperBound();
        }

        if (MirrorTypeHelper.isSubtypeOfType(elementType, "android.support.v4.app.Fragment")
                || (MirrorTypeHelper.isSubtypeOfType(elementType, "android.support.v4.app.FragmentActivity")
                    && AnnotationSearcher.findAnnotationForElement((TypeElement) element.getEnclosingElement(), FEActivity.class) != null)) {
            processor.addProcessedElement(getAnnotation(), elementType);
            return true;
        }
        setError(new Error(element, "@Flow can only modify enhanced FragmentActivity (with @FEActivity) or Fragment fields."));
        return false;
    }
}
