package flowengine.core.processors;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import flowengine.FlowEngine;
import flowengine.annotations.injectable.Model;
import flowengine.core.FlowEngineProcessor;
import flowengine.core.InnerFlow;

import java.lang.reflect.Field;

@SuppressWarnings("unused")
public class ModelBuilder {
    private Bundle savedInstanceState;

    public ModelBuilder(Bundle savedInstanceState) {
        this.savedInstanceState = savedInstanceState;
    }

    public Object newInstance(Activity target, String name, String type, String key) {
        InnerFlow innerFlow = (InnerFlow) FlowEngine.getFlow();
        Object o = (innerFlow != null) ? innerFlow.__getFlowEngineInnerFlowModel(key) : null;
        return o;
    }

    public Object newInstance(Fragment target, String name, String type, String key) {
        Activity activity = target.getActivity();
        InnerFlow innerFlow = (InnerFlow) FlowEngine.getFlow();
        Object o = (innerFlow != null) ? innerFlow.__getFlowEngineInnerFlowModel(key) : null;
        if (o != null) return o;
        if (activity != null && !key.isEmpty() && !key.equals("null")) {
            Class<?> cls = activity.getClass();
            String clsName = cls.getName();
            while (!clsName.startsWith(FlowEngineProcessor.ANDROID_PREFIX) && !clsName.startsWith(FlowEngineProcessor.JAVA_PREFIX)) {
                for (Field field : cls.getDeclaredFields()) {
                    if (field.isAnnotationPresent(Model.class)) {
                        String fieldKey = field.getAnnotation(Model.class).value();
                        if (fieldKey.equals(key) || field.getType().getName().equals(type)) {
                            try {
                                field.setAccessible(true);
                                Object obj = field.get(activity);
                                if (innerFlow != null) innerFlow.__setFlowEngineInnerFlowModel(obj, key);
                                return obj;
                            } catch (IllegalAccessException e) {
                                return null;
                            }
                        }
                    }
                }
                cls = cls.getSuperclass();
                clsName = cls.getName();
            }
        }
        return null;
    }

}
