package flowengine.core.processors.validators;

import java.lang.annotation.Annotation;

public class StepContainer extends Event {

    @Override
    protected Class<? extends Annotation> getAnnotation() {
        return flowengine.annotations.flow.StepContainer.class;
    }
}
