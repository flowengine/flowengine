package flowengine.core.processors;

import flowengine.annotations.injectable.PersistableModel;

import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.Writer;
import java.lang.annotation.Annotation;
import java.util.*;

/**
 * Created by florencia on 08/08/15.
 */
public class PersistableModelBuilderProcessor extends Processor {

    public PersistableModelBuilderProcessor(ProcessingEnvironment env) {
        super(env);
    }

    public Class<? extends Annotation> getAnnotationClass() {
        return PersistableModel.class;
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        Set<String> supportTypes = new LinkedHashSet<>();
        supportTypes.add(getAnnotationClass().getCanonicalName());
        return supportTypes;
    }

    @Override
    public boolean process(Set<? extends TypeElement> elements, RoundEnvironment env) {
        List<String> persistableModelClassNames = new ArrayList<>();

        for (Element element : env.getElementsAnnotatedWith(getAnnotationClass())) {
            String className = parse(element);
            if(!persistableModelClassNames.contains(className))
                persistableModelClassNames.add(className);
        }
        if(persistableModelClassNames.size() > 0)
            saveFile(persistableModelClassNames);

        return true;
    }

    public String parse(Element element) {

        TypeMirror elementType = element.asType();

        if (!this.isClass(elementType)) {
            String className = getAnnotationClass().getSimpleName();
            this.error(element, "@%s fields must be a Class. (%s)",
                    className, element.getSimpleName());
        }

        return element.toString();
    }


    private void saveFile(List<String> persistableModelClassNames){

        PersistableModelBuilder persistableModelBuilder = new PersistableModelBuilder(persistableModelClassNames);
        try {
            JavaFileObject jfo = filer.createSourceFile(persistableModelBuilder.getFqcn());
            Writer writer = jfo.openWriter();
            writer.write(persistableModelBuilder.brewJava());
            writer.flush();
            writer.close();
        } catch (IOException e) {
            processingEnv.getMessager().printMessage(Diagnostic.Kind.MANDATORY_WARNING, "ERROR : " + e.getMessage());
        }

    }

}
