package flowengine.core.processors.validators;

import java.lang.annotation.Annotation;

public class Animation extends Event {

    @Override
    protected Class<? extends Annotation> getAnnotation() {
        return flowengine.annotations.flow.Transition.class;
    }
}
