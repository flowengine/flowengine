package flowengine.core.processors;

/**
 * Created by florencia on 01/07/15.
 */
public final class InjectableType {

    public final static String PERSISTABLE = "Persistable";
    public final static String SERVICE = "Service";
    public final static String MODEL = "Models";
}
