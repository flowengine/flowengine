package flowengine.core.processors.validators;

import flowengine.annotations.flow.FEActivity;
import flowengine.core.processors.MirrorTypeHelper;
import flowengine.core.processors.ValidatorProcessor;
import flowengine.core.utils.AnnotationSearcher;

import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;

public abstract class Callbacks extends Validator {

    @Override protected boolean validateElement(Element element, ValidatorProcessor processor) {
        TypeMirror elementType = element.getEnclosingElement().asType();
        if (elementType instanceof TypeVariable) {
            TypeVariable typeVariable = (TypeVariable) elementType;
            elementType = typeVariable.getUpperBound();
        }
        if (element.getKind() != ElementKind.METHOD) {
            setError(new Error(element, "@" + getAnnotation().getSimpleName() + " can only modify public methods inside enhanced FragmentActivity (with @FEActivity) or Fragments."));
            return false;
        }
        if (processor.isInaccessibleViaGeneratedCode(getAnnotation(), "methods", element)) {
            return false;
        }
        if (!validateMethodReturnType(element)) {
            return false;
        }
        if (MirrorTypeHelper.isSubtypeOfType(elementType, "android.support.v4.app.Fragment")
                || (MirrorTypeHelper.isSubtypeOfType(elementType, "android.support.v4.app.FragmentActivity")
                    && AnnotationSearcher.findAnnotationForElement((TypeElement) element.getEnclosingElement(), FEActivity.class) != null)) {
            processor.addProcessedElement(getAnnotation(), elementType);
            return true;
        }
        setError(new Error(element, "@" + getAnnotation().getSimpleName() + " can only modify public methods inside enhanced FragmentActivity (with @FEActivity) or Fragments."));
        return false;
    }

    protected boolean validateMethodReturnType(Element element) {
        if (!((ExecutableElement) element).getReturnType().getKind().equals(TypeKind.VOID)) {
            setError(new Error(element, "@" + getAnnotation().getSimpleName() + " methods must have no return value."));
            return false;
        }
        return true;
    }
}
