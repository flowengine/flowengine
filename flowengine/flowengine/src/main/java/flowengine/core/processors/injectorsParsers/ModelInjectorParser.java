package flowengine.core.processors.injectorsParsers;

import flowengine.annotations.injectable.Model;
import flowengine.annotations.injectable.Service;
import flowengine.core.processors.FlowInjector;
import flowengine.core.processors.InjectableBinding;
import flowengine.core.processors.InjectableType;
import flowengine.core.processors.Processor;

import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import java.lang.annotation.Annotation;
import java.util.Map;
import java.util.Set;

public class ModelInjectorParser extends InjectorParser {

    public Class<? extends Annotation> getAnnotationClass(){
        return Model.class;
    }

    public void parse(Element element, Map<TypeElement, FlowInjector> targetClassMap,
                      Set<String> erasedTargetNames, Processor processor) {

        TypeElement enclosingElement = (TypeElement) element.getEnclosingElement();
        InjectableBinding binding = parseInjectable(element, erasedTargetNames, processor);
        binding.setKey(element.getAnnotation(Model.class).value());
        FlowInjector viewInjector = getOrCreateTargetClass(targetClassMap, enclosingElement, true, processor);
        viewInjector.addInjectable(InjectableType.MODEL, binding);
    }

}
