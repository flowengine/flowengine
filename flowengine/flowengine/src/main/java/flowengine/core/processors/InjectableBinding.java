package flowengine.core.processors;

public class InjectableBinding {

    private final String name;
    private final String type;
    private String key;

    public InjectableBinding(String name, String type) {
        this(name, type, null);
    }

    public InjectableBinding(String name, String type, String key) {
        this.name = name;
        this.type = type;
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
