package flowengine.core.processors;

import android.content.Context;
import flowengine.core.persistence.BasicDao;
import flowengine.core.persistence.strategies.PersistenceStrategy;
import flowengine.core.persistence.strategies.PersistenceStrategyBuilder;
import android.support.v4.app.Fragment;

/**
 * Class used to build the corresponding persistable entity.
 */
public class PersistableBuilder {

    private PersistenceStrategy persistenceStrategy;

    /**
     * @param object
     *      It should extend {@link Context} or {@link Fragment}. Associated content from the application.
     *      This is needed to locate the database.
     */
    public PersistableBuilder(Object object){
        Context context = Fragment.class.isAssignableFrom(object.getClass()) ? ((Fragment) object).getActivity() : (Context) object;
        persistenceStrategy = PersistenceStrategyBuilder.getInstance().newInstance(context);
    }

    /**
     * Create a new dao for the type passed in.
     *
     * @param type
     *        The type of the object for which the dao has to be created.
     * @return
     *      A {@link BasicDao} corresponding to the type passed in.
     */
    public Object newInstance(String type){
        Class<?> targetClazz;
        try {
            targetClazz = Class.forName(type);
            return new BasicDao<>(targetClazz, persistenceStrategy);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;
    }

}
