package flowengine.core.processors.validators;

import java.lang.annotation.Annotation;

public class Restart extends Forbbiden {

    @Override
    protected Class<? extends Annotation> getAnnotation() {
        return flowengine.annotations.flow.methods.Restart.class;
    }
}
