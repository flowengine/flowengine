package flowengine.core.processors;

import flowengine.core.processors.injectorsParsers.*;

import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.*;

public class InjectorProcessor extends Processor {

    public InjectorProcessor(ProcessingEnvironment env) {
        super(env);
    }

    private InjectorParser[] injectorsParser = new InjectorParser[]{
            new FlowInjectorParser(),
            new ServiceInjectorParser(),
            new PersistableInjectorParser(),
            new ModelInjectorParser()
    };


    @Override public Set<String> getSupportedAnnotationTypes() {
        Set<String> supportTypes = new LinkedHashSet<>();
        for(InjectorParser injectorParser: injectorsParser)
            supportTypes.add(injectorParser.getAnnotationClass().getCanonicalName());
        return supportTypes;
    }

    @Override public boolean process(Set<? extends TypeElement> elements, RoundEnvironment env) {
        Map<TypeElement, FlowInjector> targetClassMap = findAndParseTargets(env);
        for (Map.Entry<TypeElement, FlowInjector> entry : targetClassMap.entrySet()) {
            TypeElement typeElement = entry.getKey();
            FlowInjector viewInjector = entry.getValue();
            try {
                JavaFileObject jfo = filer.createSourceFile(viewInjector.getFqcn(), typeElement);
                Writer writer = jfo.openWriter();
                writer.write(viewInjector.brewJava());
                writer.flush();
                writer.close();
            } catch (IOException e) {
                error(typeElement, "Unable to write injector for type %s: %s", typeElement, e.getMessage());
            }
        }

        return true;
    }

    private void logError(Element element, Exception e, String injectableName){
        StringWriter stackTrace = new StringWriter();
        e.printStackTrace(new PrintWriter(stackTrace));
        error(element, "Unable to generate annotations injector for %s.\n\n%s", injectableName , stackTrace);
    }

    private Map<TypeElement, FlowInjector> findAndParseTargets(RoundEnvironment env) {
        Map<TypeElement, FlowInjector> targetClassMap = new LinkedHashMap<>();
        Set<String> erasedTargetNames = new LinkedHashSet<>();
        for(InjectorParser injectorParser: injectorsParser){
            for (Element element : env.getElementsAnnotatedWith(injectorParser.getAnnotationClass())) {
                try {
                    injectorParser.parse(element, targetClassMap, erasedTargetNames, this);
                } catch (Exception e) {
                    logError(element, e, injectorParser.getAnnotationClass().getCanonicalName());
                }
            }
        }
        return targetClassMap;
    }
}
