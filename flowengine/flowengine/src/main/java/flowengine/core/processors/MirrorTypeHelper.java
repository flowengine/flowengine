package flowengine.core.processors;

import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.*;
import javax.lang.model.util.SimpleTypeVisitor6;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

public class MirrorTypeHelper {

    public static boolean isInterface(TypeMirror typeMirror) {
        return typeMirror instanceof DeclaredType && ((DeclaredType) typeMirror).asElement().getKind() == ElementKind.INTERFACE;
    }

    public static boolean isClass(TypeMirror typeMirror) {
        return typeMirror instanceof DeclaredType && ((DeclaredType) typeMirror).asElement().getKind() == ElementKind.CLASS;
    }

    public static boolean isMethod(TypeMirror typeMirror) {
        return typeMirror instanceof DeclaredType && ((DeclaredType) typeMirror).asElement().getKind() == ElementKind.METHOD;
    }

    public static boolean isField(TypeMirror typeMirror) {
        return typeMirror instanceof DeclaredType && ((DeclaredType) typeMirror).asElement().getKind() == ElementKind.FIELD;
    }

    public static boolean isSubtypeOfType(TypeMirror typeMirror, String otherType) {
        if (otherType.equals(typeMirror.toString())) {
            return true;
        }
        if (!(typeMirror instanceof DeclaredType)) {
            return false;
        }
        DeclaredType declaredType = (DeclaredType) typeMirror;
        List<? extends TypeMirror> typeArguments = declaredType.getTypeArguments();

        if (typeArguments.size() > 0) {

            String typeString = buildGenericTypeFullName(declaredType.asElement().toString(), typeArguments);
            if (typeString.equals(otherType)) {
                return true;
            }
        }
        Element element = declaredType.asElement();
        if (!(element instanceof TypeElement)) {
            return false;
        }
        TypeElement typeElement = (TypeElement) element;
        TypeMirror superType = typeElement.getSuperclass();
        if (isSubtypeOfType(superType, otherType)) {
            return true;
        }
        for (TypeMirror interfaceType : typeElement.getInterfaces()) {
            if (isSubtypeOfType(interfaceType, otherType)) {
                return true;
            }
        }
        return false;
    }

    public static String buildGenericTypeFullName(String className, List typeArguments) {

        StringBuilder typeString = new StringBuilder(className);
        typeString.append('<');
        for (int i = 0; i < typeArguments.size(); i++) {
            if (i > 0) {
                typeString.append(',');
            }
            typeString.append('?');
        }
        typeString.append('>');
        return typeString.toString();
    }

    public static String buildGenericTypeFullName(Class<?> cls) {
        return buildGenericTypeFullName(cls.getCanonicalName(), Arrays.asList(cls.getTypeParameters()));
    }

    public static TypeMirror[] getGenericType(final TypeMirror type) {
        final TypeMirror[] result = { null };

        type.accept(new SimpleTypeVisitor6<Void, Void>() {

            @Override
            public Void visitDeclared(DeclaredType declaredType, Void v) {
                List<? extends TypeMirror> typeArguments = declaredType.getTypeArguments();
                if (!typeArguments.isEmpty())
                    result[0] = typeArguments.get(0);
                return null;
            }

            @Override
            public Void visitPrimitive(PrimitiveType primitiveType, Void v) { return null; }

            @Override
            public Void visitArray(ArrayType arrayType, Void v) { return null; }

            @Override
            public Void visitTypeVariable(TypeVariable typeVariable, Void v) { return null; }

            @Override
            public Void visitError(ErrorType errorType, Void v) { return null; }

            @Override
            protected Void defaultAction(TypeMirror typeMirror, Void v) {
                throw new UnsupportedOperationException();
            }

        }, null);

        return result;
    }

    public static Class<?> getGenericType(Type type){
        return  (Class<?>)((ParameterizedType) type).getActualTypeArguments()[0];
    }

    public static Boolean extendsClass(Class superClass, Class subClass){
        return superClass.isAssignableFrom(subClass);
    }
}
