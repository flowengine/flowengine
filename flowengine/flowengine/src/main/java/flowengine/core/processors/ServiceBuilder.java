package flowengine.core.processors;

import flowengine.annotations.injectable.Builder;
import flowengine.core.Environment;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

@SuppressWarnings("unused")
public class ServiceBuilder {

    public Object newInstance(Object target, String type) {
        Environment env = Environment.getInstance(target);
        Class<?> targetClazz;
        try {
            targetClazz = Class.forName(env.getProperty(type));
            Method method = getBuilderMethod(targetClazz);
            if (method != null) {
                return method.invoke(null);
            }
            return targetClazz.newInstance();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;
    }

    private Method getBuilderMethod(Class clazz) throws Exception {
        Method[] methods = clazz.getMethods();
        for (Method method : methods) {
            if (method.getAnnotation(Builder.class) != null) {
                if (isInaccessible(method, clazz)) {
                    return null;
                }
                return method;
            }
        }
        return null;
    }

    private boolean isInaccessible(Method method, Class clazz) {
        int modifiers = method.getModifiers();
        Class returnType = method.getReturnType();
        Class[] parameters = method.getParameterTypes();
        return !(Modifier.isStatic(modifiers)
                && Modifier.isPublic(modifiers)
                && (parameters == null || parameters.length == 0)
                && returnType == clazz);
    }

}
