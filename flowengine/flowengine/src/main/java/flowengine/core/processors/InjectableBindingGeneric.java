package flowengine.core.processors;

import java.util.List;

/**
 * Created by florencia on 31/05/15.
 */
public class InjectableBindingGeneric extends InjectableBinding {


    private final List<String> genericsFields;

    public InjectableBindingGeneric(String name, String type, List<String> genericsFields) {
        super(name, type);
        this.genericsFields = genericsFields;
    }

    public List<String> getGenericsFields() {
        return genericsFields;
    }

}
