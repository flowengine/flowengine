package flowengine.core.processors.injectorsParsers;

import flowengine.annotations.injectable.Service;
import flowengine.core.processors.*;

import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import java.lang.annotation.Annotation;
import java.util.Map;
import java.util.Set;

/**
 * Created by florencia on 02/07/15.
 */
public class ServiceInjectorParser extends InjectorParser {

    public Class<? extends Annotation> getAnnotationClass(){
        return Service.class;
    }

    public void parse(Element element, Map<TypeElement, FlowInjector> targetClassMap,
                      Set<String> erasedTargetNames, Processor processor) {

        TypeElement enclosingElement = (TypeElement) element.getEnclosingElement();
        InjectableBinding binding = parseInjectable(element, erasedTargetNames, processor);
        FlowInjector viewInjector = getOrCreateTargetClass(targetClassMap, enclosingElement, true, processor);
        viewInjector.addInjectable(InjectableType.SERVICE, binding);
    }

}
