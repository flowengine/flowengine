package flowengine.core.processors.validators;

import flowengine.annotations.flow.methods.Back;
import flowengine.annotations.flow.methods.Next;
import flowengine.annotations.flow.methods.Restart;
import flowengine.annotations.flow.methods.Resume;
import flowengine.core.processors.MirrorTypeHelper;
import flowengine.core.processors.ValidatorProcessor;

import javax.lang.model.element.Element;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import java.lang.annotation.Annotation;

public abstract class Event extends Validator {

    @Override protected boolean validateElement(Element element, ValidatorProcessor processor) {
        TypeMirror elementType = element.asType();
        if (elementType instanceof TypeVariable) {
            TypeVariable typeVariable = (TypeVariable) elementType;
            elementType = typeVariable.getUpperBound();
        }
        if (!isInterface(element) && !isEventMethod(element)) {
            setError(new Error(element, "@" + getAnnotation().getSimpleName() + " must be use with an interface that extends Flow or a @Next method"));
            return false;
        }
        processor.addProcessedElement(getAnnotation(), elementType);
        return true;
    }

    protected boolean isInterface(Element element) {
        return MirrorTypeHelper.isInterface(element.asType());
    }

    protected boolean isEventMethod(Element element) {
        return MirrorTypeHelper.isMethod(element.asType()) && (
            element.getAnnotation(Next.class) != null
            || element.getAnnotation(Back.class) != null
            || element.getAnnotation(Resume.class) != null
            || element.getAnnotation(Restart.class) != null);
    }
}
