package flowengine.core.processors;

import java.util.*;

public final class FlowInjector {

    private String classPackage;
    private String className;
    private String targetType;
    private Map<String, List<InjectableBinding>> injectableBindings;
    private InjectableBinding flow;

    public FlowInjector(String classPackage, String className, String targetType) {
        this.classPackage = classPackage;
        this.className = className;
        this.targetType = targetType;
        this.injectableBindings = new HashMap<>();
    }

    String getFqcn() {
        return classPackage + "." + className;
    }

    public String brewJava() {
        StringBuilder builder = new StringBuilder();
        builder.append("// Generated code from FlowEngine. Do not modify!\n");
        builder.append("package ").append(classPackage).append(";\n\n");

        builder.append("import flowengine.FlowEngine.Injector;\n");

        builder.append("import flowengine.core.processors.ServiceBuilder;\n\n");
        builder.append("import flowengine.core.processors.FlowBuilder;\n\n");
        builder.append("import flowengine.core.processors.PersistableBuilder;\n\n");
        builder.append("import flowengine.core.processors.ModelBuilder;\n\n");
        builder.append("import android.os.Bundle;\n");

        // public class {{ AUTO-GENERATED CLASS }} implements Injector {
        builder.append("public class ").append(className).append(" implements Injector { \n\n");

        emitInject(builder);
        emitInjectPersistable(builder);
        emitInitFlow(builder);
        emitInjectModels(builder);
        builder.append("}\n");
        return builder.toString();
    }

    public void addInjectable(String type, InjectableBinding injectableBinding) {

        if(injectableBindings.containsKey(type)){
            injectableBindings.get(type).add(injectableBinding);
            return;
        }
        List injectableList = new LinkedList();
        injectableList.add(injectableBinding);
        injectableBindings.put(type, injectableList);
    }

    public void setFlow(InjectableBinding flow) {
        this.flow = flow;
    }

    public void emitInject(StringBuilder builder) {
        builder.append("public void injectServices(Object o) {\n");
        builder.append("    ").append(targetType).append(" target = (").append(targetType).append(") o;\n");
        builder.append("    ServiceBuilder builder = new ServiceBuilder();\n");

        if(!injectableBindings.containsKey(InjectableType.SERVICE)) {
            builder.append("}\n");
            return;
        }

        for (InjectableBinding binding : injectableBindings.get(InjectableType.SERVICE)) {
            if (binding != null)
                builder.append("    target.")
                        .append(binding.getName())
                        .append(" = (")
                        .append(binding.getType())
                        .append(") builder.newInstance(target, \"")
                        .append(binding.getType())
                        .append("\");\n");
        }
        builder.append("}\n");
    }

    public void emitInitFlow(StringBuilder builder) {
        builder.append("public void injectFlow(Object o) {\n");
        if (flow != null){
            builder.append("    ").append(targetType).append(" target = (").append(targetType).append(") o;\n");
            builder.append("    FlowBuilder builder = new FlowBuilder();\n");
            builder.append("    target.")
                    .append(flow.getName())
                    .append(" = (")
                    .append(flow.getType())
                    .append(") builder.newInstance();\n");
        }
        builder.append("}\n");
    }

    public void emitInjectModels(StringBuilder builder) {
        builder.append("public void injectModels(Object o, Bundle savedInstanceState) {\n");
        builder.append("    ").append(targetType).append(" target = (").append(targetType).append(") o;\n");
        builder.append("    ModelBuilder builder = new ModelBuilder(savedInstanceState);\n");

        if(!injectableBindings.containsKey(InjectableType.MODEL)) {
            builder.append("}\n");
            return;
        }

        for (InjectableBinding binding : injectableBindings.get(InjectableType.MODEL)) {
            if (binding != null)
                builder.append("    target.")
                        .append(binding.getName())
                        .append(" = (")
                        .append(binding.getType())
                        .append(") builder.newInstance(target, \"")
                        .append(binding.getName())
                        .append("\", \"")
                        .append(binding.getType())
                        .append("\", \"")
                        .append(binding.getKey())
                        .append("\");\n");
        }
        builder.append("}\n");
    }

    public void emitInjectPersistable(StringBuilder builder) {
        builder.append("public void injectPersistable(Object o) {\n");

        if(!injectableBindings.containsKey(InjectableType.PERSISTABLE)) {
            builder.append("}\n");
            return;
        }
        builder.append("    ").append(targetType).append(" target = (").append(targetType).append(") o;\n");
        builder.append("    PersistableBuilder builder = new PersistableBuilder(o);\n");

        for (InjectableBinding binding : injectableBindings.get(InjectableType.PERSISTABLE)) {
            InjectableBindingGeneric genericBinding = (InjectableBindingGeneric) binding;
            if (genericBinding != null)
                builder.append("    target.")
                        .append(genericBinding.getName())
                        .append(" = (")
                        .append(genericBinding.getType())
                        .append(") builder.newInstance(\"")
                        .append(genericBinding.getGenericsFields().get(0))
                        .append("\");\n");
        }
        builder.append("}\n");
    }

}
