package flowengine.core.processors.injectorsParsers;

import flowengine.annotations.flow.Flow;
import flowengine.core.processors.FlowInjector;
import flowengine.core.processors.InjectableBinding;
import flowengine.core.processors.Processor;

import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import java.lang.annotation.Annotation;
import java.util.Map;
import java.util.Set;

/**
 * Created by florencia on 02/07/15.
 */
public class FlowInjectorParser extends InjectorParser {

    public Class<? extends Annotation> getAnnotationClass(){
        return Flow.class;
    }

    public void parse(Element element, Map<TypeElement, FlowInjector> targetClassMap,
                      Set<String> erasedTargetNames, Processor processor) {

        TypeElement enclosingElement = (TypeElement) element.getEnclosingElement();
        InjectableBinding binding = parseInjectable(element, erasedTargetNames, processor);
        if (!processor.isSubtypeOfType(element.asType(), flowengine.Flow.class.getCanonicalName())) {
            processor.error(enclosingElement, "@Flow can only modify Flow interface fields.");
        }
        FlowInjector viewInjector = getOrCreateTargetClass(targetClassMap, enclosingElement, false, processor);
        if (viewInjector == null) {
            processor.error(enclosingElement, "Can not use @Flow annotation multiple times in the same class.");
        } else {
            viewInjector.setFlow(binding);
        }
    }

}
