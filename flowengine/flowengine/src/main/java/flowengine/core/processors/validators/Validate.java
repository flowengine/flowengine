package flowengine.core.processors.validators;

import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import java.lang.annotation.Annotation;

public class Validate extends Callbacks {

    @Override
    protected Class<? extends Annotation> getAnnotation() {
        return flowengine.annotations.callbacks.Validate.class;
    }

    protected boolean validateMethodReturnType(Element element) {
        if (!Boolean.class.getName().equals(((ExecutableElement) element).getReturnType().toString())
                && !Boolean.class.getSimpleName().toLowerCase().equals(((ExecutableElement) element).getReturnType().toString())) {
            setError(new Error(element, "@" + getAnnotation().getSimpleName() + " methods must return a Boolean object."));
            return false;
        }
        return true;
    }
}
