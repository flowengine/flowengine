package flowengine.core;

import java.io.*;
import java.util.Properties;

@SuppressWarnings("unused")
public class Environment {

    private static final String PROPERTIES_FILENAME = "flowengine.properties";
    private Properties props;
    private static Environment instance;

    public static Environment getInstance(Object o) {
        if (instance == null) {
            instance = new Environment(o);
        }
        return instance;
    }

    private Environment(Object o) {
        props = new Properties();
        InputStream inputStream = null;
        try {
            inputStream = o.getClass().getClassLoader().getResourceAsStream(PROPERTIES_FILENAME);
            props.load(inputStream);
            inputStream.close();
        } catch (IOException e) {
            if (e instanceof InterruptedIOException) {
                Thread.currentThread().interrupt();
            }
            // throw new FileNotFoundException("property file '" + PROPERTIES_FILENAME + "' not found in the classpath");
        } finally {
            if(inputStream != null) {
                try {
                    inputStream.close();
                } catch(InterruptedIOException ignore) {
                    Thread.currentThread().interrupt();
                } catch(Throwable ignore) {
                }
            }
        }
    }

    public Properties getProperties() {
        return props;
    }

    public Object get(Object key) {
        return props.get(key);
    }

    public String getProperty(String key){
        return props.getProperty(key, key);
    }
}
