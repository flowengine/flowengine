package flowengine.core;

import android.support.v4.app.Fragment;

/**
 * Created by Ignacio on 20/04/2015.
 */
public interface FragmentBuilder {
    Fragment createFragment(Class<? extends Fragment> clazz) throws IllegalAccessException, InstantiationException;
}
