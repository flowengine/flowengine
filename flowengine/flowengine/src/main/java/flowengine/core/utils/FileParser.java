package flowengine.core.utils;

import android.content.Context;

import java.io.*;

/**
 * Parser to save and retrieve data from a file.
 */
public class FileParser {

    private Context context;
    private String filename;

    /**
     * Constructor for the parser.
     *
     * @param context
     *            Associated content from the application. This is needed to locate the database.
     * @param filename
     *            Name of the file.
     */
    public FileParser(Context context, String filename){
        this.context = context;
        this.filename = filename;
    }

    /**
     * Write into a file the data passed in.
     *
     * @param data
     *      Data to write into the file.
     * @throws RuntimeException
     *      on any problems writing into the file.
     */
    public void writeToFile(String data) throws RuntimeException {
        try {
            OutputStream outputStream = context.openFileOutput(filename, Context.MODE_PRIVATE);

            if(outputStream != null){
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
                outputStreamWriter.write(data);
                outputStreamWriter.close();
            }
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    /**
     *  Retrieve the data from the file.
     *
     * @return String
     *      The content of the file.
     * @throws FileNotFoundException
     *      If the file does not exist.
     * @throws IOException
     *      on any problems reading from the file.
     */
    public String readFromFile() throws Exception {

        String ret = "";
        String receiveString;
        try {
            InputStream inputStream = context.openFileInput(filename);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            throw e;
        } catch (IOException e) {
            throw e;
        }
        return ret;
    }

}
