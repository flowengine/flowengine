package flowengine.core.utils;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

/**
 * Parser to get data from Android Manifest file.
 */
public class AndroidManifestParser {

    /**
     * Retrieve the value of the meta data's name passed in.
     *
     * @param context
     *      Associated content from the application. This is needed to locate the database.
     * @param name
     *      Name of the meta data to retrieve.
     * @return String
     *      The value of the meta data name, it will be null in case of the meta data does not exist.
     */
    public static String getMetadata(Context context, String name) {
        try {
            ApplicationInfo appInfo = context.getPackageManager().getApplicationInfo(
                    context.getPackageName(), PackageManager.GET_META_DATA);
            if (appInfo.metaData != null)
                return appInfo.metaData.getString(name);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}

