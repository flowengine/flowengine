package flowengine.core.utils;

import flowengine.Flow;
import flowengine.annotations.flow.FEActivity;
import flowengine.core.DummyFlow;
import flowengine.core.FlowEngineProcessor;

import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import java.lang.annotation.Annotation;

public class AnnotationSearcher {


    public static Class<? extends Flow> findAFlowForClass(Class<?> cls) {
        FEActivity annotation = (FEActivity) findAnnotationForClass(cls, FEActivity.class);

        if(annotation != null)
            return annotation.value();

        return DummyFlow.class;
    }

    public static Annotation findAnnotationForElement(TypeElement element, Class<? extends Annotation> annotation) {
        String clsName = element.toString();
        if (clsName.startsWith(FlowEngineProcessor.ANDROID_PREFIX) || clsName.startsWith(FlowEngineProcessor.JAVA_PREFIX)) {
            return null;
        }
        if (element.getAnnotation(annotation) != null) {
            return element.getAnnotation(annotation);
        }
        DeclaredType declaredType = (DeclaredType) element.getSuperclass();
        return findAnnotationForElement((TypeElement) declaredType.asElement(), annotation);
    }

    public static Annotation findAnnotationForClass(Class<?> cls, Class<? extends Annotation> annotation) {
        String clsName = cls.getName();
        if (clsName.startsWith(FlowEngineProcessor.ANDROID_PREFIX) || clsName.startsWith(FlowEngineProcessor.JAVA_PREFIX)) {
            return null;
        }
        if (cls.isAnnotationPresent(annotation)) {
            return cls.getAnnotation(annotation);
        }
        return findAnnotationForClass(cls.getSuperclass(), annotation);
    }

}
