package flowengine.core.utils;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * Created by Ignacio on 04/07/2015.
 */
public class MethodSearcher {

    private static final String ON_EVENT_METHOD_NAME = "onEvent";
    private static final int BRIDGE = 0x40;
    private static final int SYNTHETIC = 0x1000;
    private static int MODIFIERS_IGNORE = Modifier.ABSTRACT | Modifier.STATIC | BRIDGE | SYNTHETIC;

    public static boolean hasEventMethod(Class<?> subscriberClass) {
        String key = subscriberClass.getName();
        Class<?> clazz = subscriberClass;
        StringBuilder methodKeyBuilder = new StringBuilder();
        while (clazz != null) {
            String name = clazz.getName();
            // Starting with EventBus 2.2 we enforced methods to be public (might change with annotations again)
            Method[] methods = clazz.getDeclaredMethods();
            for (Method method : methods) {
                String methodName = method.getName();
                if (name.startsWith("java.") || name.startsWith("javax.") || name.startsWith("android.")) {
                    break;
                }
                if (methodName.startsWith(ON_EVENT_METHOD_NAME)) {
                    int modifiers = method.getModifiers();
                    if ((modifiers & Modifier.PUBLIC) != 0 && (modifiers & MODIFIERS_IGNORE) == 0) {
                        Class<?>[] parameterTypes = method.getParameterTypes();
                        if (parameterTypes.length == 1) {
                            String modifierString = methodName.substring(ON_EVENT_METHOD_NAME.length());
                            return true;
                        }
                    }
                }
            }
            clazz = clazz.getSuperclass();
        }
        return false;
    }
}
