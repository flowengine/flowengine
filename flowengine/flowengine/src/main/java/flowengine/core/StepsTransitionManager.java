package flowengine.core;

import android.app.Activity;
import android.support.v4.app.Fragment;

import flowengine.TransitionManager;
import flowengine.annotations.flow.Join;
import flowengine.annotations.flow.Sequence;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class StepsTransitionManager implements TransitionManager {
    private Stack<StepStatus> stepStatuses;
    private StepStatus persistedStatus;
    private String mainSequenceId;
    private Map<String, Class<? extends Fragment>[]> steps;
    private Map<String, Class<? extends Activity>> exitJumps;
    private Map<String, Join> exitJoins;

    public StepsTransitionManager(Sequence[] alternatives, String mainSequenceId) {
        this.stepStatuses = new Stack<>();
        this.steps = new HashMap<>(alternatives.length);
        this.exitJoins = new HashMap<>(alternatives.length);
        this.exitJumps = new HashMap<>(alternatives.length);
        this.mainSequenceId = mainSequenceId;
        for (Sequence sequence: alternatives) {
            if (this.mainSequenceId.isEmpty()) {
                this.mainSequenceId = sequence.id();
            }
            this.steps.put(sequence.id(), sequence.steps());
            this.exitJumps.put(sequence.id(), sequence.exitJump());
            this.exitJoins.put(sequence.id(), sequence.exitJoin());
        }
        this.persistedStatus = new StepStatus(this.mainSequenceId, 0);
    }

    @Override
    public void deserialize(Serializable stepStatus) {
        this.persistedStatus = (StepStatus) stepStatus;
    }

    @Override
    public Serializable serialize() {
        return this.stepStatuses.peek();
    }

    public boolean canGoBack() {
        return (!stepStatuses.isEmpty() && (stepStatuses.size() > 1 || stepStatuses.peek().stepIndex > stepStatuses.peek().initialStep));
    }

    @Override
    public void clearBackStack() {
        StepStatus stepStatus = stepStatuses.pop();
        stepStatus.initialStep = stepStatus.stepIndex;
        stepStatuses.clear();
        stepStatuses.push(stepStatus);
    }

    @Override
    public String currentSequenceId() {
        if (stepStatuses.isEmpty())
            return "";
        return stepStatuses.peek().sequenceId;
    }

    @Override
    public Fragment next(String eventId) {
        StepStatus stepStatus = new StepStatus(eventId.isEmpty() ? mainSequenceId : eventId, -1);
        if (eventId.isEmpty() && !stepStatuses.isEmpty()) {
            stepStatus = stepStatuses.pop();
        }
        if (steps.containsKey(stepStatus.sequenceId)) {
            try {
                Fragment fragment = steps.get(stepStatus.sequenceId)[++stepStatus.stepIndex].newInstance();
                stepStatuses.push(stepStatus);
                return fragment;
            } catch (Throwable e) {
                stepStatus.stepIndex--;
                stepStatuses.push(stepStatus);
                return exitJoin();
            }
        }
        stepStatuses.push(stepStatus);
        return null;
    }

    @Override
    public boolean back() {
        if (canGoBack()) {
            StepStatus stepStatus = stepStatuses.pop();
            if (steps.containsKey(stepStatus.sequenceId)) {
                if ((stepStatuses.size() > 0 && stepStatus.stepIndex > stepStatus.initialStep) || (stepStatuses.size() == 0 && stepStatus.stepIndex - persistedStatus.stepIndex > 0)) {
                    stepStatus.stepIndex--;
                    stepStatuses.push(stepStatus);
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public Class<? extends Activity> exitJump() {
        if (!stepStatuses.empty()) {
            return exitJumps.get(stepStatuses.peek().sequenceId);
        }
        return null;
    }

    @Override
    public Fragment resume() {
        StepStatus stepStatus = new StepStatus(persistedStatus.sequenceId, persistedStatus.stepIndex);
        if (stepStatuses.isEmpty()) {
            if (steps.containsKey(stepStatus.sequenceId)) {
                try {
                    Fragment fragment = steps.get(stepStatus.sequenceId)[stepStatus.stepIndex].newInstance();
                    stepStatuses.push(stepStatus);
                    return fragment;
                } catch (Throwable e) {
                }
            }
        }
        return null;
    }

    @Override
    public Fragment restart() {
        stepStatuses.clear();
        return resume();
    }

    private Fragment exitJoin(){
        if (!exitJoins.containsKey(currentSequenceId())) {
            return null;
        }
        Join join = exitJoins.get(currentSequenceId());
        if (join.stepIndex() < 0 || !steps.containsKey(join.id())) {
            return null;
        }
        StepStatus stepStatus = new StepStatus(join.id(), join.stepIndex());
        try {
            Fragment fragment = steps.get(stepStatus.sequenceId)[stepStatus.stepIndex].newInstance();
            stepStatuses.push(stepStatus);
            return fragment;
        } catch (Throwable e) {
            return null;
        }
    }

    private class StepStatus implements Serializable {
        String sequenceId;
        int stepIndex;
        int initialStep;

        StepStatus(String sequenceId, int stepIndex) {
            this.sequenceId = sequenceId;
            this.stepIndex = stepIndex;
            this.initialStep = 0;
        }
    }
}
