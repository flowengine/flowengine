package flowengine.core;

import android.content.Context;
import android.os.Bundle;
import flowengine.annotations.flow.methods.*;

public interface InnerFlow {
    @Resume @BackStack(true) void start();
    @Resume @BackStack(false) void resume();
    @Back void back();
    @Restart @BackStack(false) void restart();
    void pause();
    void saveInstanceState(Bundle bundle);
    void initSavedInstanceState(Bundle bundle);
    String getUuid();
    void setFlowEngineInitialContext(Context context);
    Context getFlowEngineInitialContext();
    @GetModel Object __getFlowEngineInnerFlowModel(String key);
    @SetModel void __setFlowEngineInnerFlowModel(Object o, String key);
}
