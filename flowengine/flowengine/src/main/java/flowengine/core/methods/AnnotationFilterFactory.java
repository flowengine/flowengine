package flowengine.core.methods;

import flowengine.annotations.callbacks.*;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.lang.reflect.Method;

public class AnnotationFilterFactory {

    public abstract class AnnotationFilter {
        public Method getMatch(List<Method> methods, String id) {
            Method possibleMethod = null;
            for (Method method : methods) {
                String value = getValue(method);
                possibleMethod = value.isEmpty() ? method : null;
                if (value.equals(id)){
                    return method;
                }
            }
            return possibleMethod;
        }

        protected abstract String getValue(Method method);
    }

    public static AnnotationFilterFactory getInstance() {
        if (instance == null)
            instance = new AnnotationFilterFactory();
        return instance;
    }

    public AnnotationFilter getFilter(Class<? extends Annotation> annotation) {
        if (methodMap.containsKey(annotation.getName())) {
            return methodMap.get(annotation.getName());
        }
        return null;
    }

    private static AnnotationFilterFactory instance = null;
    private Map<String, AnnotationFilter> methodMap;

    private AnnotationFilterFactory() {
        methodMap = new HashMap<>();
        methodMap.put(BeforeJump.class.getName(), new AnnotationFilter() {
            @Override
            protected String getValue(Method method) {
                return method.getAnnotation(BeforeJump.class).value();
            }
        });
        methodMap.put(BeforeTransition.class.getName(), new AnnotationFilter() {
            @Override
            protected String getValue(Method method) {
                return method.getAnnotation(BeforeTransition.class).value();
            }
        });
        methodMap.put(OnFlowFinish.class.getName(), new AnnotationFilter() {
            @Override
            protected String getValue(Method method) {
                return method.getAnnotation(OnFlowFinish.class).value();
            }
        });
        methodMap.put(Validate.class.getName(), new AnnotationFilter() {
            @Override
            protected String getValue(Method method) {
                return method.getAnnotation(Validate.class).value();
            }
        });
        methodMap.put(BeforeBack.class.getName(), new AnnotationFilter() {
            @Override
            protected String getValue(Method method) {
                return method.getAnnotation(BeforeBack.class).value();
            }
        });
        methodMap.put(BeforeExit.class.getName(), new AnnotationFilter() {
            @Override
            protected String getValue(Method method) {
                return method.getAnnotation(BeforeExit.class).value();
            }
        });
    }
}
