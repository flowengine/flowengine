package flowengine.core.methods;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import flowengine.Flow;
import flowengine.FlowEngine;
import flowengine.TransitionManager;
import flowengine.annotations.callbacks.BeforeExit;
import flowengine.annotations.flow.parameters.Argument;
import flowengine.core.parameters.BundleBuilder;
import flowengine.core.parameters.Parameter;

import java.util.List;

public class Exit extends Action {

    public Exit(java.lang.reflect.Method method, Flow flow, Class<? extends Flow> flowInterface, TransitionManager transitionManager, Object[] args) {
        super(method, flow, flowInterface, transitionManager, args);
    }

    @Override
    public void invoke() {
        FragmentActivity currentActivity = FlowEngine.currentActivity;
        FragmentManager fragmentManager = currentActivity.getSupportFragmentManager();
        Fragment currentFragment = getCurrentFragment(fragmentManager);
        callMethodAnnotatedWith(currentFragment, BeforeExit.class, transitionManager.currentSequenceId());
        callMethodAnnotatedWith(currentActivity, BeforeExit.class, transitionManager.currentSequenceId());
        Intent intent = new Intent();
        addExtras(intent);
        int resultCode = method.getAnnotation(flowengine.annotations.flow.methods.Exit.class).value();
        currentActivity.setResult(resultCode, intent);
        FlowEngine.finishFlow();
        currentActivity.finish();
    }

    private Fragment getCurrentFragment(FragmentManager fragmentManager) {
        return fragmentManager.findFragmentByTag(Event.TAG);
    }

    private void addExtras(Intent intent) {
        List<Parameter> arguments = getParametersWithAnnotation(Argument.class);
        Bundle bundle = BundleBuilder.getInstance().newInstance(arguments);
        intent.putExtras(bundle);
    }

}
