package flowengine.core.methods;

import android.support.v4.app.Fragment;
import flowengine.Flow;
import flowengine.FlowEngine;
import flowengine.TransitionManager;
import flowengine.annotations.callbacks.BeforeBack;
import flowengine.annotations.callbacks.BeforeTransition;

public class Back extends Event {
    private boolean canGoBack;
    public Back(java.lang.reflect.Method method, Flow flow, Class<? extends Flow> flowInterface, TransitionManager transitionManager, Object[] args) {
        super(method, flow, flowInterface, transitionManager, args);
    }

    @Override
    protected Fragment getStep() {
        canGoBack = transitionManager.back();
        return null;
    }

    @Override
    protected void onNullFragment() {
        super.onNullFragment();
        String eventFilter = transitionManager.currentSequenceId();
        callMethodAnnotatedWith(getCurrentFragment(), BeforeBack.class, eventFilter);
        callMethodAnnotatedWith(FlowEngine.currentActivity, BeforeBack.class, eventFilter);
        if (!canGoBack){
            FlowEngine.finishFlow();
        }
    }
}
