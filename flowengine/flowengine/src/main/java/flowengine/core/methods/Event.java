package flowengine.core.methods;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import flowengine.Flow;
import flowengine.FlowEngine;
import flowengine.TransitionManager;
import flowengine.annotations.flow.StepContainer;
import flowengine.annotations.flow.methods.BackStack;
import flowengine.annotations.flow.parameters.Argument;
import flowengine.core.parameters.BundleBuilder;
import flowengine.core.parameters.Parameter;

import java.util.List;

public abstract class Event extends Action {
    protected static final String TAG = "EventFragmentTag";
    protected boolean transitionStopped;

    public Event(java.lang.reflect.Method method, Flow flow, Class<? extends Flow> flowInterface, TransitionManager transitionManager, Object[] args) {
        super(method, flow, flowInterface, transitionManager, args);
    }

    @Override
    public void invoke() {
        transitionStopped = false;
        beforeTransition();
        if (transitionStopped) {
            return;
        }
        FragmentActivity activity = FlowEngine.currentActivity;
        Fragment fragment = getStep();
        if (fragment == null) {
            onNullFragment();
            return;
        }
        setParameters(fragment);
        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
        setCustomAnimation(ft);
        setTransition(ft);
        setBreadCrumbShortTitle(ft);
        ft.replace(getStepContainer(), fragment, TAG);
        BackStack backStack = getAnnotation(BackStack.class);
        if (backStack == null || backStack.value()) {
            ft.addToBackStack(null);
        } else {
            transitionManager.clearBackStack();
            clearBackStack(activity.getSupportFragmentManager());
        }
        ft.commit();
        afterTransition();
    }

    protected abstract Fragment getStep();

    protected void onNullFragment() {
        // do nothing
    }

    protected void afterTransition() {
        // do nothing
    }

    protected void beforeTransition() {
        // do nothing
    }

    protected Fragment getCurrentFragment() {
        FragmentActivity activity = FlowEngine.currentActivity;
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        return fragmentManager.findFragmentByTag(TAG);
    }

    private void setCustomAnimation(FragmentTransaction fragmentTransaction) {
        Animation animation = new Animation(getAnnotation(flowengine.annotations.flow.Animation.class));
        if (animation.enter > 0 && animation.exit > 0 && animation.popEnter > 0 && animation.popExit > 0) {
            fragmentTransaction.setCustomAnimations(animation.enter, animation.exit, animation.popEnter, animation.popExit);
        } else if (animation.enter > 0 && animation.exit > 0) {
            fragmentTransaction.setCustomAnimations(animation.enter, animation.exit);
        }
    }

    private void setTransition(FragmentTransaction fragmentTransaction) {
        Transition transition = new Transition(getAnnotation(flowengine.annotations.flow.Transition.class));
        if (transition.transition > 0) {
            fragmentTransaction.setTransition(transition.transition);
        }
        if (transition.transitionStyle > 0) {
            fragmentTransaction.setTransitionStyle(transition.transitionStyle);
        }
    }

    private void setBreadCrumbShortTitle(FragmentTransaction fragmentTransaction) {
        BreadCrumbShortTitle breadCrumbShortTitle = new BreadCrumbShortTitle(getAnnotation(flowengine.annotations.flow.BreadCrumbShortTitle.class));
        if (breadCrumbShortTitle.value > 0) {
            fragmentTransaction.setBreadCrumbShortTitle(breadCrumbShortTitle.value);
        }
    }

    private int getStepContainer() {
        StepContainer annotation = getAnnotation(StepContainer.class);
        return annotation.value();
    }

    private void clearBackStack(FragmentManager fm) {
        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }

    private class Animation {
        int enter = 0, exit = 0, popEnter = 0, popExit = 0;

        private Animation(flowengine.annotations.flow.Animation animation) {
            if (animation == null) return;
            enter = animation.enter();
            exit = animation.exit();
            popEnter = animation.popEnter();
            popExit = animation.popExit();
        }

    }

    private class Transition {
        int transition = 0, transitionStyle = 0;

        private Transition(flowengine.annotations.flow.Transition transition) {
            if (transition == null) return;
            this.transition = transition.transition();
            this.transitionStyle = transition.transitionStyle();
        }
    }

    private class BreadCrumbShortTitle {
        int value = 0;

        private BreadCrumbShortTitle(flowengine.annotations.flow.BreadCrumbShortTitle breadCrumbShortTitle) {
            if (breadCrumbShortTitle == null) return;
            this.value = breadCrumbShortTitle.value();
        }
    }

    private void setParameters(Fragment fragment){
        List<Parameter> arguments = getParametersWithAnnotation(Argument.class);
        Bundle bundle = BundleBuilder.getInstance().newInstance(arguments);
        fragment.setArguments(bundle);
    }
}
