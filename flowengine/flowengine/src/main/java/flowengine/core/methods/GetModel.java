package flowengine.core.methods;

import flowengine.Flow;
import flowengine.TransitionManager;
import flowengine.core.StorageHandler;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class GetModel<T> extends Function<T> {

    public GetModel(Flow flow, Method method, Object[] args, Class<? extends Flow> flowInterface, TransitionManager transitionManager, StorageHandler storageHandler) {
        super(flow, method, args, flowInterface, transitionManager);
        this.storageHandler = storageHandler;
    }

    private StorageHandler storageHandler;

    @Override
    public T invoke() {
        flowengine.annotations.flow.methods.GetModel annotation = this.method.getAnnotation(flowengine.annotations.flow.methods.GetModel.class);
        String key = annotation.value();
        if ((key == null || key.isEmpty()) && parameters.length >= 0 && parameters[0] instanceof String) {
            key = (String) parameters[0];
        }
        if (key == null || key.isEmpty()) {
            throw new RuntimeException("The GetModel annotation provided to " + method.getName() + " needs to be provided with a key value");
        }
        try {
            return storageHandler.getModel(key);
        } catch (RuntimeException e) {
            return null;
        }
    }
}
