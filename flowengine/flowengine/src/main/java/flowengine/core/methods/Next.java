package flowengine.core.methods;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import flowengine.Flow;
import flowengine.FlowEngine;
import flowengine.TransitionManager;
import flowengine.annotations.callbacks.OnFlowFinish;
import flowengine.annotations.callbacks.BeforeTransition;
import flowengine.annotations.callbacks.Validate;
import flowengine.annotations.flow.parameters.Argument;
import flowengine.core.parameters.BundleBuilder;
import flowengine.core.parameters.Parameter;

import java.util.List;

public class Next extends Event {
    String eventId;

    public Next(java.lang.reflect.Method method, Flow flow, Class<? extends Flow> flowInterface, TransitionManager transitionManager, Object[] args) {
        super(method, flow, flowInterface, transitionManager, args);
        this.eventId = method.getAnnotation(flowengine.annotations.flow.methods.Next.class).value();
    }

    @Override
    protected Fragment getStep() {
        return transitionManager.next(eventId);
    }

    @Override
    protected void beforeTransition() {
        super.beforeTransition();
        FragmentActivity activity = FlowEngine.currentActivity;
        Fragment currentFragment = getCurrentFragment();
        if (!validateCurrentStep(currentFragment)) {
            transitionStopped = true;
        } else {
            finishCurrentStep(activity, currentFragment);
        }
    }

    @Override
    protected void onNullFragment() {
        super.onNullFragment();
        String eventFilter = eventId.isEmpty() ? transitionManager.currentSequenceId() : eventId;
        callMethodAnnotatedWith(getCurrentFragment(), BeforeTransition.class, eventFilter);
        callMethodAnnotatedWith(FlowEngine.currentActivity, BeforeTransition.class, eventFilter);
        callMethodAnnotatedWith(getCurrentFragment(), OnFlowFinish.class, eventFilter);
        callMethodAnnotatedWith(FlowEngine.currentActivity, OnFlowFinish.class, eventFilter);
        jumpIfExists();
    }

    private void jumpIfExists() {
        Class<? extends Activity> activity = transitionManager.exitJump();
        if (activity != Activity.class) {
            Intent intent = new Intent(FlowEngine.currentActivity, activity);
            FlowEngine.currentActivity.startActivity(addExtras(intent));
        }
    }

    private boolean validateCurrentStep(Fragment currentFragment) {
        String eventFilter = eventId.isEmpty() ? transitionManager.currentSequenceId() : eventId;
        Boolean result = (Boolean) callMethodAnnotatedWith(currentFragment, Validate.class, eventFilter);
        return (result != null) ? result : true;
    }

    private void finishCurrentStep(Activity currentActivity, Fragment currentFragment) {
        String eventFilter = eventId.isEmpty() ? transitionManager.currentSequenceId() : eventId;
        callMethodAnnotatedWith(currentFragment, BeforeTransition.class, eventFilter);
        callMethodAnnotatedWith(currentActivity, BeforeTransition.class, eventFilter);
    }

    private Intent addExtras(Intent intent) {
        List<Parameter> arguments = getParametersWithAnnotation(Argument.class);
        Bundle bundle = BundleBuilder.getInstance().newInstance(arguments);
        intent.putExtras(bundle);
        return intent;
    }


}
