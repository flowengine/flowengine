package flowengine.core.methods;

import flowengine.Flow;
import flowengine.TransitionManager;
import flowengine.core.FlowEngineProcessor;
import flowengine.core.parameters.Parameter;
import flowengine.core.parameters.ParameterFactory;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public abstract class Action extends Invokeable {

    Action(java.lang.reflect.Method method, Flow flow, Class<? extends Flow> flowInterface,
           TransitionManager transitionManager, Object[] args) {
        super(flow, method, args, flowInterface, transitionManager);
    }

    public abstract void invoke();

}
