package flowengine.core.methods;

import flowengine.Flow;
import flowengine.TransitionManager;
import flowengine.core.StorageHandler;

import java.lang.reflect.Method;

/**
 * Created by Ignacio on 17/05/2015.
 */
public class SetModel<T> extends Action {

    private StorageHandler storageHandler;

    private T model;

    public SetModel(Method method, Flow flow, Class<? extends Flow> flowInterface, TransitionManager transitionManager, Object[] args, T model, StorageHandler storageHandler) {
        super(method, flow, flowInterface, transitionManager, args);
        this.model = model;
        this.storageHandler = storageHandler;
    }

    @Override
    public void invoke() {
        flowengine.annotations.flow.methods.SetModel annotation = this.method.getAnnotation(flowengine.annotations.flow.methods.SetModel.class);
        String key = annotation.value();
        if (key == null || key.isEmpty() && parameters.length > 1 && parameters[1] instanceof String) {
            key = (String) parameters[1];
        }
        if (key == null || key.isEmpty()) {
            throw new RuntimeException("The SetModel annotation provided to " + method.getName() + " needs to be provided with a key value");
        }
        storageHandler.setModel(key, this.model);
    }
}
