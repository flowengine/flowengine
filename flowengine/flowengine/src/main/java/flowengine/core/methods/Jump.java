package flowengine.core.methods;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import flowengine.Flow;
import flowengine.FlowEngine;
import flowengine.TransitionManager;
import flowengine.annotations.flow.methods.BackStack;
import flowengine.annotations.flow.methods.WaitForResult;
import flowengine.annotations.callbacks.BeforeJump;
import flowengine.annotations.flow.parameters.Argument;
import flowengine.annotations.flow.parameters.RequestCode;
import flowengine.core.parameters.BundleBuilder;
import flowengine.core.parameters.Parameter;

import java.util.List;

public class Jump extends Action {

    public Jump(java.lang.reflect.Method method, Flow flow, Class<? extends Flow> flowInterface, TransitionManager transitionManager, Object[] args) {
        super(method, flow, flowInterface, transitionManager, args);
    }

    @Override
    public void invoke() {
        FragmentActivity currentActivity = FlowEngine.currentActivity;
        FragmentManager fragmentManager = currentActivity.getSupportFragmentManager();
        Fragment currentFragment = getCurrentFragment(fragmentManager);
        callMethodAnnotatedWith(currentFragment, BeforeJump.class, transitionManager.currentSequenceId());
        callMethodAnnotatedWith(currentActivity, BeforeJump.class, transitionManager.currentSequenceId());
        Class<? extends Activity> activityClass = method.getAnnotation(flowengine.annotations.flow.methods.Jump.class).value();
        Intent intent = new Intent(currentActivity, activityClass);
        addExtras(intent);
        List<Parameter> requestCode = getParametersWithAnnotation(RequestCode.class);
        if (!requestCode.isEmpty()) {
            currentFragment.startActivityForResult(intent, (Integer) requestCode.get(0).getValue());
        } else if (method.isAnnotationPresent(WaitForResult.class)) {
            currentFragment.startActivityForResult(intent, method.getAnnotation(WaitForResult.class).value());
        } else {
            currentFragment.startActivity(intent);
        }
        BackStack backStack = getAnnotation(BackStack.class);
        if(backStack != null && !backStack.value()) {
            currentActivity.finish();
        }
    }

    private Fragment getCurrentFragment(FragmentManager fragmentManager) {
        return fragmentManager.findFragmentByTag(Event.TAG);
    }

    private void addExtras(Intent intent) {
        List<Parameter> arguments = getParametersWithAnnotation(Argument.class);
        Bundle bundle = BundleBuilder.getInstance().newInstance(arguments);
        intent.putExtras(bundle);
    }

}
