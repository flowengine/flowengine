package flowengine.core.methods;

import flowengine.Flow;
import flowengine.TransitionManager;
import flowengine.core.MethodsEnum;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;

public class ActionFactory {

    private interface ActionBuilder {
        Action buildEvent(java.lang.reflect.Method method, Flow flow, Class<? extends Flow> flowInterface, TransitionManager transitionManager, Object[] args);
    }

    public static ActionFactory getInstance() {
        if (instance == null)
            instance = new ActionFactory();
        return instance;
    }

    public Action getMethod(java.lang.reflect.Method interfaceMethod, Flow flow, Class<? extends Flow> flowInterface, TransitionManager transitionManager, Object[] args) {
        for (Annotation annotation : getDeclaredAnnotationsForMethod(interfaceMethod)) {
            String annotationMethodName = annotation.annotationType().getName();
            if (actionMap.containsKey(annotationMethodName)) {
                return actionMap.get(annotationMethodName).buildEvent(interfaceMethod, flow, flowInterface, transitionManager, args);
            }
        }
        return null;
    }

    //TODO: check in parent classes
    private Annotation[] getDeclaredAnnotationsForMethod(java.lang.reflect.Method interfaceMethod) {
        return interfaceMethod.getDeclaredAnnotations();
    }

    private static ActionFactory instance = null;

    private Map<String, ActionBuilder> actionMap;

    private ActionFactory() {
        actionMap = new HashMap<>();
        actionMap.put(MethodsEnum.EVENT_NEXT, new ActionBuilder() {
            public Action buildEvent(java.lang.reflect.Method method, Flow flow, Class<? extends Flow> flowInterface, TransitionManager transitionManager, Object[] args) {
                return new Next(method, flow, flowInterface, transitionManager, args);
            }
        });
        actionMap.put(MethodsEnum.EVENT_RESUME, new ActionBuilder() {
            public Action buildEvent(java.lang.reflect.Method method, Flow flow, Class<? extends Flow> flowInterface, TransitionManager transitionManager, Object[] args) {
                return new Resume(method, flow, flowInterface, transitionManager, args);
            }
        });
        actionMap.put(MethodsEnum.EVENT_RESTART, new ActionBuilder() {
            public Action buildEvent(java.lang.reflect.Method method, Flow flow, Class<? extends Flow> flowInterface, TransitionManager transitionManager, Object[] args) {
                return new Restart(method, flow, flowInterface, transitionManager, args);
            }
        });
        actionMap.put(MethodsEnum.EVENT_BACK, new ActionBuilder() {
            public Action buildEvent(java.lang.reflect.Method method, Flow flow, Class<? extends Flow> flowInterface, TransitionManager transitionManager, Object[] args) {
                return new Back(method, flow, flowInterface, transitionManager, args);
            }
        });
        actionMap.put(MethodsEnum.EVENT_JUMP, new ActionBuilder() {
            public Action buildEvent(java.lang.reflect.Method method, Flow flow, Class<? extends Flow> flowInterface, TransitionManager transitionManager, Object[] args) {
                return new Jump(method, flow, flowInterface, transitionManager, args);
            }
        });
        actionMap.put(MethodsEnum.EVENT_EXIT, new ActionBuilder() {
            public Action buildEvent(java.lang.reflect.Method method, Flow flow, Class<? extends Flow> flowInterface, TransitionManager transitionManager, Object[] args) {
                return new Exit(method, flow, flowInterface, transitionManager, args);
            }
        });
    }
}
