package flowengine.core.methods;

import android.support.v4.app.Fragment;
import flowengine.Flow;
import flowengine.TransitionManager;

public class Restart extends Event {
    public Restart(java.lang.reflect.Method method, Flow flow, Class<? extends Flow> flowInterface, TransitionManager transitionManager, Object[] args) {
        super(method, flow, flowInterface, transitionManager, args);
    }

    @Override
    protected Fragment getStep() {
        return transitionManager.restart();
    }
}
