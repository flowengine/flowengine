package flowengine.core.methods;

import flowengine.Flow;
import flowengine.TransitionManager;

import java.lang.reflect.Method;

/**
 * Created by Ignacio on 17/05/2015.
 */
public abstract class Function<T> extends Invokeable {
    public Function(Flow flow, Method method, Object[] args, Class<? extends Flow> flowInterface, TransitionManager transitionManager) {
        super(flow, method, args, flowInterface, transitionManager);
    }
    public abstract T invoke();
}
