package flowengine.core.methods;

import flowengine.Flow;
import flowengine.TransitionManager;
import flowengine.core.FlowEngineProcessor;
import flowengine.core.parameters.Parameter;
import flowengine.core.parameters.ParameterFactory;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Ignacio on 17/05/2015.
 */
public class Invokeable {
    protected java.lang.reflect.Method method;
    protected Flow flow;
    protected Class<? extends Flow> flowInterface;
    protected TransitionManager transitionManager;
    protected Object[] parameters;

    public Invokeable(Flow flow, java.lang.reflect.Method method, Object[] args, Class<? extends Flow> flowInterface, TransitionManager transitionManager) {
        this.flow = flow;
        this.method = method;
        this.parameters = args;
        this.flowInterface = flowInterface;
        this.transitionManager = transitionManager;
    }

    protected <T extends Annotation> T getAnnotation(Class<T> annotationClass) {
        if (this.method.isAnnotationPresent(annotationClass))
            return method.getAnnotation(annotationClass);
        return flowInterface.getAnnotation(annotationClass);
    }

    protected Object callMethodAnnotatedWith(Object obj, Class<? extends Annotation> annotation, String filter) {
        if (obj == null) {
            return null;
        }
        Class clazz = obj.getClass();
        java.lang.reflect.Method method = getMethodAnnotatedWith(clazz, annotation, filter);
        if (method != null) {
            try {
                return method.invoke(obj);
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private java.lang.reflect.Method getMethodAnnotatedWith(final Class<?> type, final Class<? extends Annotation> annotation, String filter) {
        Class<?> cls = type;
        String clsName = cls.getName();
        AnnotationFilterFactory.AnnotationFilter annotationFilter = AnnotationFilterFactory.getInstance().getFilter(annotation);
        while (!clsName.startsWith(FlowEngineProcessor.ANDROID_PREFIX) && !clsName.startsWith(FlowEngineProcessor.JAVA_PREFIX)) { // need to iterated thought hierarchy in order to retrieve methods from above the current instance
            final List<java.lang.reflect.Method> allMethods = new ArrayList<>(Arrays.asList(cls.getDeclaredMethods()));
            List<java.lang.reflect.Method> possibleMethods = new LinkedList<>();
            for (final java.lang.reflect.Method method : allMethods) {
                if (annotation == null || method.isAnnotationPresent(annotation)) {
                    possibleMethods.add(method);
                }
            }
            java.lang.reflect.Method method = annotationFilter.getMatch(possibleMethods, filter);
            if (method != null) {
                return method;
            }
            cls = cls.getSuperclass();
            clsName = cls.getName();
        }
        return null;
    }

    public List<Parameter> getParametersWithAnnotation(Class<? extends Annotation> annotationType){

        Annotation[][] parameterAnnotations = method.getParameterAnnotations();
        Class[] parameterTypes = method.getParameterTypes();
        List<Parameter> parameters = new ArrayList<>();

        int i=0;
        for(Annotation[] annotations : parameterAnnotations){
            Object parameterValue = this.parameters[i];
            Class parameterType = parameterTypes[i++];

            for(Annotation annotation : annotations){
                if(annotation.annotationType().equals(annotationType)){
                    parameters.add(ParameterFactory.getInstance().getParameter(annotation, parameterValue,parameterType));
                }
            }
        }
        return parameters;
    }
}
