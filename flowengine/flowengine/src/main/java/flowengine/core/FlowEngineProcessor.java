package flowengine.core;

import flowengine.core.processors.PersistableModelBuilderProcessor;
import flowengine.core.processors.InjectorProcessor;
import flowengine.core.processors.Processor;
import flowengine.core.processors.ValidatorProcessor;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.*;

import java.util.*;

public final class FlowEngineProcessor extends AbstractProcessor {

    public static final String SUFFIX = "$$FlowEngineInjector";
    public static final String ANDROID_PREFIX = "android.";
    public static final String JAVA_PREFIX = "java.";
    public static final String FLOWENGINE_PREFIX = "flowengine.";
    public static final String FLOWENGINE_SEPARATOR = "_separator_";

    private List<Processor> processors;

    @Override public synchronized void init(ProcessingEnvironment env) {
        super.init(env);
        processors = new ArrayList<>(3);
        processors.add(new InjectorProcessor(env));
        processors.add(new ValidatorProcessor(env));
        processors.add(new PersistableModelBuilderProcessor(env));
    }

    @Override public Set<String> getSupportedAnnotationTypes() {
        Set<String> supportTypes = new LinkedHashSet<>();

        for (Processor processor: processors) {
            supportTypes.addAll(processor.getSupportedAnnotationTypes());
        }

        return supportTypes;
    }

    @Override public boolean process(Set<? extends TypeElement> elements, RoundEnvironment env){

        for (Processor processor: processors) {
            if (!processor.process(elements, env))
                return false;
        }
        return true;
    }

    @Override public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }

}
