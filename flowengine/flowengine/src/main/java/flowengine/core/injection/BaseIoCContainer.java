package flowengine.core.injection;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ignacio on 11/07/2015.
 */
public class BaseIoCContainer implements IoCContainer {

    private static BaseIoCContainer instance;

    private Map<Class<?>, Object> registeredSingletons;

    private IoCInjector injector;

    public BaseIoCContainer() {
        registeredSingletons = new HashMap<>();
        injector = new BaseIoCInjector();
    }

    public static BaseIoCContainer getInstance() {
        if (instance == null) {
            instance = new BaseIoCContainer();
        }
        return instance;
    }

    public void registerSingleton(Class<?> representingInterface, Object object) {
        if (registeredSingletons.containsKey(representingInterface))
            throw new RuntimeException("Cannot register " + object.getClass().toString() + " as implementor of " + representingInterface.toString() + " because there is already an implementor for such interface");
        if (!implementsInterface(representingInterface, object))
            throw new RuntimeException("Object of class " + object.getClass().toString() + " cannot be registered as an implementor for " + representingInterface + " interface");
        registeredSingletons.put(representingInterface, object);
    }

    private boolean implementsInterface(Class<?> checkInterface, Object object) {
        for (Class<?> currentInterface : object.getClass().getInterfaces()) {
            if (currentInterface == checkInterface) {
                return true;
            }
        }
        return false;
    }

    public <T> T getSingleton(Class<T> representingInterface) {
        if (!registeredSingletons.containsKey(representingInterface))
            throw new RuntimeException("Component  " + representingInterface.toString() + " has no registered implementor");
        return (T) registeredSingletons.get(representingInterface);
    }

    public boolean hasImplementor(Field field) {
        return registeredSingletons.containsKey(field.getType());
    }

    @Override
    public Collection<Object> getAllSingletons() {
        return registeredSingletons.values();
    }

    @Override
    public void resolveInjectedSingletonDependencies() {
        injector.resolveInjectedSingletonDependencies(this);
    }

    @Override
    public void injectSingletons(Object target) {
        injector.injectSingletons(target, this);
    }

}