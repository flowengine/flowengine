package flowengine.core.injection;

import java.lang.reflect.Field;
import java.util.Collection;

/**
 * Created by Ignacio on 11/07/2015.
 */
public interface IoCContainer {

    void registerSingleton(Class<?> representingInterface, Object object);

    <T> T getSingleton(Class<T> representingInterface);

    boolean hasImplementor(Field field);

    Collection<Object> getAllSingletons();

    void resolveInjectedSingletonDependencies();

    void injectSingletons(Object target);
}
