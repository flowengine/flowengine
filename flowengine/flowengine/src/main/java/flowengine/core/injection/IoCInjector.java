package flowengine.core.injection;

import flowengine.core.injection.IoCContainer;

/**
 * Created by Ignacio on 11/07/2015.
 */
public interface IoCInjector {

    public void injectSingletons(Object target, IoCContainer container);
    public void resolveInjectedSingletonDependencies(IoCContainer container);
}
