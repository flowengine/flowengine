package flowengine.core.parameters;

/**
 * Created by florencia on 26/04/15.
 */
public final class ParametersEnum {
    public final static String PARAMETER_ARGUMENT = flowengine.annotations.flow.parameters.Argument.class.getName();
    public final static String PARAMETER_REQUEST_CODE = flowengine.annotations.flow.parameters.RequestCode.class.getName();

}
