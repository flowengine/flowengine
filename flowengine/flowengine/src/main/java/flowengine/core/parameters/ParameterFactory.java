package flowengine.core.parameters;

import flowengine.annotations.flow.parameters.Argument;
import flowengine.annotations.flow.parameters.RequestCode;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by florencia on 25/04/15.
 */
public class ParameterFactory {

    private interface ParameterBuilder {
        Parameter buildParameter(Annotation annotation, Object value, Class<?> type);
    }

    public static ParameterFactory getInstance() {
        if (instance == null)
            instance = new ParameterFactory();
        return instance;
    }

    public Parameter getParameter(Annotation annotation, Object value, Class type) {
        String annotationParameterName = annotation.annotationType().getName();
        if (parameterMap.containsKey(annotationParameterName)) {
            return parameterMap.get(annotationParameterName).buildParameter(annotation, value, type);
        }
        return null;
    }

    private static ParameterFactory instance = null;

    private Map<String, ParameterBuilder> parameterMap;

    private ParameterFactory() {
        parameterMap = new HashMap<>();
        parameterMap.put(ParametersEnum.PARAMETER_ARGUMENT, new ParameterBuilder() {
            public Parameter buildParameter(Annotation annotation, Object value, Class<?> type) {
                return new NamedParameter(((Argument)annotation).value(), value, type);
            }
        });
        parameterMap.put(ParametersEnum.PARAMETER_REQUEST_CODE, new ParameterBuilder() {
            public Parameter buildParameter(Annotation annotation, Object value, Class<?> type) {
                return new Parameter(value, type);
            }
        });
    }

}
