package flowengine.core.parameters;

public class NamedParameter extends Parameter{
    String name;

    public NamedParameter(String name, Object value, Class type) {
        super(value, type);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NamedParameter parameter = (NamedParameter) o;

        if (name != null ? !name.equals(parameter.name) : parameter.name != null) return false;
        if (value != null ? !value.equals(parameter.value) : parameter.value != null) return false;
        return !(type != null ? !type.equals(parameter.type) : parameter.type != null);

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}
