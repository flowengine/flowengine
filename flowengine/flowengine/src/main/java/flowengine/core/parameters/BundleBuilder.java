package flowengine.core.parameters;

import android.os.Bundle;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

/**
 * Created by florencia on 25/04/15.
 */
public class BundleBuilder {

    private static BundleBuilder instance = null;

    public static BundleBuilder getInstance(){
        if (instance == null)
            instance = new BundleBuilder();
        return instance;
    }

    public Bundle newInstance(List<Parameter> parameters){
        Bundle bundle = new Bundle();
        for(Parameter parameter : parameters){
            if(parameter.value instanceof Serializable)
                bundle.putSerializable(((NamedParameter) parameter).name, (Serializable) parameter.value);
            if(parameter.value instanceof Parcelable)
                bundle.putParcelable(((NamedParameter) parameter).name, (Parcelable) parameter.value);
        }
        return bundle;
    }

}



