package flowengine.core;

import java.util.Map;

public interface StorageHandler {
    <T> T getModel(String key);
    <T> void setModel(String key, T value);
    Map<String, ? extends Object> getModels();
}
