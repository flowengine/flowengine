package flowengine.core;

/**
 * Created by Ignacio on 18/04/2015.
 */
public final class MethodsEnum {
    public final static String EVENT_RESUME = flowengine.annotations.flow.methods.Resume.class.getName();
    public final static String EVENT_RESTART = flowengine.annotations.flow.methods.Restart.class.getName();
    public final static String EVENT_BACK = flowengine.annotations.flow.methods.Back.class.getName();
    public final static String EVENT_NEXT = flowengine.annotations.flow.methods.Next.class.getName();
    public final static String EVENT_JUMP = flowengine.annotations.flow.methods.Jump.class.getName();
    public final static String EVENT_EXIT = flowengine.annotations.flow.methods.Exit.class.getName();
}
