package flowengine;

public interface Step {
    void onBeforeValidate();
    boolean validate();
    void onFinish();
}
