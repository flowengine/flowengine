package flowengine;

import flowengine.core.webservice.CancelRetryEvent;
import flowengine.core.webservice.RetryEvent;
import flowengine.core.webservice.WebError;

import java.util.UUID;

public class WebRetryableFailureEvent extends WebEvent<WebError>{
    private UUID uuid;

    public WebRetryableFailureEvent() {
        this.uuid = uuid;
    }

    void  setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public void retry() {
        FlowEngine.getCurrentBus().post(new RetryEvent(this.uuid));
    }

    public void cancel() {
        FlowEngine.getCurrentBus().post(new CancelRetryEvent(this.uuid));
    }
}
