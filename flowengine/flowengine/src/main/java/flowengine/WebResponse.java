package flowengine;

import flowengine.core.webservice.RepositoryEvent;
import flowengine.core.webservice.WebError;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.List;
import java.util.UUID;

public final class WebResponse<T> implements retrofit.Callback<T> {
    private WebSuccessEvent<T> successEvent;
    private WebEvent<WebError> failureEvent;
    private Class returnType;
    private Object originalResult;
    private UUID uuid;

    public WebResponse(){
        this(null, null);
    }

    public WebResponse(WebSuccessEvent<T> successEvent){
        this(successEvent, null);
    }

    public WebResponse(WebEvent<WebError> failureEvent){
        this(null, failureEvent);
    }

    public WebResponse(WebSuccessEvent<T> successEvent, WebEvent<WebError>failureEvent){
        setSuccessEvent(successEvent);
        setFailureEvent(failureEvent);
        this.uuid = UUID.randomUUID();
    }

    @Override
    final public void success(T t, Response response) {

        postEventWithResult(t, response);

        if(t != null)
            postEventToUpdateLocalData(t,t.getClass());
    }

    final public void postEventWithResult(T t, Response response){
        Object event = t;
        if (successEvent != null) {
            successEvent.set(t);
            successEvent.setResponse(new flowengine.core.webservice.Response(response));
            event = successEvent;
        }
        if (event instanceof List && event.getClass() != returnType) {
            List typedList;
            try {
                typedList = (List) returnType.newInstance();
                List list = (List) event;
                for (Object o : list) {
                    typedList.add(o);
                }
                event = typedList;
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
        if(event != null)
            postEvent(event);
    }


    @Override
    final public void failure(RetrofitError retrofitError) {
        WebError webError = new WebError(retrofitError);
        Object event = webError;
        if (failureEvent != null) {
            failureEvent.set(webError);
            if (failureEvent instanceof WebRetryableFailureEvent)
                ((WebRetryableFailureEvent) failureEvent).setUuid(uuid);
            event = failureEvent;
        }
        postEvent(event);
    }

    final public UUID getUuid() {
        return uuid;
    }

    private void postEvent(Object event) {
        FlowEngine.getCurrentBus().post(event);
    }

    private void postEventToUpdateLocalData(Object element, Class elementClass){
        RepositoryEvent event = new RepositoryEvent(element, originalResult, elementClass);
        postEvent(event);
    }

    private void setSuccessEvent(WebSuccessEvent<T> successEvent) {
        this.successEvent = successEvent;
    }

    private void setFailureEvent(WebEvent<WebError> failureEvent) {
        this.failureEvent = failureEvent;
    }

    public boolean isRetryable() {
        return this.failureEvent != null && this.failureEvent instanceof WebRetryableFailureEvent;
    }

    public void setReturnType(Class returnType) {
        this.returnType = returnType;
    }

    public void setOriginalResult(Object o) {
        this.originalResult = o;
    }

}
