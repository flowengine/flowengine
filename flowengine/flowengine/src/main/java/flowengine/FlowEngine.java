package flowengine;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import de.greenrobot.event.EventBus;

import flowengine.core.FlowEngineProcessor;
import flowengine.core.FlowHandler;
import flowengine.core.InnerFlow;
import flowengine.core.injection.BaseIoCContainer;
import flowengine.core.processors.PersistableModelBuilder;
import flowengine.core.utils.AnnotationSearcher;
import flowengine.core.webservice.WebServiceAdapter;

import java.lang.reflect.Proxy;
import java.util.*;

@SuppressWarnings("unused")
public class FlowEngine {

    static Stack<Flow> flowStack = new Stack<>();
    public static FragmentActivity currentActivity = null;

    static final Map<Class<?>, Injector> INJECTORS = new LinkedHashMap<>();

    static final Map<Class<?>, Object> CREATED_INSTANCES = new HashMap<>();

    static final Injector NOP_INJECTOR = new Injector() {
        public void injectServices(Object target) { }
        public void injectFlow(Object target) { }
        public void injectModels(Object target, Bundle savedInstanceState) { }
        public void injectPersistable(Object target) { }
    };

    private static Map<String, WebServiceAdapter> registeredAdapters = new HashMap<>();

    public static flowengine.EventBus getCurrentBus() {
        return new flowengine.EventBus(EventBus.getDefault());
    }

    public static void initApp(AppConfigurator configurator) {
        configureWebAdapters(configurator);
        configureWebServices(configurator);
        configureSingletons(configurator);
    }

    public static void init(Activity target, Class<? extends Flow> flowInterface) {
        try {
            Injector injector = findInjectorForClass(target.getClass());
            if (injector != null) {
                flowStack.push(newInstanceFlow(target.getClass().getClassLoader(), flowInterface, target));
                injector.injectFlow(target);
            }
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException("Unable to inject flow for " + target.getClass().getName(), e);
        }
    }

    public static void init(Activity target, Bundle savedInstanceState) {

        FlowEngine.getCurrentBus().register(target);
        Class<?> targetClass = target.getClass();
        FragmentActivity previousActivity = currentActivity;
        currentActivity = (FragmentActivity) target;
        loadPersistableModels();

        try {
            Injector injector = findInjectorForClass(targetClass);
            if (injector != null) {
                initFlow(target.getClass(), savedInstanceState, target);
                injector.injectServices(target);
                injector.injectFlow(target);
                injector.injectModels(target, savedInstanceState);
                injector.injectPersistable(target);
                BaseIoCContainer.getInstance().injectSingletons(target);
            }
        } catch (RuntimeException e) {
            currentActivity = previousActivity;
            throw e;
        } catch (Exception e) {
            currentActivity = previousActivity;
            throw new RuntimeException("Unable to inject services and models for " + target.getClass().getName(), e);
        }
    }

    public static void init(Fragment target, Bundle savedInstanceState) {
        FlowEngine.getCurrentBus().register(target);
        Class<?> targetClass = target.getClass();
        try {
            Injector injector = findInjectorForClass(targetClass);
            if (injector != null) {
                injector.injectServices(target);
                injector.injectFlow(target);
                injector.injectModels(target, savedInstanceState);
                injector.injectPersistable(target);
                BaseIoCContainer.getInstance().injectSingletons(target);
            }
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException("Unable to inject services and models for " + target.getClass().getName(), e);
        }
    }

    public static void start(Activity activity) {
        if (!FlowEngine.getCurrentBus().isRegistered(activity)) {
            FlowEngine.getCurrentBus().register(activity);
        }
    }

    public static void resume(Activity activity) {
        InnerFlow innerFlow = getInnerFlow();
        if(activity != null) {
            currentActivity = (FragmentActivity) activity;
            innerFlow.setFlowEngineInitialContext(currentActivity);
        }
        if (innerFlow != null)
            innerFlow.resume();
    }

    public static void saveInstanceState(Activity activity, Bundle bundle) {
        if (!flowStack.empty()) {
            for (Flow flow : flowStack) {
                InnerFlow innerFlow = (InnerFlow) flow;
                if (activity.equals(innerFlow.getFlowEngineInitialContext())) {
                    innerFlow.saveInstanceState(bundle);
                    innerFlow.setFlowEngineInitialContext(null);
                    break;
                }
            }
        }
    }

    public static void pause(Activity activity) {
        InnerFlow innerFlow = getInnerFlow();
        if (innerFlow != null) {
            innerFlow.pause();
        }
    }

    public static void stop(Activity activity) {
        if (FlowEngine.getCurrentBus().isRegistered(activity)) {
            FlowEngine.getCurrentBus().unregister(activity);
        }
    }

    public static void back(Activity activity) {
        InnerFlow innerFlow = getInnerFlow();
        if (innerFlow != null) {
            innerFlow.back();
        }
    }

    public static void createViews(Fragment fragment) {
        if (!FlowEngine.getCurrentBus().isRegistered(fragment)) {
            FlowEngine.getCurrentBus().register(fragment);
        }
    }

    public static void destroyViews(Fragment fragment) {
        if (FlowEngine.getCurrentBus().isRegistered(fragment)) {
            FlowEngine.getCurrentBus().unregister(fragment);
        }
    }

    public static void restart() {
        InnerFlow innerFlow = getInnerFlow();
        if (innerFlow != null) {
            innerFlow.restart();
        }
    }

    public static void finishFlow() {
        if (!flowStack.empty()) {
            flowStack.pop();
        }
    }

    public static WebServiceAdapter getAdapterByName(String name) {
        if (!registeredAdapters.containsKey(name)){
            throw new RuntimeException("There is no web adapter registered as " + name);
        }
        return registeredAdapters.get(name);
    }

    private static void configureSingletons(AppConfigurator configurator) {
        configurator.registerSingletons();
        for (Map.Entry<Class<?>, Object> pair : configurator.getRegisteredSingletons().entrySet()) {
            BaseIoCContainer.getInstance().registerSingleton(pair.getKey(), pair.getValue());
        }
        BaseIoCContainer.getInstance().resolveInjectedSingletonDependencies();
    }

    private static void configureWebAdapters(AppConfigurator configurator) {
        configurator.createWebAdapters();
        Map<String, WebServiceAdapter> registeredWs = configurator.getRegisteredWebAdapters();
        for (Map.Entry<String, WebServiceAdapter> context : registeredWs.entrySet()) {
            registeredAdapters.put(context.getKey(), context.getValue());
        }
    }

    private static void configureWebServices(AppConfigurator configurator) {
        configurator.createWebServices();
        Map<String, List<Class<?>>> registeredWs = configurator.getRegisteredWebServices();
        for (Map.Entry<String, List<Class<?>>> context : registeredWs.entrySet()) {
            for (Class<?> wsClass : context.getValue()) {
                Object ws = buildWebServiceFromAdapter(context.getKey(), wsClass);
                BaseIoCContainer.getInstance().registerSingleton(wsClass, ws);
            }
        }
    }

    private static Object buildWebServiceFromAdapter(String adapterKey, Class<?> ws) {
        WebServiceAdapter adapter = null;
        if (registeredAdapters.containsKey(adapterKey))
            adapter = registeredAdapters.get(adapterKey);
        if (adapter == null) {
            throw new RuntimeException("There is no adapter registered for " + adapterKey + ". Check registered adapters.");
        }
        return adapter.create(ws);
    }

    private static Injector findInjectorForClass(Class<?> cls)
            throws IllegalAccessException, InstantiationException {
        Injector injector = INJECTORS.get(cls);
        if (injector != null) {
            return injector;
        }
        String clsName = cls.getName();
        if (clsName.startsWith(FlowEngineProcessor.ANDROID_PREFIX) || clsName.startsWith(FlowEngineProcessor.JAVA_PREFIX)) {
            return NOP_INJECTOR;
        }
        try {
            Class<?> injectorClass = Class.forName(clsName + FlowEngineProcessor.SUFFIX);
            injector = (Injector) injectorClass.newInstance();
        } catch (ClassNotFoundException e) {
            injector = findInjectorForClass(cls.getSuperclass());
        }
        INJECTORS.put(cls, injector);
        return injector;
    }

    public interface Injector {
        void injectServices(Object o);
        void injectFlow(Object o);
        void injectModels(Object o, Bundle savedInstanceState);
        void injectPersistable(Object o);
    }

    public interface Builder {
        void build(Object o);
    }

    public static Flow getFlow() {
        if (flowStack.empty()) return null;
        return flowStack.peek();
    }

    static InnerFlow getInnerFlow() {
        if (flowStack.empty()) return null;
        return (InnerFlow) flowStack.peek();
    }

    private static void initFlow(Class clazz, Bundle savedInstanceState, Context context) {
        Class<? extends Flow> flowInterface = AnnotationSearcher.findAFlowForClass(clazz);
        Flow flow = newInstanceFlow(clazz.getClassLoader(), flowInterface, context);
        String uuid = null;
        if (savedInstanceState != null && savedInstanceState.containsKey(FlowEngineProcessor.FLOWENGINE_PREFIX + "UUID")) {
            uuid = savedInstanceState.getString(FlowEngineProcessor.FLOWENGINE_PREFIX + "UUID");
        }
        if (flowStack.empty() || !getInnerFlow().getUuid().equals(uuid)) {
            flowStack.push(flow);
        } else {
            flow = flowStack.peek();
        }
        ((InnerFlow) flow).setFlowEngineInitialContext(context);
        ((InnerFlow) flow).initSavedInstanceState(savedInstanceState);
    }

    private static Flow newInstanceFlow(ClassLoader classLoader, Class<? extends Flow> flowInterface, Context context) {
        return (Flow) Proxy.newProxyInstance(
                classLoader,
                new Class[]{flowInterface, InnerFlow.class},
                new FlowHandler(flowInterface, UUID.randomUUID().toString(), context)
        );
    }

    private static void loadPersistableModels(){
        try {
            Class<?> builderClass = Class.forName(PersistableModelBuilder.getFqcn());
            Builder builder = (Builder) builderClass.newInstance();
            builder.build(currentActivity);
        } catch (Exception ex){
            // There is not a persistable model builder
        }
    }
}
