package flowengine;

import flowengine.core.utils.MethodSearcher;

/**
 * Created by Ignacio on 09/06/2015.
 */
public class EventBus {

    private de.greenrobot.event.EventBus innerBus;

    EventBus(de.greenrobot.event.EventBus bus) {
        innerBus = bus;
    }

    public void registerSticky(Object listener) {
        innerBus.registerSticky(listener);
    }

    public boolean isRegistered(Object subscriber) {
        return innerBus.isRegistered(subscriber);
    }

    public void cancelEventDelivery(Object event) {
        innerBus.cancelEventDelivery(event);
    }

    public <T> T getStickyEvent(Class<T> eventType) {
        return innerBus.getStickyEvent(eventType);
    }

    public <T> T removeStickyEvent(Class<T> eventType) {
        return innerBus.removeStickyEvent(eventType);
    }

    public boolean removeStickyEvent(Object event) {
        return innerBus.removeStickyEvent(event);
    }

    public void removeAllStickyEvents() {
        innerBus.removeAllStickyEvents();
    }

    public void registerSticky(Object listener, int priority) {
        innerBus.registerSticky(listener, priority);
    }

    public boolean hasSubscriberForEvent(Class<?> eventClass) {
        return innerBus.hasSubscriberForEvent(eventClass);
    }

    public void postSticky(Object listener) {
        innerBus.postSticky(listener);
    }

    public void register(Object listener) {
        register(listener, 0);
    }

    public void register(Object listener, int priority) {
        if (listener != null && MethodSearcher.hasEventMethod(listener.getClass()))
            innerBus.register(listener, priority);
    }

    public void unregister(Object listener) {
        innerBus.unregister(listener);
    }

    public void post(Object event) {
        innerBus.post(event);
    }
}
