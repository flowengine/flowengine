package flowengine.annotations.flow.methods;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Next {
    String value() default "";
}
