package flowengine.annotations.flow;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface StepContainer {
    /*
    * @StepContainer(R.id.fragmentContainer);
    */
    int value();
}
