package flowengine.annotations.flow;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface BreadCrumbShortTitle {
    int value() default 0;
}
