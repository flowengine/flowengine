package flowengine.annotations.flow;

import android.app.Activity;
import android.support.v4.app.Fragment;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Sequence {
    String id();
    Class<? extends Fragment>[] steps();
    Class<? extends Activity> exitJump() default Activity.class;
    Join exitJoin() default @Join(id = "", stepIndex = -1);
}
