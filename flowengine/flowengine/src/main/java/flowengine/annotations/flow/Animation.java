package flowengine.annotations.flow;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface Animation {
    int enter() default 0;
    int exit() default 0;
    int popEnter() default 0;
    int popExit() default 0;
}
