package flowengine.annotations.flow;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface FlowSteps {
    boolean persistable() default false;
    Sequence[] sequences() default {};
    String mainSequenceId() default "";
}
