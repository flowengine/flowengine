package flowengine.annotations.flow.parameters;

import java.lang.annotation.*;

/**
 * Created by florencia on 25/04/15.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface Argument {
    String value() default "";
}
