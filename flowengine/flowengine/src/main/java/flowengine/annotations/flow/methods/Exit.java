package flowengine.annotations.flow.methods;

import android.app.Activity;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Exit {
    int value() default Activity.RESULT_OK;
}
