package flowengine.annotations.flow.methods;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface GetModel {
    /*
    * @GetModel("objId")
    * Object getMyObject();
    */
    String value() default "";
}
