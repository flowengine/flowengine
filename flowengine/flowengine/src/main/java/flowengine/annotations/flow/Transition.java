package flowengine.annotations.flow;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface Transition {
    int transition() default 0;
    int transitionStyle() default 0;
}
