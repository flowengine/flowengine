package flowengine.annotations.flow.methods;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface BackStack {
    boolean value() default true;
}
