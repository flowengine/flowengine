package flowengine.annotations.flow.methods;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.PARAMETER})
public @interface WaitForResult {
    int value() default 0;
}
