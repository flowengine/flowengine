package flowengine.annotations.callbacks;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface BeforeTransition {
    String value() default "";
}
