package flowengine.annotations.webservice;

import java.lang.annotation.*;

/**
 * Created by florencia on 01/08/15.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface ID {
}
