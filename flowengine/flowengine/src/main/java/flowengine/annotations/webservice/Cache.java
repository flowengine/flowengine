package flowengine.annotations.webservice;

import java.lang.annotation.*;

/**
 * Created by florencia on 29/07/15.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Cache {
    boolean forceUpdate() default false;
}
