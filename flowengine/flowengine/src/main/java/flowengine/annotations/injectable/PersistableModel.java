package flowengine.annotations.injectable;

import java.lang.annotation.*;

/**
 * Created by florencia on 08/08/15.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface PersistableModel {
}
