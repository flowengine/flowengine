package flowengine.annotations.injectable;

import java.lang.annotation.*;

/**
 * Injects a {@link flowengine.core.persistence.BasicDao}. Use it on fields of class
 * {@link flowengine.core.persistence.Dao}(for file persistence type), {@link flowengine.core.persistence.SQLDao}
 * (for database or memory persistence type) or {@link flowengine.core.persistence.BasicDao}. If the class of the field
 * does not correspond to any of the previous classes, an exception will be thrown.
 *
 * <p>
 * Example:
 * </p>
 *
 * <pre>
 *     public class MyActivity extends Activity {
 *
 *          &#064;Persistable
 * 	   SQLDao&lt;User,Long&gt; userDao;
 *
 *     }
 * </pre>
 *
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Persistable {
}
