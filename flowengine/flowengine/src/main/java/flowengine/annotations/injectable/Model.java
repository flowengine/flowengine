package flowengine.annotations.injectable;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Model {
    /*
    * @Model("optional")
    * MyObject obj;
    */
    String value();
}
