package flowengine;

import android.app.Activity;
import android.support.v4.app.Fragment;

import java.io.Serializable;
import java.util.Stack;

public interface TransitionManager {
    Fragment next(String eventId);
    boolean back();
    Fragment resume();
    Fragment restart();
    Class<? extends Activity> exitJump();
    boolean canGoBack();
    String currentSequenceId();
    void deserialize(Serializable serializable);
    Serializable serialize();
    void clearBackStack();
}
