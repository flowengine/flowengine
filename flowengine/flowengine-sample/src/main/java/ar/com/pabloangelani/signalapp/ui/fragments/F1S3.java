package ar.com.pabloangelani.signalapp.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import ar.com.pabloangelani.signalapp.R;
import ar.com.pabloangelani.signalapp.api.TestService;
import ar.com.pabloangelani.signalapp.model.User;
import ar.com.pabloangelani.signalapp.ui.adapter.TestAdapter;
import flowengine.WebFailureEvent;
import flowengine.WebResponse;
import flowengine.WebSuccessEvent;
import flowengine.core.injection.BaseIoCContainer;

public class F1S3 extends BaseStepFragment {

    public F1S3() { TAG = "F1S3"; }

    private ListView listView;
    public TestService testService;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        BaseIoCContainer.getInstance().injectSingletons(this);
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_f1_s3, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        testService.getUser(1, new WebResponse(new WebSuccessEventUser(), new WebFailureEvent()));

        testService.listUsers(new WebResponse(new WebSuccessEventListUser(), new WebFailureEvent()));
    }

    public void onEvent(WebFailureEvent webError) {
        Log.d(TAG, "failure retrofit " + webError.get().getUrl() + " " + webError.get().getKind());
    }

    public void onEventMainThread(WebSuccessEventUser event) {
        String username = event.get().getName();
        TextView textView = (TextView) getView().findViewById(R.id.username_async);
        if (username != null && textView != null) {
            textView.setText(username);
        }
    }

    public void onEventMainThread(WebSuccessEventListUser event) {
        renderUserList(event.get());
    }

    private void renderUserList(List<User> users) {
        TestAdapter adapter = new TestAdapter(getActivity(), users);
        listView = (ListView) this.getView().findViewById(R.id.list_users_async);
        listView.setAdapter(adapter);
    }

    public class WebSuccessEventUser extends WebSuccessEvent<User> {}

    public class WebSuccessEventListUser extends WebSuccessEvent<List<User>> {}

}
