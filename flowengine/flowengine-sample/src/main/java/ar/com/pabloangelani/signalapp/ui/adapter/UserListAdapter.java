package ar.com.pabloangelani.signalapp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import ar.com.pabloangelani.signalapp.R;
import ar.com.pabloangelani.signalapp.model.User;

/**
 * Created by florencia on 10/06/15.
 */
public class UserListAdapter extends ArrayAdapter<User> {

    private List<User> users;
    private Context context;

    public UserListAdapter(Context context, List<User> users) {
        super(context, R.layout.user_list_item,users);
        this.users = users;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.user_list_item , parent, false);
        setRowValues(position, rowView);

        return rowView;
    }

    private void setRowValues(int position, View rowView){
        TextView username = (TextView) rowView.findViewById(R.id.user_list_item_name);

        User user = users.get(position);

        if(user.getName() != null)
            username.setText(user.getName());

    }
}
