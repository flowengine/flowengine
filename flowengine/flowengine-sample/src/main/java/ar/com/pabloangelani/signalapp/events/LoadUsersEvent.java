package ar.com.pabloangelani.signalapp.events;

import java.util.List;

import ar.com.pabloangelani.signalapp.model.User;

/**
 * Created by florencia on 04/07/15.
 */
public class LoadUsersEvent {

    private List<User> users;

    public LoadUsersEvent(List<User> users){
        this.users = users;
    }

    public List<User> getUsers(){
        return users;
    }

}
