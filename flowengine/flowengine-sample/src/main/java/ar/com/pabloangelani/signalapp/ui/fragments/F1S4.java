package ar.com.pabloangelani.signalapp.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ar.com.pabloangelani.signalapp.R;

public class F1S4 extends BaseStepFragment {

    public F1S4() {
        TAG = "F1S4";
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_f1_s4, container, false);
    }

}
