package ar.com.pabloangelani.signalapp.flow;

import ar.com.pabloangelani.signalapp.R;
import ar.com.pabloangelani.signalapp.model.ISarasa;
import ar.com.pabloangelani.signalapp.ui.activities.SecondActivity;
import ar.com.pabloangelani.signalapp.ui.fragments.F1S1;
import ar.com.pabloangelani.signalapp.ui.fragments.F1S2;
import ar.com.pabloangelani.signalapp.ui.fragments.F1S3;
import ar.com.pabloangelani.signalapp.ui.fragments.F1S4;
import ar.com.pabloangelani.signalapp.ui.fragments.F1S5F3S3;
import ar.com.pabloangelani.signalapp.ui.fragments.F2S1;
import ar.com.pabloangelani.signalapp.ui.fragments.F3S1;
import ar.com.pabloangelani.signalapp.ui.fragments.F3S2;
import flowengine.Flow;
import flowengine.annotations.flow.Animation;
import flowengine.annotations.flow.FlowSteps;
import flowengine.annotations.flow.Join;
import flowengine.annotations.flow.Sequence;
import flowengine.annotations.flow.StepContainer;
import flowengine.annotations.flow.methods.Next;
import flowengine.annotations.flow.methods.SetModel;
import flowengine.annotations.flow.parameters.Argument;

@FlowSteps(
    persistable = true,
    sequences = {
        @Sequence(id = "main", steps = {F1S1.class, F1S2.class, F1S3.class, F1S4.class, F1S5F3S3.class}, exitJump = SecondActivity.class),
        @Sequence(id = "alt1", steps = F2S1.class, exitJump = SecondActivity.class),
        @Sequence(id = "alt2", steps = {F3S1.class, F3S2.class}, exitJoin=@Join(id = "main", stepIndex = 4)),
    })
@StepContainer(R.id.container)
@Animation(enter = R.anim.abc_slide_in_bottom, exit = R.anim.abc_fade_out)
public interface MainFlow extends Flow {

    @Next void next();
    @Next void next(@Argument("arg") String arg);

    @Next("alt1") void startFlow2();
    @Next("alt2") void startFlow3();

    @SetModel("sarasa")
    void setSarasa(ISarasa sarasa);
}
