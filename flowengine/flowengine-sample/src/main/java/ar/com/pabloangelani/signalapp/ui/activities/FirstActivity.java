package ar.com.pabloangelani.signalapp.ui.activities;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.widget.Toolbar;
import android.util.Log;

import java.util.List;

import ar.com.pabloangelani.signalapp.R;
import ar.com.pabloangelani.signalapp.events.LoadUsersEvent;
import ar.com.pabloangelani.signalapp.events.RequestUsersEvent;
import ar.com.pabloangelani.signalapp.events.SaveUserEvent;
import ar.com.pabloangelani.signalapp.flow.MainFlow;
import ar.com.pabloangelani.signalapp.model.ISarasa;
import ar.com.pabloangelani.signalapp.model.User;
import ar.com.pabloangelani.signalapp.model.Sarasa;

import flowengine.FlowEngine;
import flowengine.annotations.callbacks.BeforeJump;
import flowengine.annotations.callbacks.BeforeTransition;
import flowengine.annotations.callbacks.OnFlowFinish;
import flowengine.annotations.flow.AFlow;
import flowengine.annotations.flow.Flow;
import flowengine.annotations.injectable.Persistable;
import flowengine.annotations.webservice.EFWebService;
import retrofit.http.GET;

import flowengine.core.persistence.SQLDao;

import flowengine.annotations.injectable.Model;

@AFlow(MainFlow.class)
public class FirstActivity extends BaseActivity {

    @EFWebService
    public interface TestInjectionInterface {
        @GET("/get/")
        String getTest();
    }

    private static final String TAG = "FirstActivity";

    Toolbar toolbar;

    @Model("sarasa")
    ISarasa sarasa;

    @Persistable
    SQLDao<User,Long> userDao;

    @Flow
    MainFlow flow;

    public TestInjectionInterface test;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signal);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle(R.string.app_name);
            setSupportActionBar(toolbar);
        }
        if (sarasa == null) {
            sarasa = Sarasa.build();
        }
    }

    @Override
    public void onStepCompleted() {
        flow.next();
        Log.i("INJECTION", "Se injecto el test: " + String.valueOf(this.test != null));
    }

    @Override
    public void onStepCompleted(String arg) {
        flow.next(arg);
    }

    @Override
    public void onStartSecondFlow() {
        flow.startFlow2();
    }

    @Override
    public void onStartThirdFlow() {
        flow.startFlow3();
    }

    @BeforeTransition
    public void beforeTransition() {
        Log.d(TAG, "beforeTransition()");
    }

    @BeforeJump
    public void beforeJump() {
        Log.d(TAG, "beforeJump()");
    }

    @OnFlowFinish
    public void onFlowFinish() {
        Log.d(TAG, "onFlowFinish()");
    }

    public void onEvent(SaveUserEvent event){
        try {
            userDao.save(event.getUser());
        } catch(Exception ex){
            Log.e(TAG,ex.getMessage());
        }
    }

    public void onEvent(RequestUsersEvent event){
        try {
            List<User> users = userDao.getAll();
            FlowEngine.getCurrentBus().post(new LoadUsersEvent(users));
        } catch(Exception ex){
            Log.e(TAG,ex.getMessage());
        }
    }

}