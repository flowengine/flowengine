package ar.com.pabloangelani.signalapp.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ar.com.pabloangelani.signalapp.R;
import ar.com.pabloangelani.signalapp.flow.MainFlow;
import flowengine.annotations.flow.AFlow;
public class F1S5F3S3 extends BaseStepFragment {

    public F1S5F3S3() {
        TAG = "F1S5F3S3";
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_f1_s5_f3_s3, container, false);
    }

    protected void callNextStep(){
        mListener.onStepCompleted("Comming from Step 5");
    }

}
