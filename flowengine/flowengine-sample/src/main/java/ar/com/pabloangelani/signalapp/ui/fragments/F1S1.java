package ar.com.pabloangelani.signalapp.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import ar.com.pabloangelani.signalapp.R;

import ar.com.pabloangelani.signalapp.events.SaveUserEvent;
import flowengine.FlowEngine;

public class F1S1 extends BaseStepFragment {

    private EditText etUsername;

    public F1S1() {
        TAG = "F1S1";
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_f1_s1, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        etUsername = (EditText) view.findViewById(R.id.username);

        view.findViewById(R.id.flow2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onStartSecondFlow();
            }
        });
    }

    protected void callNextStep(){
        mListener.onStepCompleted("Coming from Step 1");

        //Save user
        if(etUsername.getText()!= null && !etUsername.getText().toString().isEmpty()){
            String name  = etUsername.getText().toString();
            FlowEngine.getCurrentBus().post(new SaveUserEvent(name));
        }
    }

}
