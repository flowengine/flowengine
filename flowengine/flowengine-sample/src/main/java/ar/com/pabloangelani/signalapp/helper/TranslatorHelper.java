package ar.com.pabloangelani.signalapp.helper;

import android.content.Context;

public class TranslatorHelper {

    public static TranslatorHelper instance = null;

    public static void init(Context context) {
        instance = new TranslatorHelper(context);
    }

    public static TranslatorHelper getInstance() {
        return instance;
    }

    private Context context;

    private TranslatorHelper(Context context) {
        this.context = context;
    }

    public String getString(int resId) {
        return context.getString(resId);
    }

}
