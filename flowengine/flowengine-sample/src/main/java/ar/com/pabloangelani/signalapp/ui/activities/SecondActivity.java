package ar.com.pabloangelani.signalapp.ui.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import ar.com.pabloangelani.signalapp.R;

public class SecondActivity extends ActionBarActivity {
    private static final String TAG = "SecondActivity";

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("SecondActivity");
            setSupportActionBar(toolbar);
        }
        findViewById(R.id.btn_finish).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SecondActivity.this.setResult(RESULT_OK);
                SecondActivity.this.finish();
            }
        });
    }
    
}