package ar.com.pabloangelani.signalapp;

import android.app.Application;

import ar.com.pabloangelani.signalapp.api.TestService;
import ar.com.pabloangelani.signalapp.ui.activities.FirstActivity;
import flowengine.AppConfigurator;
import flowengine.FlowEngine;
import flowengine.core.webservice.WebServiceAdapter;

/**
 * Created by florencia on 10/07/15.
 */
public class TestApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        FlowEngine.initApp(new AppConfigurator() {

            @Override
            public void createWebAdapters() {
                WebServiceAdapter adapter = WebServiceAdapter.startBuilding()
                        .setEndpoint("http://192.168.56.1:62255")
                        .setContext(getApplicationContext())
                        .build();
                registerWebAdapter("test", adapter);
            }

            @Override
            public void createWebServices() {
                registerWebService("test", FirstActivity.TestInjectionInterface.class);
                registerWebService("test", TestService.class);
            }

            @Override
            public void registerSingletons() {

            }
        });
    }
}
