package ar.com.pabloangelani.signalapp.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ar.com.pabloangelani.signalapp.R;

public class F3S2 extends BaseStepFragment {

    public F3S2() {
        TAG = "F3S2";
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_f3_s2, container, false);
    }

    protected void callNextStep(){
        mListener.onStepCompleted("Comming from alternative sequence");
    }

}
