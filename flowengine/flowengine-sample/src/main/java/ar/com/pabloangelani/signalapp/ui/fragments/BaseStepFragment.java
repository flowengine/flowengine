package ar.com.pabloangelani.signalapp.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import ar.com.pabloangelani.signalapp.R;
import ar.com.pabloangelani.signalapp.model.Sarasa;

import flowengine.FlowEngine;
import flowengine.annotations.callbacks.BeforeJump;
import flowengine.annotations.callbacks.BeforeTransition;
import flowengine.annotations.callbacks.OnFlowFinish;
import flowengine.annotations.callbacks.Validate;
import flowengine.annotations.injectable.Model;

public abstract class BaseStepFragment extends Fragment {
    protected String TAG = "BaseStepFragment";

    protected StepListener mListener;
    private CheckBox invalidate;

    @Model("sarasa")
    public Sarasa sarasa;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (StepListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FlowEngine.init(this, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        invalidate = (CheckBox) view.findViewById(R.id.checkBox);
        view.findViewById(R.id.next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callNextStep();
            }
        });
        sarasa.setName("sarasa");
    }

    protected void callNextStep(){
        mListener.onStepCompleted();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onStop() {
        super.onStop();
        FlowEngine.stop(this);
    }

    @Validate("alt2")
    public boolean validateAlt2() {
        Log.d(TAG, "validate(): alt2");
        if (invalidate.isChecked()) {
            Toast.makeText(getActivity(), "Invalid step status", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Validate
    public boolean validate() {
        Log.d(TAG, "validate()");
        if (invalidate.isChecked()) {
            Toast.makeText(getActivity(), "Invalid step status", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @BeforeTransition
    public void beforeTransition() {
        Log.d(TAG, "beforeTransition()");
    }

    @BeforeJump
    public void beforeJump() {
        Log.d(TAG, "beforeJump()");
    }

    @OnFlowFinish
    public void onFlowFinish() {
        Log.d(TAG, "onFlowFinish()");
    }
}
