package ar.com.pabloangelani.signalapp.helper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class PreferencesHelper {

    private SharedPreferences sharedPrefs;
    private SharedPreferences.Editor prefsEditor;

    private static final String KEY_RECENT_EQUATIONS = "KEY_RECENT_EQUATIONS";
    private static final String KEY_FAVOURITE_EQUATIONS  = "KEY_FAVOURITE_EQUATIONS";

    private static final String APP_SHARED_PREFS = PreferencesHelper.class.getSimpleName();

    private Gson gson = null;

    public PreferencesHelper(Context context) {
        this.sharedPrefs = context.getSharedPreferences(APP_SHARED_PREFS, Activity.MODE_PRIVATE);
        this.prefsEditor = sharedPrefs.edit();
        gson = new GsonBuilder()
                .disableHtmlEscaping()
                .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
                .setPrettyPrinting()
                .serializeNulls()
                .excludeFieldsWithoutExposeAnnotation()
                .create();
    }

    public void reset() {
        prefsEditor.remove(KEY_RECENT_EQUATIONS);
        prefsEditor.remove(KEY_FAVOURITE_EQUATIONS);
        prefsEditor.commit();
    }

}
