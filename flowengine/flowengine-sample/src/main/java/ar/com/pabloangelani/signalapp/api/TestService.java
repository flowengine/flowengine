package ar.com.pabloangelani.signalapp.api;

import java.util.List;

import ar.com.pabloangelani.signalapp.model.User;
import flowengine.WebResponse;
import flowengine.annotations.webservice.ID;
import flowengine.annotations.webservice.Cache;
import flowengine.annotations.webservice.EFWebService;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by Ignacio on 12/07/2015.
 */
@EFWebService
public interface TestService {

    @Cache
    @GET("/test")
    List<User> listUsers();

    @Cache
    @GET("/test/{id}")
    User getUser(@Path("id") @ID long userId);

    @Cache
    @GET("/test")
    void listUsers(WebResponse<List<User>> callback);

    @Cache
    @GET("/test/{id}")
    void getUser(@Path("id") @ID long userId, WebResponse<User> callback);



}
