package ar.com.pabloangelani.signalapp.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import flowengine.annotations.injectable.PersistableModel;

/**
 * Created by florencia on 31/05/15.
 */
@PersistableModel
@DatabaseTable
public class User {

    public static final String ID = "id";
    public static final String NAME = "name";

    @DatabaseField(generatedId = true, columnName = ID)
    private Long id;
    @DatabaseField(columnName = NAME)
    private String name;

    public User() {}

    public User(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}