package ar.com.pabloangelani.signalapp.ui.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import ar.com.pabloangelani.signalapp.R;
import ar.com.pabloangelani.signalapp.api.TestService;
import ar.com.pabloangelani.signalapp.model.User;
import ar.com.pabloangelani.signalapp.ui.adapter.TestAdapter;
import flowengine.FlowEngine;
import flowengine.core.injection.BaseIoCContainer;
import ar.com.pabloangelani.signalapp.events.LoadUsersEvent;
import ar.com.pabloangelani.signalapp.events.RequestUsersEvent;
import ar.com.pabloangelani.signalapp.ui.adapter.UserListAdapter;

public class F1S2 extends BaseStepFragment {

    public F1S2() {
        TAG = "F1S2";
    }

    private ListView listView;
    public TestService testService;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        BaseIoCContainer.getInstance().injectSingletons(this);
        return inflater.inflate(R.layout.fragment_f1_s2, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String arg = this.getArguments().getString("arg");
        TextView textView = (TextView) view.findViewById(R.id.arg);
        if (arg != null && textView != null) {
            textView.setText(arg);
        }

       view.findViewById(R.id.flow3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onStartThirdFlow();
            }
        });

//       FlowEngine.getCurrentBus().post(new RequestUsersEvent());

       /* List users repository sync */
//       new SyncOperationOnUsers().execute();

       /* First user repository sync */
//       new SyncOperationOnUser().execute();

    }

    /* List loaded from database */
    public void onEvent(LoadUsersEvent event){
        UserListAdapter adapter = new UserListAdapter(getActivity(),event.getUsers());
        listView = (ListView) this.getView().findViewById(R.id.list_users);
        listView.setAdapter(adapter);
    }

    private void renderUserList(List<User> users) {
        TestAdapter adapter = new TestAdapter(getActivity(), users);
        listView = (ListView) this.getView().findViewById(R.id.list_test);
        listView.setAdapter(adapter);
    }

    /* List loaded from WS sync */
    private class SyncOperationOnUsers extends AsyncTask<String, Void, List<User>> {

        @Override
        protected List<User> doInBackground(String... params) {
            return testService.listUsers();
        }

        @Override
        protected void onPostExecute(List<User> users) {
            renderUserList(users);
        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

    private class SyncOperationOnUser extends AsyncTask<String, Void, User> {

        @Override
        protected User doInBackground(String... params) {
            return testService.getUser(1);
        }

        @Override
        protected void onPostExecute(User user) {
            TextView textView = (TextView) getView().findViewById(R.id.arg);
            if (user != null && textView != null) {
                textView.setText(user.getName());
            }
        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

}
