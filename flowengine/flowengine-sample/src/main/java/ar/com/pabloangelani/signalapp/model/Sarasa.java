package ar.com.pabloangelani.signalapp.model;

import flowengine.annotations.injectable.Builder;

public class Sarasa implements ISarasa{

    private String name;

    @Builder
    public static Sarasa build() {
        return new Sarasa();
    }

    private Sarasa() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
