package ar.com.pabloangelani.signalapp.helper;

import android.content.Context;
import android.widget.Toast;

public class MessageHelper {

    public static void showMessage(Context ctx, String msg, int length) {
        Toast.makeText(ctx, msg, length).show();
    }

    public static void showMessage(Context ctx, String msg) {
        Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show();
    }

    public static void showMessage(Context ctx, int resId) {
        showMessage(ctx,ctx.getText(resId).toString());
    }

    public static void showMessage(Context ctx, int resId, int length) {
        showMessage(ctx,ctx.getText(resId).toString(), length);
    }
}
