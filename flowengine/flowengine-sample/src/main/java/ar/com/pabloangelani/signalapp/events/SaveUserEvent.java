package ar.com.pabloangelani.signalapp.events;

import ar.com.pabloangelani.signalapp.model.User;

/**
 * Created by florencia on 04/07/15.
 */
public class SaveUserEvent {

    private User user;

    public SaveUserEvent(String username){
        user = new User(username);
    }

    public User getUser(){
        return user;
    }
}
