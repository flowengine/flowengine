package ar.com.pabloangelani.signalapp.ui.fragments;


public interface StepListener {
    public void onStepCompleted();
    public void onStepCompleted(String arg);
    public void onStartSecondFlow();
    public void onStartThirdFlow();

}